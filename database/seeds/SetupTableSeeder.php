<?php

use Illuminate\Database\Seeder;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Categoria;
use App\Models\Pedido;
use App\Models\Detalle_pedido;
use App\User;

class SetupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(User::class, 50)->create();
        // factory(Cliente::class, 20)->create();
        // factory(Producto::class, 20)->create();
        // factory(Categoria::class, 20)->create();
        // factory(Pedido::class, 20)->create();
        // factory(Detalle_pedido::class, 50)->create();
    }
}
