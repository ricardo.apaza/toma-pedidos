<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		$this->call(SetupTableSeeder::class);

		DB::table('tbl_grupo_vendedor')->insert([
			'codigo' => 'GRUPOVENDEDOR001',
			'descripcion' => 'GRUPOVENDEDOR001',
			'id_supervisor' => '1',
			'ubigeo' => '',
		]);

		DB::table('tbl_grupo_vendedor')->insert([
			'codigo' => 'GRUPOVENDEDOR002',
			'descripcion' => 'GRUPOVENDEDOR002',
			'id_supervisor' => '1',
			'ubigeo' => '',
		]);

		DB::table('tbl_grupo_vendedor')->insert([
			'codigo' => 'GRUPOVENDEDOR003',
			'descripcion' => 'GRUPOVENDEDOR003',
			'id_supervisor' => '1',
			'ubigeo' => '',
		]);

		DB::table('tbl_perfil')->insert([
			'codigo' => 'ADMIN',
			'descripcion' => 'Administrador',
		]);

		DB::table('tbl_perfil')->insert([
			'codigo' => 'VENDEDOR',
			'descripcion' => 'Vendedor',
		]);

		DB::table('tbl_perfil')->insert([
			'codigo' => 'SUPERVISOR',
			'descripcion' => 'supervisor',
		]);

		DB::table('tbl_usuario')->insert([
			'email' => 'ADMIN',
			'password' => bcrypt('123456'),
			'name' => 'ADMIN',
			'id_perfil' => 1,
		]);

		//vendedores
		DB::table('tbl_usuario')->insert([
			'email' => 'juan@gmail.com',
			'password' => bcrypt('123456'),
			'name' => 'Juan Mendoza',
			'id_perfil' => 2,
		]);
		DB::table('tbl_usuario')->insert([
			'email' => 'elvis@gmail.com',
			'password' => bcrypt('123456'),
			'name' => 'Elvis Perez',
			'id_perfil' => 2,
		]);

		//2
		DB::table('tbl_vendedor')->insert([
			'codigo' => 'USUARIO004',
			'apellido' => 'Liberty',
			'nombre' => 'Valerie',
			'id_grupo_vendedor' => 1,
			'id_usuario' => 52,
		]);

		DB::table('tbl_vendedor')->insert([
			'codigo' => 'USUARIO004',
			'apellido' => 'Yair',
			'nombre' => 'Vargas',
			'id_grupo_vendedor' => 1,
			'id_usuario' => 53,
		]);

//         DB::table('tbl_usr')->insert([
		//             'email' => 'wchoque',
		//             'password' => bcrypt('Abc123'),
		//             'name'=>'wchoque',
		//         ]);
		//         DB::table('tbl_usr')->insert([
		//             'email' => 'yccasa',
		//             'password' => bcrypt('Abc123'),
		//             'name'=>'yccasa',
		//         ]);

		DB::table('tbl_cliente')->insert([
			'codigo' => 'CLIENTE001',
			'apellido' => 'Guilizzani',
			'nombre' => 'Giacomo',
			'razon_social' => 'RS 01',
			'tipo_cliente' => 'Mayorista',
			'giro' => 'Bodega',
			'deuda' => 0,
			'calificacion' => '',
		]);
		DB::table('tbl_cliente')->insert([
			'codigo' => 'CLIENTE002',
			'apellido' => 'Botton',
			'nombre' => 'Marco',
			'razon_social' => 'RS 02',
			'tipo_cliente' => 'Minorista',
			'giro' => 'Bodega',
			'deuda' => 0,
			'calificacion' => '',
		]);

		DB::table('tbl_cliente')->insert([
			'codigo' => 'CLIENTE003',
			'apellido' => 'Guilizzani',
			'nombre' => 'Giacomo',
			'razon_social' => 'RS 03',
			'tipo_cliente' => 'Mayorista',
			'giro' => 'Bodega',
			'deuda' => 0,
			'calificacion' => '',
		]);

		DB::table('tbl_cliente')->insert([
			'codigo' => 'CLIENTE004',
			'apellido' => 'Liberty',
			'nombre' => 'Valerie',
			'razon_social' => 'RS 04',
			'tipo_cliente' => 'Minorista',
			'giro' => 'Bodega',
			'deuda' => 0,
			'calificacion' => '',
		]);

		DB::table('tbl_cliente')->insert([
			'codigo' => 'CLIENTE005',
			'apellido' => 'Guilizzani',
			'nombre' => 'Guido Jack',
			'razon_social' => 'RS 05',
			'tipo_cliente' => 'Minorista',
			'giro' => 'Bodega',
			'deuda' => 0,
			'calificacion' => '',
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Coronita',
			// 'descripcion' => '...',
			'precio_base' => 3.30,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Oreo',
			// 'descripcion' => '...',
			'precio_base' => 0.80,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Tentacion de Vainilla',
			// 'descripcion' => '...',
			'precio_base' => 1.00,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Tentacion de Chocolate',
			// 'descripcion' => '...',
			'precio_base' => 1.00,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Soda V',
			// 'descripcion' => '...',
			'precio_base' => 1.20,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Coronita',
			// 'descripcion' => '...',
			'precio_base' => 3.30,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Oreo',
			// 'descripcion' => '...',
			'precio_base' => 0.80,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Tentacion de Vainilla',
			// 'descripcion' => '...',
			'precio_base' => 1.00,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Tentacion de Chocolate',
			// 'descripcion' => '...',
			'precio_base' => 1.00,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Soda V',
			// 'descripcion' => '...',
			'precio_base' => 1.20,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Coronita',
			// 'descripcion' => '...',
			'precio_base' => 3.30,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Oreo',
			// 'descripcion' => '...',
			'precio_base' => 0.80,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Tentacion de Vainilla',
			// 'descripcion' => '...',
			'precio_base' => 1.00,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Tentacion de Chocolate',
			// 'descripcion' => '...',
			'precio_base' => 1.00,
		]);

		DB::table('tbl_producto')->insert([
			'codigo' => 'PRODUCTO001',
			'descripcion' => 'Galletas Soda V',
			// 'descripcion' => '...',
			'precio_base' => 1.20,
		]);

		DB::table('tbl_ruta')->insert([
			'id_cliente' => 1,
			'id_vendedor' => 1,
			'fecha_inicio' => '16/10/2018',
			'fecha_fin' => '26/10/2018',
			'dia_semana' => 'L',
		]);

		DB::table('tbl_ruta')->insert([
			'id_cliente' => 2,
			'id_vendedor' => 2,
			'fecha_inicio' => '02/12/2018',
			'fecha_fin' => '26/12/2018',
			'dia_semana' => 'X',
		]);

//         //example
		DB::table('tbl_pedido')->insert([
			'id_cliente' => 1,
			'id_vendedor' => 1,
			'fecha_inicio' => '2016/03/01',
			'hora_inicio' => '23:30:00 ',
			'codigo' => 'PED2018-000001',
			'monto_total' => 30.00,
			'nota' => '',
			'longitud' => '',
			'latitud' => '',
		]);

		DB::table('tbl_pedido')->insert([
			'id_cliente' => 2,
			'id_vendedor' => 2,
			'fecha_inicio' => '2016/03/01',
			'hora_inicio' => '23:30:00 ',
			'codigo' => 'PED2018-000002',
			'monto_total' => 30.00,
			'nota' => '',
			'longitud' => '',
			'latitud' => '',
		]);

//TODO ->TBL_VENDEDOR

		DB::table('tbl_pedido_detalle')->insert([
			'id_pedido' => 1,
			'id_producto' => 1,
			'cantidad' => 2,
			'precio_unitario' => 1.5,
			'id_precio_producto' => 1,
			'descuento_porcentaje' => 0,
			'subtotal' => 3.00,
		]);

		DB::table('tbl_pedido_detalle')->insert([
			'id_pedido' => 1,
			'id_producto' => 2,
			'cantidad' => 3,
			'precio_unitario' => 1.00,
			'subTotal' => 3.00,
			'id_precio_producto' => 1,
			'descuento_porcentaje' => 0,
		]);

		DB::table('tbl_pedido_detalle')->insert([
			'id_pedido' => 1,
			'id_producto' => 3,
			'cantidad' => 4,
			'precio_unitario' => 0.50,
			'subTotal' => 2.00,
			'id_precio_producto' => 1,
			'descuento_porcentaje' => 0,
		]);

		DB::table('tbl_pedido_detalle')->insert([
			'id_pedido' => 1,
			'id_producto' => 4,
			'cantidad' => 10,
			'precio_unitario' => 1,
			'subTotal' => 10.00,
			'id_precio_producto' => 1,
			'descuento_porcentaje' => 0,
		]);

		DB::table('tbl_pedido_detalle')->insert([
			'id_pedido' => 1,
			'id_producto' => 5,
			'cantidad' => 8,
			'precio_unitario' => 1.5,
			'subTotal' => 12.00,
			'id_precio_producto' => 1,
			'descuento_porcentaje' => 0,
		]);

		DB::table('tbl_pedido_detalle')->insert([
			'id_pedido' => 2,
			'id_producto' => 5,
			'cantidad' => 8,
			'precio_unitario' => 1.5,
			'subTotal' => 12.00,
			'id_precio_producto' => 1,
			'descuento_porcentaje' => 0,
		]);

// //fin demo

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);
		//          DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 2,
		//             'fechaHora'=>'2018/10/15 05:00:00 ',
		//             'montoTotal'=>810.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 3,
		//             'fechaHora'=>'2018/05/15 12:15:00 ',
		//             'montoTotal'=>1500.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 4,
		//             'fechaHora'=>'2017/10/16 04:30:00 ',
		//             'montoTotal'=>30.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         DB::table('tbl_ped')->insert([
		//             'id_vendedor' => 1,
		//             'id_cliente' => 5,
		//             'fechaHora'=>'2014/10/30 06:45:00 ',
		//             'montoTotal'=>60.00,
		//             'observaciones'=>'Ninguna',
		//             'longitud'=>'...',
		//             'latitud'=>'...',
		//         ]);

//         //table NO PEDIDOS
		DB::table('tbl_no_pedido')->insert([
			'id_vendedor' => 1,
			'id_cliente' => 1,
			'fecha_inicio' => '2016/09/08',
			'hora_inicio' => '08:33:00 ',
			'motivo_no_pedido' => 'Tiene stock',
			'nota' => '',
		]);
		DB::table('tbl_no_pedido')->insert([
			'id_vendedor' => 2,
			'id_cliente' => 2,
			'fecha_inicio' => '2016/09/08',
			'hora_inicio' => '08:40:00 ',
			'motivo_no_pedido' => 'Tienda cerrada',
			'nota' => '',
		]);
		DB::table('tbl_no_pedido')->insert([
			'id_vendedor' => 1,
			'id_cliente' => 3,
			'fecha_inicio' => '2016/09/08',
			'hora_inicio' => '09:00:00 ',
			'motivo_no_pedido' => 'No se encuentra',
			'nota' => '',
		]);
		DB::table('tbl_no_pedido')->insert([
			'id_vendedor' => 2,
			'id_cliente' => 4,
			'fecha_inicio' => '2016/09/08',
			'hora_inicio' => '11:15:00 ',
			'motivo_no_pedido' => 'Se cambio de domicilio',
			'nota' => '',
		]);
		DB::table('tbl_no_pedido')->insert([
			'id_vendedor' => 2,
			'id_cliente' => 5,
			'fecha_inicio' => '2016/09/08',
			'hora_inicio' => '13:13:00 ',
			'motivo_no_pedido' => 'Quebro',
			'nota' => '',
		]);

		DB::table('tbl_cliente_direccion')->insert([
			'id_cliente' => 1,
			'direccion' => 'su casa 1',
			'ubigeo' => '',
			'tipo_direccion' => 'Negocio',
			'principal' => 1,
			'latitud' => '',
			'longitud' => '',
		]);

		DB::table('tbl_cliente_direccion')->insert([
			'id_cliente' => 1,
			'direccion' => 'su casa 2',
			'ubigeo' => '',
			'tipo_direccion' => 'Entrega',
			'principal' => 0,
			'latitud' => '',
			'longitud' => '',
		]);

		DB::table('tbl_cliente_direccion')->insert([
			'id_cliente' => 1,
			'direccion' => 'su casa 3',
			'ubigeo' => '',
			'tipo_direccion' => 'Pago',
			'principal' => 0,
			'latitud' => '',
			'longitud' => '',
		]);

		DB::table('tbl_cliente_direccion')->insert([
			'id_cliente' => 1,
			'direccion' => 'su casa 4',
			'ubigeo' => '',
			'tipo_direccion' => 'Domicilio',
			'principal' => 0,
			'latitud' => '',
			'longitud' => '',
		]);

		DB::table('tbl_cliente_direccion')->insert([
			'id_cliente' => 2,
			'direccion' => 'casa cliente 2',
			'ubigeo' => '',
			'tipo_direccion' => 'Negocio',
			'principal' => 1,
			'latitud' => '',
			'longitud' => '',
		]);

		DB::table('tbl_cliente_telefono')->insert([
			'id_cliente' => 1,
			'telefono' => '949494944',
			'tipo_telefono' => 'Celular',
			'operadora' => 'MOVISTAR',
			'id_cliente_direccion' => 1,
			'principal' => 1,
		]);
		DB::table('tbl_cliente_telefono')->insert([
			'id_cliente' => 1,
			'telefono' => '45454545',
			'tipo_telefono' => 'Fijo',
			'operadora' => 'CLARO',
			'id_cliente_direccion' => 1,
			'principal' => 0,
		]);

		DB::table('tbl_cliente_telefono')->insert([
			'id_cliente' => 1,
			'telefono' => '12321321321',
			'tipo_telefono' => 'Celular',
			'operadora' => 'MOVISTAR',
			'id_cliente_direccion' => 1,
			'principal' => 0,
		]);
		DB::table('tbl_cliente_telefono')->insert([
			'id_cliente' => 1,
			'telefono' => '12321',
			'tipo_telefono' => 'Fijo',
			'operadora' => 'CLARO',
			'id_cliente_direccion' => 1,
			'principal' => 0,
		]);

		DB::table('tbl_cliente_email')->insert([
			'id_cliente' => 1,
			'email' => 'email1@mail.com',
			'tipo_email' => '',
			'principal' => 1,
		]);
		DB::table('tbl_cliente_email')->insert([
			'id_cliente' => 1,
			'email' => 'email1@mail.com',
			'tipo_email' => '',
			'principal' => 0,
		]);
		DB::table('tbl_cliente_email')->insert([
			'id_cliente' => 1,
			'email' => 'email1@mail.com',
			'tipo_email' => '',
			'principal' => 0,
		]);

	}

}
