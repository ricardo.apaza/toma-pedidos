<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('tbl_perfil', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('codigo', 45);
			$table->string('descripcion', 100)->default('');
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::create('tbl_usuario', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
			// $table->string('email',100)->unique();
			// $table->string('email', 250)->unique();
			$table->string('email');
			$table->string('password');
			$table->bigInteger('id_perfil')->unsigned()->nullable();;
			$table->foreign('id_perfil')->references('id')->on('tbl_perfil')->onDelete('cascade');
			$table->rememberToken();
			$table->softDeletes();
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('tbl_usuario');
		Schema::dropIfExists('tbl_perfil');
	}
}
