<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    //codigo | nombre | direccion | telefono | tipo cliente
        Schema::create('tbl_cliente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',50);
            $table->string('apellido',60);
            $table->string('nombre',40);
            $table->string('razon_social',100);
            $table->string('tipo_cliente',45)->default('Minorista');//Mayoriste Minorista
            $table->string('giro',45);
            $table->decimal('deuda',18,2)->nullable();
            $table->string('calificacion',50);
            $table->softDeletes();
            $table->timestamps();
        });


        //TODO id_supervisor, de donde viene se id->smallInt
        Schema::create('tbl_grupo_vendedor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',50);
            $table->string('descripcion',100);
            $table->bigInteger('id_supervisor');
            $table->string('ubigeo',20)->default('');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_vendedor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',50);
            $table->string('apellido',60);
            $table->string('nombre',40);
            $table->bigInteger('id_grupo_vendedor')->unsigned()->nullable();//vendedor
            $table->foreign('id_grupo_vendedor')->references('id')->on('tbl_grupo_vendedor')->onDelete('cascade');
            $table->bigInteger('id_usuario')->unsigned()->nullable();
            $table->foreign('id_usuario')->references('id')->on('tbl_usuario')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_cliente_email', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_cliente')->unsigned()->nullable();//vendedor
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->string('email',100)->nullable();
            $table->string('tipo_email',45)->nullable();
            $table->boolean('principal')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('tbl_cliente_telefono', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_cliente')->unsigned()->nullable();
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->string('telefono',45)->nullable();
            $table->string('tipo_telefono',45)->nullable();
            $table->string('operadora',45)->nullable();
            $table->string('id_cliente_direccion',45)->nullable();
            $table->boolean('principal')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
        /*  */

        
        Schema::create('tbl_cliente_direccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_cliente')->unsigned()->nullable();//vendedor
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->string('direccion',400);
            $table->string('ubigeo',20);
            $table->string('tipo_direccion',45);
            $table->boolean('principal');
            $table->string('latitud',45);
            $table->string('longitud',45);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_cliente_documento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_documento',45);
            $table->string('numero_documento',45);
            $table->bigInteger('id_cliente')->unsigned()->nullable();//vendedor
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->boolean('principal')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('tbl_producto_unidad_venta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',45);
            $table->string('descripcion',100);
            $table->string('cantidad_unidad',45);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('tbl_producto_categoria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',45);
            $table->string('descripcion',100);
            $table->string('id_padre',45);
            $table->softDeletes();
            $table->timestamps();
        });

    

        Schema::create('tbl_producto_presentacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',45);
            $table->string('descripcion',100);
            $table->softDeletes();
            $table->timestamps();
        });
             

        Schema::create('tbl_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('nombre');
            $table->string('codigo',45);
            $table->string('descripcion',400)->default('...');
            $table->decimal('precio_base',18,2);
            $table->bigInteger('id_categoria')->unsigned()->nullable();
            $table->foreign('id_categoria')->references('id')->on('tbl_producto_categoria')->onDelete('cascade');
            $table->bigInteger('id_unidad')->unsigned()->nullable();
            $table->foreign('id_unidad')->references('id')->on('tbl_producto_unidad_venta')->onDelete('cascade');
            $table->bigInteger('id_presentacion')->unsigned()->nullable();
            $table->foreign('id_presentacion')->references('id')->on('tbl_producto_presentacion')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_producto_precio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_producto')->unsigned()->nullable();//vendedor
            $table->foreign('id_producto')->references('id')->on('tbl_producto')->onDelete('cascade');
            $table->string('condicion_venta',45);
            $table->string('tipo_cliente',45);
            $table->softDeletes();
            $table->timestamps();
        });
        // Schema::create('tbl_cat', function (Blueprint $table) {
        //     $table->increments('id_cat');
        //     $table->string('nombre');
        //     $table->string('descripcion');
        //     $table->softDeletes();
        //     $table->timestamps();
        // });

        // Schema::create('tbl_cat_pro', function (Blueprint $table) {
        //     $table->increments('id_catpro');
        //     $table->integer('cat_id_cat')->unsigned()->nullable();
        //     $table->foreign('cat_id_cat')->references('id_cat')->on('tbl_cat')->onDelete('cascade');
        //     $table->integer('pro_id_pro')->unsigned()->nullable();
        //     $table->foreign('pro_id_pro')->references('id_pro')->on('tbl_pro')->onDelete('cascade');
        //     $table->softDeletes();
        //     $table->timestamps();
        // });

        Schema::create('tbl_pedido', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_cliente')->unsigned()->nullable();//vendedor
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->bigInteger('id_vendedor')->unsigned()->nullable();
            $table->foreign('id_vendedor')->references('id')->on('tbl_vendedor')->onDelete('cascade');
            //$table->string('nombreCliente');
            // $table->datetime('fechaHora')->nullable();
            $table->string('codigo',100)->default('PED2018-000000');
            $table->string('fecha_inicio',45)->default('');
            $table->string('hora_inicio',45)->default('');
            $table->decimal('monto_total',18,2);
            $table->string('nota',400)->default('');
            $table->string('latitud',45)->default('');
            $table->string('longitud',45)->default('');
            $table->softDeletes();
            $table->timestamps();
        });

        //TODO-> ID_PRECIO_PRODUCTO
        Schema::create('tbl_pedido_detalle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pedido')->unsigned()->nullable();//
            $table->foreign('id_pedido')->references('id')->on('tbl_pedido')->onDelete('cascade');
            $table->bigInteger('id_producto')->unsigned()->nullable();//
            $table->foreign('id_producto')->references('id')->on('tbl_producto')->onDelete('cascade');
            $table->integer('cantidad');
            $table->decimal('precio_unitario',18,2)->nullable();
            $table->integer('id_precio_producto');
            $table->integer('descuento_porcentaje');
            $table->decimal('subtotal',18,2)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_no_pedido', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_cliente')->unsigned()->nullable();//vendedor
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->bigInteger('id_vendedor')->unsigned()->nullable();
            $table->foreign('id_vendedor')->references('id')->on('tbl_vendedor')->onDelete('cascade');
            $table->string('codigo',100)->default('PED2018-000000');
            $table->string('fecha_inicio')->default('');
            $table->string('hora_inicio')->default('');
            $table->string('motivo_no_pedido');
            $table->string('nota');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_general',function(Blueprint $table){
            $table->string('codigo_grupo',45);
            $table->string('codigo',45);
            $table->string('descripcion',45);
            $table->boolean('default');
            $table->softDeletes();
            $table->timestamps();
        });



        /**/

            

                 /**/

//TODO -> fechas, relacion vendedor -> id
    Schema::create('tbl_ruta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_cliente')->unsigned()->nullable();
            $table->foreign('id_cliente')->references('id')->on('tbl_cliente')->onDelete('cascade');
            $table->bigInteger('id_vendedor')->unsigned()->nullable();
            $table->foreign('id_vendedor')->references('id')->on('tbl_vendedor')->onDelete('cascade');
            $table->string('fecha_inicio',45)->nullable();
            $table->string('fecha_fin',45)->nullable();
            $table->string('dia_semana');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('tbl_cliente');
        Schema::dropIfExists('tbl_cliente_email');
        Schema::dropIfExists('tbl_cliente_telefono');
        Schema::dropIfExists('tbl_cliente_direccion');
        Schema::dropIfExists('tbl_cliente_documento');
        Schema::dropIfExists('tbl_cliente_direccion');



        Schema::dropIfExists('tbl_producto');
        Schema::dropIfExists('tbl_producto_presentacion');
        Schema::dropIfExists('tbl_producto_precio');
        Schema::dropIfExists('tbl_producto_categoria');
        Schema::dropIfExists('tbl_producto_unidad_venta');

        Schema::dropIfExists('tbl_pedido');
        Schema::dropIfExists('tbl_no_pedido');
        Schema::dropIfExists('tbl_pedido_detalle');

        Schema::dropIfExists('tbl_grupo_vendedor');
        Schema::dropIfExists('tbl_ruta');
        Schema::dropIfExists('tbl_general');

    }
}
