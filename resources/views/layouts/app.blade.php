<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>



    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tittle -->
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Style -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
{{-- <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> --}}
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/footable/footable.core.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-toggle.min.css') }}" rel="stylesheet">



    {{-- <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> --}}
    {{-- <link href="css/bootstrap-switch/bootstrap-switch.css" rel="stylesheet"></head> --}}

    <!-- Script -->
    {{-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"> </script> --}}
    {{-- <script
              src="https://code.jquery.com/jquery-3.3.1.js"
              integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
              crossorigin="anonymous"></script> --}}

    <script>  var root = "{{url('/')}}";</script>
    <script src="{{ asset('js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('js/plugins/jsKnob/jquery.knob.js') }}"></script>
    <script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/nouslider/jquery.nouislider.min.js') }}"></script>
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/ionRangeSlider/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

    {{-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"> </script> --}}
    {{-- modificaciion orden j1uery --}}
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"> </script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.9/metisMenu.js"></script> --}}


    <script src="{{ asset('js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>
    <script src="{{ asset('js/plugins/cropper/cropper.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fullcalendar/moment.min.js') }}"></script>
    <script src="{{ asset('js/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/plugins/touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('js/plugins/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/plugins/footable/footable.all.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <!-- Vue -->
    <script src="{{ asset('js/vue/vue.js') }}"></script>
    <script src="{{ asset('js/vue/vue-router.min.js') }}"></script>
    <script src="{{ asset('js/vue/vue-resource.min.js') }}"></script>
    <script src="{{ asset('js/vue/vue-strap.js') }}"></script>

{{--  --}}
    <!-- App -->

    <script src="{{ asset('js/app/hifi_components.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".touchspin1").TouchSpin({
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });
            //
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            //
            $(".select2").select2({
                placeholder: "Seleccione un item",
                allowClear: true,
                width: "100%"
            });
            //
            $('.footable').footable();
            //
            $(".date").datepicker();
            });
    </script>
</head>

<body class="pace-done" cz-shortcut-listen="true">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>

    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation" id="menu">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu" style="">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span>
                               <img alt="image" class="img-circle" src="{{ url('/img/lock_color.png') }}">
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">

                                    <span class="block m-t-xs">
                                        <strong class="font-bold">TOMA PEDIDOS</strong>
                                    </span> 
                                    <span class="block m-t-xs">
                                        <strong class="font-bold">{{Auth::user()->name}}</strong>
                                    </span>
                                    <span class="text-muted text-xs block">PERFIL = {{Auth::user()->id_perfil}}
                                        <b class="caret"></b>
                                    </span>

                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li>
                                    <a href="{!! url('/logout') !!}">Cerrar Sesión</a>
                                </li>
                            </ul>
                        </div>
                        <div class="logo-element">TOMA PEDIDOS</div>
                    </li>
                    <li>
                        <a href="{{ url('home') }}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">Inicio</span>
                        </a>
                    </li>


                    <li>
                        <a href="">
                            <i class="fa fa-user"></i>
                            <span class="nav-label">Vendedores</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('vendedores') }}">Vendedores</a>
                            </li>
                            <li>
                                <a href="{{ url('grupos-vendedores') }}">Grupos Vendedores</a>
                            </li>
                            <li>
                                <a href="{{ url('rutas') }}">Ruta</a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="">
                            <i class="fa fa-users"></i>
                            <span class="nav-label">Clientes</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('clientes') }}">Clientes</a>
                            </li>
                            <li>
                                <a href="{{ url('clientes-direcciones') }}">Direcciones</a>
                            </li>
                            <li>
                                <a href="{{ url('clientes-telefonos') }}">Teléfonos</a>
                            </li>
                            <li>
                                <a href="{{ url('clientes-emails') }}">Emails</a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="">
                            <i class="fa fa-cubes"></i>
                            <span class="nav-label">Productos</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('productos') }}">Productos</a>
                            </li>
                            <li>
                                <a href="{{ url('productos-lista-precios') }}">Zona</a>
                            </li>
                            <li>
                                <a href="{{ url('producto-categorias') }}">Categorias</a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="">
                            <i class="fa fa-file"></i>
                            <span class="nav-label">Reportes</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                {{-- <a href="{{ url('reportes') }}">Reportes</a> --}}
                                <a href="#">Reportes</a>
                            </li>
                        </ul>
                    </li>
                         <li>
                        <a href="">
                            <i class="fa fa-pencil"></i>
                            <span class="nav-label">Pedidos</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('pedidos') }}">Pedidos</a>
                            </li>   
                            <li>
                                <a href="{{ url('no-pedidos') }}">No pedidos</a>
                            </li>
                            <li>
                                <a href="{{ url('cotizaciones') }}">Cotizaciones</a>
                            </li>
                            <li>
                                <a href="{{ url('visitas') }}">Visitas</a>
                            </li>
                        </ul>
                    </li>
                         <li>
                        <a href="">
                            <i class="fa fa-wrench"></i>
                            <span class="nav-label">Importar / Exportar</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('importar') }}">Importar</a>
                            </li>
                            <li>
                                <a href="{{ url('exportar') }}">Exportar</a>
                            </li>
                            {{-- <li>
                                <a href="{{ url('producto-categorias') }}">Categorias</a>
                            </li> --}}
                        </ul>
                    </li>
                         <li>
                        <a href="">
                            <i class="fa fa-cogs"></i>
                            <span class="nav-label">Configuracion</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('configuracion') }}">Configuracion</a>
                            </li>
                            
                        </ul>
                    </li>


           {{--          <li>
                        <a href="">
                            <i class="fa fa-user"></i>
                            <span class="nav-label">Mantenimientos</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('usuarios') }}">Usuarios</a>
                            </li>
                            <li>
                                <a href="{{ url('clientes') }}">Clientes</a>
                            </li>
                            <li>
                                <a href="{{ url('productos') }}">Productos</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-file"></i>
                            <span class="nav-label">Reportes</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            
                            <li>
                                <a href="{{ url('pedidos') }}">Pedidos</a>
                            </li>
                            <li>
                                <a href="{{ url('no-pedidos') }}">No pedidos</a>
                            </li>
                            <li>
                                <a href="{{ url('/vendedor/vendedor-datos/5') }}">test</a>
                            </li>


                                                        
                        </ul>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-sitemap"></i>
                            <span class="nav-label">Integracion</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{url('integracion1')}}">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                                
                            </li>
                            <li>
                                <a href="#">3</a>
                                
                            </li>
                      
                        </ul>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-wrench"></i>
                            <span class="nav-label">Herramientas</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{ url('exportar') }}">Exportar</a>
                            </li>
                            <li>
                                <a href="{{ url('importar') }}">Importar</a>
                            </li>
                            
                        </ul>
                    </li> --}}
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg" style="min-height: 382px;">
            <div class="row border-bottom" id="cabecera">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span><i class="fa fa-user"></i></span>
                        </li>
                        <li>
                            <span class="m-r-sm text-muted welcome-message">{{Auth::user()->name}}</span>
                            {{-- <i class="fa fa-sign-out"> --}}
                            {{-- user --}}
                           
                        </li>
                        <li>
                            <a href="{!! url('/logout') !!}"><i class="fa fa-sign-out"></i></a>
                        </li>

                        <!--<li>


                                 <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fa fa-sign-out"></i>
                                        {{ __('Cerrar Sesión') }}
                                    </a>
                    
                        </li>-->
                    </ul>
                    
                </nav>
            </div>
            <!--<div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>-->

            @yield('content')

            
             <div id="app">
                <app></app>
            </div>


            <div class="footer text-center">
                <div>
                    <strong>Copyright</strong> Developer &copy; 2018
                </div>
            </div>

        </div>
    </div>
    {{-- <script src="{{ mix('js/app.js') }}"></script> --}}

</body>

</html>
