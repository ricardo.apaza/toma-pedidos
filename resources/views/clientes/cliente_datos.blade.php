@extends('layouts.app') 
@section('content')
    <div id="vendedores">
         <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInUp">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="col-lg-10">
                                <div class="col-lg-1">
                                    <a href="/clientes" class="btn btn-info"><i class="fa fa-arrow-left"></i></a>
                                </div>
                                <div class="col-lg-11">
                                    <bold class="lead">
                                        <label class=""> CLIENTE:
                                        </label>
                                    </bold> 
                                    <spam class="lead"> @{{cliente[0].nombre}}, @{{cliente[0].apellido}}</spam>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <span  @click.prevent="editarDatos();" class="btn btn-default"><i class="fa fa-pencil"></i></span>
                            </div>
                            <div class="col-lg-1">
                                <span @click.prevent="eliminarCliente(cliente[0].id);" class="btn btn-danger"> <i class="fa fa-trash"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
            <div class="">
                @foreach ($cliente as $c)
                    <select hidden v-model="id_cliente">
                      <option selected>{{$c->id}}</option>
                    </select>

                @endforeach
            </div>

              <div class="row" > 
                <div class="wrapper wrapper-content">
                    <div class="row animated fadeInRight">
                        <div class="col-lg-12">
                            <div class="wrapper wrapper-content animated fadeInUp">
                                <div class="ibox">
                                     <div class="ibox-content">
                                        <div class="project-list">
                                            <div class="row m-b-sm m-t-sm">
                                                <form class="form-horizontal">
                                                    <div class=""  v-for="c in cliente">
                                                        <div class="form-group">
                                                            <label class="col-md-1 control-label">Codigo</label>
                                                            <div class="col-md-3">
                                                                <input readonly type="text" class="form-control" v-model="cliente_editar.codigo" placeholder="VEND001" value="@{{c.codigo}}" />
                                                            </div>

                                                            <label class="col-md-1 control-label">Nombre</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" v-if="editar" placeholder="Mendoza" v-model="cliente_editar.nombre" value="@{{c.nombre}}">
                                                                <h1 class="text-success" v-else="editar"  type="text" name="">@{{c.nombre}}</h1>
                                                            </div>

                                                            <label class="col-md-1 control-label">Estado</label>
                                                            <div class="col-md-3">
                                                                {{-- <input type="text" class="form-control" v-if="editar" placeholder="Juan" v-model="cliente_editar.estado" value="@{{c.estado}}"/>
                                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{c.estado}}</h1> --}}
                                                                <label class="label label-success">VISITADO  </label><label class="text-success"> &emsp; 04-09-2018</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                          
                                                            <label class="col-md-1 control-label">Giro</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" v-if="editar" v-model="cliente_editar.giro" value="@{{c.giro}}"/>
                                                                <h1 class="text-success" v-else="editar"  type="text" name="">@{{c.giro}}</h1>
                                                            </div>
                                                            
                                                            <label class="col-md-1 control-label">Tipo</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" v-if="editar"  v-model="cliente_editar.tipo_cliente" value="@{{c.tipo_cliente}}"/>
                                                                <h1 class="text-success" v-else="editar"  type="text" name="">@{{c.tipo_cliente}}</h1>
                                                            </div>
                                                            
                                                        </div>
                                                      
                                                        <div class="form-group">
                                                            <div v-if="editar" class="col-md-offset-9">
                                                                <span class="btn btn-md btn-primary" v-on:click="guardarCliente()">Guardar</span>
                                                                <span class="btn btn-md btn-danger" v-on:click="noEditarDatos()">Cancelar</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Modal  -->
                                    <div class="modal fade" id="MostrarDetalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">EDITAR DIRECCION</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive" v-for=" cd in cliente_direccion">
                                                                    <div>
                                                                       <h1>DIRECCION:</h1> 
                                                                        <input type="text" class="form-control"  v-model="cliente_direccion_editar.direccion" value="@{{cd.direccion}}">
                                                                    </div>
                                                                     <div>
                                                                        <h1>TIPO:</h1> 
                                                                            <select class="form-control"  v-model="cliente_direccion_editar.tipo_direccion">
                                                                                <option value="Negocio">Negocio</option>
                                                                                <option value="Entrega">Entrega</option>
                                                                                <option value="Pago">Pago</option>
                                                                                <option value="Domicilio" selected>Domicilio</option>
                                                                            </select>
                                                                    </div>
                                                                    <div>
                                                                        <h1>PRINCIPAL</h1> 
                                                                        <div>
                                                                            <label class="switch">
                                                                            <input type="checkbox" checked v-model="cliente_direccion_editar.principal">
                                                                            <span class="slider round"></span>
                                                                            </label>     
                                                                        </div>
                                                                    </div>
                                                                </div>                                          
                                                            </div>
                                                        </div>
                                                <div class="modal-footer">
                                                         <button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="editarClienteDireccion();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- nueva direccion --}}
                                      <div class="modal fade" id="crear_nueva_direccion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">CREAR DIRECCION</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive">
                                                                    <div>
                                                                       <h1>DIRECCION:</h1> 
                                                                        <textarea type="text" class="form-control"  v-model="cliente_direccion_nuevo.direccion">
                                                                            @{{direccion.direccion}}
                                                                        </textarea>
                                                                    </div>
                                                                     <div>
                                                                        <h1>TIPO:</h1> 
                                                                        <select class="form-control" v-model="cliente_direccion_nuevo.tipo_direccion">
                                                                          <option value="Negocio">Negocio</option>
                                                                          <option value="Entrega">Entrega</option>
                                                                          <option value="Pago">Pago</option>
                                                                          <option value="Domicilio" selected>Domicilio</option>
                                                                        </select>
                                                                    </div>
                                                                    <div>
                                                                        <h1>PRINCIPAL</h1> 
                                                                        <div>
                                                                            <label class="switch">
                                                                            <input type="checkbox" checked v-model="cliente_direccion_nuevo.principal">
                                                                            <span class="slider round"></span>
                                                                            </label>     
                                                                        </div>
                                                                    </div>
                                                                    
                                                            </div>                                          
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                         <button type="button" data-dismiss="modal" class="btn btn-primary" v-on:click="guardarClienteDireccion();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



{{-- TELEFONO MODALS--}}
                                    <div class="modal fade" id="modal_telefono_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">EDITAR TELEFONO</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive" v-for=" ct in cliente_telefono">
                                                                    <div>
                                                                       <h1>TELEFONO:</h1> 
                                                                        <input type="text" class="form-control"  v-model="cliente_telefono_editar.telefono" value="@{{ct.telefono}}">
                                                                    </div>
                                                                     <div>
                                                                        <h1>TIPO:</h1> 
                                                                            <select class="form-control"  v-model="cliente_telefono_editar.tipo_telefono">
                                                                                <option value="CELULAR">CELULAR</option>
                                                                                <option value="FIJO">FIJO</option>
                                                                            </select>
                                                                    </div>
                                                                    <div>
                                                                        <h1>PRINCIPAL</h1> 
                                                                        <div>
                                                                            <label class="switch">
                                                                            <input type="checkbox" checked v-model="cliente_telefono_editar.principal">
                                                                            <span class="slider round"></span>
                                                                            </label>     
                                                                        </div>
                                                                    </div>
                                                                </div>                                          
                                                            </div>
                                                        </div>
                                                <div class="modal-footer">
                                                         <button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="editarClienteTelefono();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- nueva direccion --}}
                                      <div class="modal fade" id="modal_telefono_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">CREAR TELEFONO</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive">
                                                                    <div>
                                                                       <h1>TELEFONO:</h1> 
                                                                        <input type="textbox"  class="form-control"  v-model="cliente_telefono_nuevo.telefono">
                                                                            {{-- @{{telefono.telefono}} --}}
                                                                        
                                                                    </div>
                                                                     <div>
                                                                        <h1>TIPO:</h1> 
                                                                        <select class="form-control" v-model="cliente_telefono_nuevo.tipo_telefono">
                                                                          <option value="Celular">Celular</option>
                                                                          <option value="Fijo" selected>Fijo</option>
                                                                        </select>
                                                                    </div>
                                                                    <div>
                                                                        <h1>PRINCIPAL</h1> 
                                                                        <div>
                                                                            <label class="switch">
                                                                            <input type="checkbox" checked v-model="cliente_telefono_nuevo.principal">
                                                                            <span class="slider round"></span>
                                                                            </label>     
                                                                        </div>
                                                                    </div>
                                                                    
                                                            </div>                                          
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                   --}}
                                                         <button type="button" data-dismiss="modal" class="btn btn-primary" v-on:click="guardarClienteTelefono();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>





                                    {{-- EMAIL MODALS --}}

                                    <!-- Modal  -->
                                    <div class="modal fade" id="modal_email_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">EDITAR EMAIL</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive" v-for="ce in cliente_email">
                                                                    <div>
                                                                       <h1>EMAIL:</h1> 
                                                                        <input type="text" class="form-control"  v-model="cliente_email_editar.email" value="@{{ce.email}}">
                                                                    </div>
                                                                    <div>
                                                                        <h1>PRINCIPAL</h1> 
                                                                        <div>
                                                                            <label class="switch">
                                                                            <input type="checkbox" checked v-model="cliente_email_editar.principal">
                                                                            <span class="slider round"></span>
                                                                            </label>     
                                                                        </div>
                                                                    </div>
                                                                </div>                                          
                                                            </div>
                                                        </div>
                                                <div class="modal-footer">
                                                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                   --}}
                                                         <button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="editarClienteEmail();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- nueva direccion --}}
                                      <div class="modal fade" id="modal_email_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">CREAR Email</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive">
                                                                    <div>
                                                                       <h1>Email:</h1> 
                                                                        <input  type="email" class="form-control"  v-model="cliente_email_nuevo.email">
                                                                    </div>
                                                                    {{--  <div>
                                                                        <h1>TIPO:</h1> 
                                                                        <select class="form-control" v-model="cliente_email_nuevo.tipo_email">
                                                                          <option value="Negocio">Negocio</option>
                                                                          <option value="Entrega">Entrega</option>
                                                                          <option value="Pago">Pago</option>
                                                                          <option value="Domicilio" selected>Domicilio</option>
                                                                        </select>
                                                                    </div> --}}
                                                                    <div>
                                                                        <h1>PRINCIPAL</h1> 
                                                                        <div>
                                                                            <label class="switch">
                                                                            <input type="checkbox" checked v-model="cliente_email_nuevo.principal">
                                                                            <span class="slider round"></span>
                                                                            </label>     
                                                                        </div>
                                                                    </div>
                                                                    
                                                            </div>                                          
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                   --}}
                                                         <button type="button" data-dismiss="modal" class="btn btn-primary" v-on:click="guardarClienteEmail();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" v-show="editar">
                                          <div>
                                        <label class="col-md-2 control-label">    &emsp; &emsp; DIRECCIONES</label>
                                          <span class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#crear_nueva_direccion">
                                                <i class="text-success"  aria-="true"> + NUEVO</i>
                                           </span>
                                    </div>

                                     <div class="ibox-content">
                                        <div class="project-list">
                                            <div class="row m-b-sm m-t-sm">
                                                <form class="form-horizontal">
                                                    <div class="col-md-6"  v-for="direccion in cliente_direcciones">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label class="col-md-12"> @{{direccion.tipo_direccion}}
                                                                    <span v-if="direccion.principal" class="fa fa-star text-warning">
                                                                    </span>
                                                                </label>

                                                            </div>
                                                                
                                                            <div class="col-md-8">
                                                                <div class="col-md-8">
                                                                    <label class="col-md-6 text-success"   v-else type="text" name="">@{{direccion.direccion}}</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <span class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#MostrarDetalles" @click.prevent="obtenerClienteDireccion(direccion.id_direccion);">
                                                                        <i class="fa fa-pencil"  aria-hidden="true"></i>
                                                                    </span>
                                                                    <span class="btn-bg btn-sm btn btn-default" title="Eliminar" @click.prevent="eliminarClienteDireccion(direccion.id_direccion);">
                                                                        <i class="fa fa-trash text-danger"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>

                                    <div>
                                        <label class="col-md-2 control-label">    &emsp; &emsp; TELEFONOS</label>
                                        <span class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modal_telefono_crear">
                                                <i class="text-success"  aria-="true"> + NUEVO</i>
                                           </span>
                                    </div>

                                     <div class="ibox-content">
                                        <div class="project-list">
                                            <div class="row m-b-sm m-t-sm">
                                              <form class="form-horizontal">
                                                    <div class="col-md-6"  v-for="telefono in cliente_telefonos">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label class="col-md-12"> @{{telefono.tipo_telefono}}                                                                             <span v-if="telefono.principal" class="fa fa-star text-warning">
                                                                        
                                                                    </span>
                                                                </label>
                                                            </div>
                                                                
                                                            <div class="col-md-8">
                                                                <div class="col-md-8">
                                                                    <label class="col-md-6 text-success"   v-else type="text" name="">@{{telefono.telefono}}</label>
                                                                    
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <span class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modal_telefono_editar" @click.prevent="obtenerClienteTelefono(telefono.id_telefono);">
                                                                        <i class="fa fa-pencil"  aria-hidden="true"></i>
                                                                    </span>
                                                                    <span class="btn-bg btn-sm btn btn-default" title="Eliminar" @click.prevent="eliminarClienteTelefono(telefono.id_telefono);">
                                                                        <i class="fa fa-trash text-danger"></i>
                                                                    </span>
                                                                </div>
                                                                  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <label class="col-md-2 control-label">    &emsp; &emsp; EMAILS</label>
                                           <span class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modal_email_crear">
                                                <i class="text-success"  aria-="true"> + NUEVO</i>
                                           </span>
                                    </div>

                                     <div class="ibox-content">
                                        <div class="project-list">
                                            <div class="row m-b-sm m-t-sm">
                                               <form class="form-horizontal">
                                                    <div class="col-md-6"  v-for="email in cliente_emails">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <div class="col-md-8">
                                                                    <label class="col-md-6 text-success"   v-else type="text" >@{{email.email}}
                                                                        <span v-if="email.principal" class="fa fa-star text-warning">
                                                                    </span>
                                                                </label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                      <span class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modal_email_editar" @click.prevent="obtenerClienteEmail(email.id_email);">
                                                                        <i class="fa fa-pencil"  aria-hidden="true"></i>
                                                                    </span>
                                                                    <span class="btn-bg btn-sm btn btn-default" title="Eliminar" @click.prevent="eliminarClienteEmail(email.id_email);">
                                                                        <i class="fa fa-trash text-danger"></i>
                                                                    </span>
                                                                </div>
                                                                  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endsection
    <script src="{{ asset('js/app/sistema_cliente_datos.js') }}"></script>

