@extends('layouts.app') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>PRODUCTOS</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>MANTENIMIENTO</a>
            </li>
            <li class="active">
                <strong>PRODUCTOS</strong>
            </li>
        </ol>
    </div>
</div>
    <div class="row">
        <div  class=""> {{-- col-lg-12 --}} 
               <div class="ibox-title">
                        <h2>CLIENTE</h2>
                    </div>

            <div class="ibox-content">
            </div>
        </div>
    </div>
    <div class="row">  

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class=""> {{-- well well-sm --}}
                    <form class="form-horizontal" method="post">
                            <br>
                            <br>
                            <br>
                            <div class="form-group">
                                <label  for="codigo"  class="col-md-1 col-md-offset-2 control-label text-center">Codigo</label>
                           
                                <div class="col-md-8">
                                    <input id="codigo"  name="codigo" type="text"  placeholder="100001" class="form-control" v-model="cliente.id_cli">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nombre" class="col-md-1 col-md-offset-2 control-label text-center">Nombre:</label>

                                <div class="col-md-8">
                                    <input id="nombre" name="nombre" type="text" placeholder="Juan Mendoza" class="form-control" v-model="cliente.nombre">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="direccion" class="col-md-1 col-md-offset-2 control-label text-center">Direccion:</label>

                                <div class="col-md-8">
                                    <input id="direccion" name="direccion" type="text" placeholder="Calle cruz verde 410" class="form-control" v-model="cliente.direccion">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tipoCliente" class="col-md-1 col-md-offset-2 control-label text-center">Tipo de Cliente:</label>

                                 <div class="col-md-2">
                                    <select class="form-control " v-model="cliente.tipoCliente">
                                        <option selected value="Mayorista">Mayorista</option>
                                        <option value="Minorista">Minorista</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <a class="btn btn-primary" v-on:click="guardarCliente();">GUARDAR</a>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
<script src="{{ asset('js/app/sistema_cliente.js') }}"></script>