@extends('layouts.app') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Clientes</h2>
		<ol class="breadcrumb">
			<li>
				<a href="">INICIO</a>
			</li>
			<li>
				<a>MANTENIMIENTOS</a>
			</li>
			<li class="active">
				<strong>CLIENTES</strong>
			</li>
		</ol>
	</div>
</div>
<div id="clientes">
	{{-- Listado --}}
	  <div class="row">
        <div  class=""> {{-- col-lg-12 --}} 
               <div class="ibox-title">
                        <h5>CLIENTES</h5>
                    </div>
 <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class="ibox-title">
                              <div class="col-md-9">
                                    <span>Vendedores que cumplan 
                                        <input type="button" name="btnAlgunas" value="Algunas">
                                        <input type="button" name="btlTodas" value="todas">
                                        de las siguientes:
                                    </span>
                                </div>
                                <div class="row m-b-sm m-t-sm">
                                    <a href="/vendedor/create" class="btn btn-primary">
                                        Nuevo Vendedor
                                    </a>
                                </div>
                    </div>  

                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <select>
                                          <option value="1">Nombre</option>
                                          <option value="2">Apellido</option>
                                          <option value="3">DNI</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group">
                                    <select>
                                          <option value="1">Contiene</option>
                                          <option value="2">Diferente a </option>
                                          <option value="3">Igual a </option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="input-group">
                                    <span class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="btn btn-default">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                    <span class="btn btn-default">
                                        <i class="fa fa-retweet"></i>
                                    </span>
                                    <span class="btn btn-default">
                                        <i class="fa fa-filter"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <div class="modal fade" data-backdrop="static" data-keyboard="false" id="importarFromCSV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button @click.prevent="defaultData();" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    {{-- <span aria-hidden="true">&times;</span> --}}
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title" style="font-size:20pt"  id="myModalLabel">IMPORTAR DATOS</h4>
            </div>
            <div class="modal-body">
         
       
                <form class="form-horizontal" method="post" id="formularioCSV" enctype="multipart/form-data" >
                        <div class="form-group">
                            <label class="col-md-1 control-label">ARCHIVO CSV </label>
                            <div class="fileinput fileinput-new input-group col-md-3" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <input type="file" name="fileCSV" accept=".csv">
                                </span>
                            </div>
                        </div>
                </form>
            </div>
            {{-- </div> --}}
            <div class="modal-footer">
                <div class="outer_div">                                          
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="">    
                                <tr  class="warning">
                                    <th>Cliente</th>
                                    <th>Direccion</th>  
                                    <th>Tipo Cliente</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dp in dataProductsFromFile">
                                    <td class="project-title">
                                        @{{dp.nombre}}
                                    </td>
                                    <td class="project-title">
                                        @{{dp.direccion}}
                                    </td>
                                    <td class="project-title">
                                        @{{dp.tipoCliente}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-9 col-md-3">
                        <span class="btn btn-md btn-primary" @click.prevent="saveLoadDataFromFile();">GUARDAR EN DB</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="importarFromTXT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button @click.prevent="defaultData();" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    {{-- <span aria-hidden="true">&times;</span> --}}
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title" style="font-size:20pt"  id="myModalLabel">IMPORTAR DATOS</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="formularioTXT" enctype="multipart/form-data" >
                        <div class="form-group">
                            <label class="col-md-1 control-label">ARCHIVO TXT </label>
                            <div class="fileinput fileinput-new input-group col-md-3" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <input type="file" name="fileTXT" accept=".txt">
                                </span>
                            </div>
                        </div>
                </form>
            </div>
            {{-- </div> --}}
            <div class="modal-footer">
                <div class="outer_div">                                          
                     <div class="table-responsive">
                        <table class="table">
                            <thead class="">    
                                <tr  class="warning">
                                    <th>Cliente</th>
                                    <th>Direccion</th>  
                                    <th>Tipo Cliente</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dp in dataProductsFromFile">
                                    <td class="project-title">
                                        @{{dp.nombre}}
                                    </td>
                                    <td class="project-title">
                                        @{{dp.direccion}}
                                    </td>
                                    <td class="project-title">
                                        @{{dp.tipoCliente}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-9 col-md-3">
                        <span class="btn btn-md btn-primary" @click.prevent="saveLoadDataFromFile();">GUARDAR EN DB</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>


	<div>
		{{-- Detalles --}}
		<div class="row" v-show="nuevo">
			<div class="wrapper wrapper-content">
				<div class="row animated fadeInRight">
					<div class="col-lg-12">
		                <div class="wrapper wrapper-content animated fadeInUp">
		                    <div class="ibox">
		                        <div class="ibox-content">
		                            <div class="project-list">
                                         <div class="row m-b-sm m-t-sm">
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="btn btn-default">
                                                      <input type="checkbox" name="" value="">Seleccionar todo
                                                    </div>                                                        
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                 <div class="btn btn-default">
                                                    <i class="fa fa-file"> Importar</i>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                
                                                 <div class="btn btn-default">
                                                    <i class="fa fa-file"> Exportar</i>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                    Ordenado por:
                                                    <select>
                                                          <option value="1">Nombre</option>
                                                          <option value="2">Apellido</option>
                                                          <option value="3">DNI</option>
                                                    </select>
                                            </div>
                                        </div>

		                                <table class="table table-hover">
		                                    <thead>
                                                 <td class="project-title">
                                                        <strong></strong>
                                                    </td>
			                                    <td class="project-title">
			                                        <strong>CODIGO</strong>
			                                    </td>
			                                    <td class="project-title">
			                                        <strong>NOMBRE</strong>
			                                    </td>
                                                <td class="project-title">
                                                    <strong>ESTADO</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>GIRO</strong>
                                                </td>
			                                    <td class="project-title">
			                                        <strong>DIRECCION</strong>
			                                    </td>
			                                    <td class="project-title">
			                                        <strong>TELEFONO</strong>
			                                    </td>
		                                    </thead>
		                                    <tbody>
			                                    <tr v-for="c in clientes">
                                                    <td class="project-title" >
                                                            <input type="checkbox" name="">
                                                    </td>
			                                        <td class="project-title">
                                                            <a href="{{ url('cliente/cliente-datos/')}}/@{{c.id}}">
                                                                @{{c.codigo}}
                                                            </a>
			                                        </td>
			                                        <td class="project-title">
                                                          <a href="{{ url('cliente/cliente-datos/')}}/@{{c.id}}">
                                                                @{{c.nombre}}
                                                            </a>
			                                        </td>
			                                        <td class="project-title">
                                                          <a href="{{ url('cliente/cliente-datos/')}}/@{{c.id}}">
                                                                {{-- @{{c.estado}} --}}
                                                                <label class="label label-success">VISITADO </label>
                                                            </a>
			                                        </td>
                                                    <td class="project-title">
                                                          <a href="{{ url('cliente/cliente-datos/')}}/@{{c.id}}">
                                                                @{{c.giro}}
                                                            </a>
                                                    </td>
			                                        <td class="project-title">
                                                          <a href="{{ url('cliente/cliente-datos/')}}/@{{c.id}}">
                                                                @{{c.cliente_direccion}}
                                                            </a>
			                                        </td>
    			                                    <td class="project-title">
                                                          <a href="{{ url('cliente/cliente-datos/')}}/@{{c.id}}">
                                                                @{{c.cliente_telefono}}
                                                            </a>
                                                	</td>
                                                	
			                                    </tr>
		                                    </tbody>
		                                </table>

		                                 <nav>
			                            	<ul class="pagination">
			                            		<li v-if="pagination.current_page > 1">
			                            			<a href="#" @click.prevent="changePage(pagination.current_page - 1);">
			                            				<span>Atras</span>
			                            			</a>
			                            		</li>
			                            		<li v-for="page in pagesNumber" v-bind:class="[page == isActive ? 'active' : '']">
			                            			<a href="#" @click.prevent="changePage(page);">
			                            				@{{ page }}
			                            			</a>
			                            		</li>
			                            		<li v-if="pagination.current_page < pagination.last_page">
			                            			<a href="#" @click.prevent="changePage(pagination.current_page + 1);">
			                            				<span>Siguiente</span>
			                            			</a>
			                            		</li>

			                            	</ul>
			                            </nav>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection
<script src="{{ asset('js/app/sistema_cliente.js') }}"></script>