@extends('layouts.app') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>EXPORTAR</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>HERRAMIENTAS</a>
            </li>
            <li class="active">
                <strong>EXPORTAR</strong>
            </li>
        </ol>
    </div>
</div>
<div id="exportar">
    <div class="row">
         <div  class=""> {{-- col-lg-12 --}} 
               <div class="ibox-title">
                        <h5>CLIENTES</h5>
                    </div>

            <div class="ibox-content">
                <div class="row m-b-sm m-t-sm">

                    <div class="col-md-3">
                        <label>TABLA</label>
                        <select class="form-control" v-model="correntista.tipdoc_id_tipdoc">
                            <option value="tbl_pro">PRODUCTOS</option>
                            <option value="tbl_cli">CLIENTES</option>
                            <option value="tbl_pro">PEDIDOS</option>
                            <option value="tbl_pro">DETALLES</option>
                            <option value="tbl_pro">NO PEDIDOS</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>SEPARADOR</label>
                        <select class="form-control" >
                            <option value="1">";"</option>
                            <option value="2">"|"</option>
                        </select>
                    </div>
                     <div class="col-md-3">
                        <label>TIPO DE DOCUMENTO</label>
                        <select class="form-control" >
                            <option value="1">TXT</option>
                            <option value="2">CSV</option>
                        </select>
                    </div>

                </div>

                <div class="">
                    
                    <div class="">
                        <div class="row m-b-sm m-t-sm col-md-offset-1">
                            <div class="col-md-2">
                                    {{-- <span class="btn btn-md btn-primary" @click.prevent="descargarExcel();">EXPORTAR A EXCEL</span> --}}
                                <a href="{{ route('exportarTabla')  }}" class="btn btn-info" >Exportar a TXT</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{ asset('js/app/sistema_pedido.js') }}"></script>

