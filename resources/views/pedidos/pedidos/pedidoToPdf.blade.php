
<!DOCTYPE html>
<html>
<head>
  <title>
    PEDIDOS     
  </title>
     <style>
        #meta {
            margin-top: 1px;
            width: 300px;
            /*float: right;*/
        }
        table {
           width: 100%;
           border: 1px solid #000;
        }
        th, td {
           width: 25%;
           text-align: left;
           vertical-align: top;
           border: 1px solid #000;
           border-collapse: collapse;
           padding: 0.3em;
           caption-side: bottom;
           background-color: #086E62;
        }
        tr, td {
           width: 25%;
           text-align: left;
           vertical-align: top;
           border: 1px solid #000;
           border-collapse: collapse;
           padding: 0.3em;
           caption-side: bottom;
           background-color: #BAE1F5;
        }
        caption {
           padding: 0.3em;
           color: #fff;
            background: #000;
        }
        th {
           background: #086E62;
        }
        .titulo-pedido{
           text-align: center;
        }
        .right{
            float: right;
        }
        </style>
</head>
<body>
    <div class="">
        <div class="">
             <div class="col-lg-12">
                <div class="titulo-pedido">   
                    <div class="">
                         <strong class="hijo" ><label >PEDIDO {{$pedido[0]->codigo }}</label></strong>
                    </div>
                </div>
            </div>
            <br>

        </div>
         @foreach ($pedido as $p)
            <div class="">
                <div class="">
                    <strong>
                        <label>
                            CLIENTE:
                        </label>
                        <label class=""> {{$p->nombre_cliente}} {{$p->apellido_cliente}}</label>

                    </strong>
                    <br>
                    <strong>
                        <label>
                            DIRECCION:
                        </label>
                        <label class=""> {{$direccion_principal[0]->direccion}}</label>
                    </strong>
                </div>
{{-- 
                <table id="meta">
                    <tbody>
                        <tr>
                            <td>Numero</td>
                            <td>asdsada</td>
                        </tr>
                         <tr>
                            <td>Fecha</td>
                            <td>asdsasda</td>
                        </tr>
                         <tr>
                            <td>Vendedor</td>
                            <td>{{$p->nombre_vendedor}}</td>
                        </tr>
                    </tbody>
                </table> --}}
                    <div>
                        <strong><label>Numero:</label></strong>
                        <label class="">{{$pedido[0]->codigo}}</label>
                    </div> 
                    <div>
                        <strong><label>Fecha:</label></strong>
                        <label class=""> {{$p->fecha_inicio}}</label>
                    </div> 
                    <div>
                        <strong><label>Hora:</label></strong>
                        <label class="">{{$p->hora_inicio}}</label>
                    </div> 
                    <div>
                        <strong><label>Monto Total:</label></strong>
                        <label class="">{{$p->monto_total}}</label>
                    </div>
                     <div>
                        <strong><label>Vendedor: <label></strong>
                        <label class="">{{$p->nombre_vendedor}}</label>
                    </div>
                </div>
        @endforeach

        <br>
        <table ">
            <thead class="">    
                <tr  class="">
                    <th>COD. PRO</th>
                    <th>DESCRIPCION</th>
                    <th>CANTIDAD</th>  
                    <th>PRECIO UNI</th>  
                    <th>SUBTOTAL</th>  
                </tr>
            </thead>
            <tbody>
                @foreach($detalles as $d)
                    <tr>
                        <td class="">
                            {{$d->id_producto}}
                        </td>
                        <td class="">
                            {{$d->nombre_producto}}
                        </td>
                        <td class="">
                            {{$d->cantidad}}
                        </td>
                         <td class="">
                            {{$d->precio_unitario}}
                        </td>
                        <td class="">
                            {{$d->subtotal}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <div class="right">
            <div class="">
                <label>SUBTOTAL: {{$pedido[0]->monto_total}}</label>
            </div>
            <div class="">
                <label>DESCUENTO: {{$detalles[0]->descuento}}%</label>
            </div>
            <div class="">
                <label>TOTAL: {{$pedido[0]->monto_total}}</label>
            </div>
        </div>
    </div>
</body>
</html>