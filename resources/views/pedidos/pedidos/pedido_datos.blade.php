@extends('layouts.app') 
@section('content')
    <div id="vendedores">
      {{--    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <div class="form-group">   
                            <div class="col-md-8">
                                <h4 class="" >PEDIDO {{$pedido[0]->codigo }}</h4>
                            </div>
                            <div class="col-md-4">
                                <a  href="/pedidos" class="btn btn-default"><i class="fa fa-arrow-left"></i></a>
                                <a  href="#" class="btn btn-default"><i class="fa fa-envelope"></i></a>
                                <a  href="#" class="btn btn-default"><i class="fa fa-file-pdf-o"></i></a>
                                <a  href="#" class="btn btn-default"><i class="fa fa-download"></i></a>
                                <br>
                            </div>
                        </div>
                    </div>
                     
                        <div class="col-lg-1">
                            <span  @click.prevent="editarDatos();" class="btn btn-default"><i class="fa fa-pencil"></i></span>
                        </div>
                        <div class="col-lg-1">
                            <span @click.prevent="eliminarCliente(cliente[0].id);" class="btn btn-danger"> <i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
                
        <div class="">
            @foreach ($pedido as $p)
                <select hidden v-model="id_pedido">
                  <option selected>{{$p->id}}</option>
                </select>

            @endforeach
        </div>

              <div class="row" > 
                <div class="wrapper wrapper-content">
                    <div class="row animated fadeInRight">
                        <div class="col-lg-12">
                            <div class="wrapper wrapper-content animated fadeInUp">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <div class="col-lg-12">
                                            <div class="form-group">   
                                                <div class="col-md-8">
                                                    <strong class="lead" ><label >PEDIDO {{$pedido[0]->codigo }}</label></strong>
                                                {{-- <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">PEDIDO</h4> --}}

                                                </div>
                                                <div class="col-md-4">
                                                    <div class="col-lg-1">
                                                        <a  href="/pedidos" class="btn btn-default"><i class="fa fa-arrow-left"></i></a>
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <a  href="#" class="btn btn-default"><i class="fa fa-envelope"></i></a>
                                                    </div>
                                                    <div class="col-lg-1">
                                                    <form  method="POST" action="{{url('/pedidos/pedido-topdf')}}">
                                                        
                                                            <button type="submit" class="btn btn-default"> 
                                                                <i class="fa fa-file-pdf-o">
                                                                </i>
                                                            </button>
                                                        <input type="hidden" name="id_pedido" value="@{{id_pedido}}">
                                                    </form>
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <a  href="#" class="btn btn-default"><i class="fa fa-download"></i></a>
                                                    </div>
                                                   

                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">                                             
                                                <div class="" v-for="p in pedido">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                              <div class="col-md-12">
                                                                    <label class="col-md-3 control-label"> @{{p.nombre_cliente}} @{{p.apellido_cliente}}</label>
                                                                </div>
                                                                <div class="col-md-12">
                                                                <label class="col-md-3 control-label"> @{{cliente_direccion_principal[0].direccion}}</label>
                                                                </div>
                                                            
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label>Numero:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <label class="col-md-12 control-label">{{$pedido[0]->codigo}}</label>
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label>Fecha:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <label class="col-md-12 control-label"> @{{p.fecha_inicio}}</label>
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label>Hora:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <label class="col-md-12 control-label">@{{p.hora_inicio}}</label>
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label>Monto Total:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <label class="col-md-12 control-label">@{{p.monto_total}}</label>
                                                                </div>
                                                            </div>
                                                             <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label>Vendedor:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <label class="col-md-12 control-label">@{{p.nombre_vendedor}}</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                     {{--    <label class="col-md-3 control-label">Cliente: @{{p.id_cliente}}</label>
                                                        <label class="col-md-3 control-label">Cliente: @{{p.nombre_cliente}}</label>
                                                        <label class="col-md-6 control-label">Fecha y Hora: @{{p.fecha_inicio}} @{{p.hora_inicio}}</label>

                                                    </div>
                                                    <div class="row">
                                                       <label class="col-md-3 control-label">Vendedor: @{{p.id_vendedor}}</label>
                                                        <label class="col-md-3 control-label">Vendedor: @{{p.nombre_vendedor}}</label>
                                                        <label class="col-md-6 control-label">Monto Total: S/ @{{p.monto_total}}</label> --}}
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                   --}}
                                        <div class="outer_div">                                          
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead class="">    
                                                        <tr  class="warning">
                                                            <th>COD. PRO</th>
                                                            <th>DESCRIPCION</th>
                                                            <th>CANTIDAD</th>  
                                                            <th>PRECIO UNI</th>  
                                                            <th>SUBTOTAL</th>  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="d in detalles">
                                                            <td class="project-title">
                                                                @{{d.id_producto}} {{-- c.codigo --}}
                                                            </td>
                                                            <td class="project-title">
                                                                @{{d.nombre_producto}}
                                                            </td>
                                                            <td class="project-title">
                                                                @{{d.cantidad}}
                                                            </td>
                                                             <td class="project-title">
                                                                @{{d.precio_unitario}}
                                                            </td>
                                                            <td class="project-title">
                                                                @{{d.subtotal}}
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>SUBTOTAL: @{{pedido[0].monto_total}}</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>DESCUENTO: @{{detalles[0].descuento}}%</label>
                                                        {{-- <label>DESCUENTO: @{{pedido[0].descuento}}%</label> --}}

                                                    </div>
                                                    <div class="form-group">
                                                        <label>TOTAL: @{{pedido[0].monto_total}}</label>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endsection
    <script src="{{ asset('js/app/sistema_pedido_datos.js') }}"></script>

