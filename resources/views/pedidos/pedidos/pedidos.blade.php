@extends('layouts.app') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>PEDIDOS</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>REPORTES</a>
            </li>
            <li class="active">
                <strong>PEDIDOS</strong>
            </li>
        </ol>
    </div>
</div>
<div id="pedidos">
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class="ibox-title">
                            <h5>PEDIDOS</h5>
                    </div>  
                    <div class="wrapper wrapper-content animated fadeInUp">
                        <div class="ibox">
                            <div class="ibox-title">
                                      <div class="col-md-9">
                                            <span>Pedidos que cumplan 
                                                <input type="button" name="btnAlgunas" value="Algunas">
                                                <input type="button" name="btlTodas" value="todas">
                                                de las siguientes:
                                            </span>
                                        </div>
                                        <div class="row m-b-sm m-t-sm">
                                            <a href="/pedido/create" class="btn btn-primary">
                                                Nuevo Pedido
                                            </a>
                                        </div>
                            </div>  

                            <div class="ibox-content">
                                <div class="row m-b-sm m-t-sm">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <select>
                                                  <option value="1">Nombre</option>
                                                  <option value="2">Apellido</option>
                                                  <option value="3">DNI</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <select>
                                                  <option value="1">Contiene</option>
                                                  <option value="2">Diferente a </option>
                                                  <option value="3">Igual a </option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="input-group">
                                            <span class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <span class="btn btn-default">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                            <span class="btn btn-default">
                                                <i class="fa fa-retweet"></i>
                                            </span>
                                            <span class="btn btn-default">
                                                <i class="fa fa-filter"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
{{-- 
                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Vendedor" class="input-sm form-control">
                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Cliente" class="input-sm form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div id="fecha" class="input-group date">
                                   
                                    <input type="text" class="form-control" placeholder="19/08/2018" />
                                     <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Codigo Vendedor" class="input-sm form-control">
                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Codigo Cliente" class="input-sm form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <span class="btn btn-md btn-primary" v-on:click="">BUSCAR</span>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ route('downloadExcel')  }}" class="btn btn-info" >Exportar a excel</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn-info" >Exportar a pdf</a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ route('exportPedidosToTXT')  }}" class="btn btn-info" >Exportar PEDIDOS a TXT</a>
                            </div>
                            <div class="col-md-3">
                                    <a href="{{ route('exportDetallesToTXT')  }}" class="btn btn-info" >Exportar DETALLES a TXT</a>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

        {{-- Detalles --}}


<!-- Modal Busca Producto-->

<div class="modal fade" id="MostrarDetalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    {{-- <span aria-hidden="true">&times;</span> --}}
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">Detalles de pedido</h4>
            </div>
            <div class="modal-body">
                    <div class="form-group">                                             
                        <div class="" v-for="p in pedido">
                            <div class="row">
                                <label class="col-md-3 control-label">Cliente: @{{p.id_cliente}}</label>
                                <label class="col-md-3 control-label">Cliente: @{{p.nombre_cliente}}</label>
                                <label class="col-md-6 control-label">Fecha y Hora: @{{p.fecha_inicio}} @{{p.hora_inicio}}</label>

                            </div>
                            <div class="row">
                                <label class="col-md-3 control-label">Vendedor: @{{p.id_vendedor}}</label>
                                <label class="col-md-3 control-label">Vendedor: @{{p.nombre_vendedor}}</label>
                                <label class="col-md-6 control-label">Monto Total: S/ @{{p.monto_total}}</label>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                   --}}
                <div class="outer_div">                                          
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="">    
                                <tr  class="warning">
                                    <th>Cod. Producto</th>
                                    <th>Producto</th>
                                    <th>Precio Unit.</th>  
                                    <th>Cantidad</th>  
                                    <th>SubTotal</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="d in detalles">
                                    <td class="project-title">
                                        @{{d.id_producto}} {{-- c.codigo --}}
                                    </td>
                                    <td class="project-title">
                                        @{{d.nombre_producto}}
                                    </td>
                                    <td class="project-title">
                                        @{{d.precio_unitario}}
                                    </td>
                                     <td class="project-title">
                                        @{{d.cantidad}}
                                    </td>
                                    <td class="project-title">
                                        @{{d.subtotal}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div>
        <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">

                                        <table class="table table-hover">
                                            <thead>
                                                <td class="project-title">
                                                    <strong>COD. VENDEDOR</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>VENDEDOR</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>COD. CLIENTE</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>CLIENTE</strong>
                                                </td>

                                                <td class="project-title">
                                                    <strong>FECHA</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>HORA</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>TOTAL</strong>
                                                </td>
                                                <td class="project-title">
                                                    {{-- <strong>SELECCIONAR</strong> --}}
                                                </td>
                                            </thead>
                                            <tbody>
                                                <tr v-for="p in pedidos">
                                                    <td class="project-title" >
                                                        <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                                {{-- @{{c.codigo}} --}}
                                                            @{{p.id_vendedor}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                         <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                            @{{p.nombre_vendedor}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                         <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                            @{{p.id_cliente}}
                                                        </a>
                                                    </td>
                                                     <td class="project-title">
                                                         <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                            @{{p.nombre_cliente}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                        {{-- Carbon::createFromFormat('Y-m-d H', '1975-05-21 22')->toDateTimeString();@{{p.fechaHora}}   --}}
                                                        {{-- c.codigo --}}
                                                         <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                            @{{p.fecha_inicio | formatDate}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                         <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                            @{{p.hora_inicio }} {{-- | formatHour --}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                         <a href="{{ url('pedidos/pedido-datos/')}}/@{{p.id}}">
                                                            @{{p.monto_total}}
                                                        </a>
                                                    </td>

                                                    <td class="project-title">
                                                       {{--  <input type="checkbox"  v-model="p.checked" class="btn btn-info"> --}}
                                                        <div class="col-md-12">
                                                            {{--  <input type="checkbox" class="text-center" v-model="p.checked"  data-toggle="modal" data-target="#MostrarDetalles">
                                                            </input> --}}

                                                            <button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="modal" data-target="#MostrarDetalles"  @click.prevent="obtenerPedidoDetalles(p.id); ">
                                                                <span class="fa fa-eye fa-lg" aria-hidden="true"></span>
                                                            </button>

                                                            {{-- <button id="btnAbrirModal" type="button" class="btn btn-info" aria-label="Left Align" data-toggle="modal" data-target="#MostrarDetalles">

                                                            </button> --}}

                                                        </div>
                                                           
                                                    </td>


                                                </tr>
                                            </tbody>
                                        </table>
                                          <nav>
                                            <ul class="pagination">
                                                <li v-if="pagination.current_page > 1">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page - 1);">
                                                        <span>Atras</span>
                                                    </a>
                                                </li>
                                                <li v-for="page in pagesNumber" v-bind:class="[page == isActive ? 'active' : '']">
                                                    <a href="#" @click.prevent="changePage(page);">
                                                        @{{ page }}
                                                    </a>
                                                </li>
                                                <li v-if="pagination.current_page < pagination.last_page">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page + 1);">
                                                        <span>Siguiente</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
<script src="{{ asset('js/app/sistema_pedido.js') }}"></script>

