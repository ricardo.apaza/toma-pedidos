@extends('layouts.app') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>NO PEDIDOS</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>REPORTES</a>
            </li>
            <li class="active">
                <strong>NO PEDIDOS</strong>
            </li>
        </ol>
    </div>
</div>
<div id="pedidos">
    {{-- Listado --}}
   <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class="ibox-title">
                            <h5>BUSCAR</h5>
                    </div>

                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Vendedor" class="input-sm form-control">
                                    
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Cliente" class="input-sm form-control">
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div id="fecha" class="input-group date">
                                   
                                    <input type="text" class="form-control" placeholder="19/08/2018" />
                                     <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>


                         
                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Codigo Vendedor" class="input-sm form-control">
                                    
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="input">
                                    <input type="text" placeholder="Codigo Cliente" class="input-sm form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                    <span class="btn btn-md btn-primary" v-on:click="">BUSCAR</span>
                            </div>
                           

                        </div>
                         <div class="col-md-2">
                                    {{-- <span class="btn btn-md btn-primary" @click.prevent="descargarExcel();">EXPORTAR A EXCEL</span> --}}
                               <a href="{{ route('downloadExcel')  }}" class="btn btn-info" >Exportar a excel</a>
                        </div>
                        <div class="col-md-2">
                                    {{-- <span class="btn btn-md btn-primary" @click.prevent="descargarExcel();">EXPORTAR A EXCEL</span> --}}
                                <a href="#" class="btn btn-info" >Exportar a pdf</a>
                        </div>
                        <div class="col-md-2">
                                    {{-- <span class="btn btn-md btn-primary" @click.prevent="descargarExcel();">EXPORTAR A EXCEL</span> --}}
                                <a href="{{ route('exportNoPedidosToTXT')  }}" class="btn btn-info" >Exportar NO PEDIDOS a TXT</a>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>

        {{-- Detalles --}}
<div>
        <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">

                                        <table class="table table-hover">
                                            <thead>
                                                <td class="project-title">
                                                    <strong>COD. VENDEDOR</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>VENDEDOR</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>COD. CLIENTE</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>CLIENTE</strong>
                                                </td>

                                                <td class="project-title">
                                                    <strong>FECHA</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>HORA</strong>
                                                </td>
                                               
                                                <td class="project-title">
                                                    <strong>MOTIVO</strong>
                                                </td>
                                            </thead>
                                            <tbody>
                                                <tr v-for="np in nopedidos">
                                                    <td class="project-title">
                                                        @{{np.id_vendedor}} {{-- c.codigo --}}
                                                    </td>
                                                    <td class="project-title">
                                                        @{{np.nombre_vendedor}}
                                                    </td>
                                                    <td class="project-title">
                                                        @{{np.id_cliente}}
                                                    </td>
                                                     <td class="project-title">
                                                        @{{np.nombre_cliente}}
                                                    </td>
                                                    <td class="project-title">
                                                        @{{np.fecha_inicio}} {{-- c.codigo --}}
                                                    </td>
                                                    <td class="project-title">
                                                        @{{np.hora_inicio}}
                                                    </td>

                                                    <td class="project-title">
                                                        @{{np.motivo_no_pedido}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                       <nav>
                                            <ul class="pagination">
                                                <li v-if="pagination.current_page > 1">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page - 1);">
                                                        <span>Atras</span>
                                                    </a>
                                                </li>
                                                <li v-for="page in pagesNumber" v-bind:class="[page == isActive ? 'active' : '']">
                                                    <a href="#" @click.prevent="changePage(page);">
                                                        @{{ page }}
                                                    </a>
                                                </li>
                                                <li v-if="pagination.current_page < pagination.last_page">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page + 1);">
                                                        <span>Siguiente</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </nav>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



@endsection
<script src="{{ asset('js/app/sistema_no_pedido.js') }}"></script>