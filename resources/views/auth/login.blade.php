
@extends('layouts.public')


@section('content')
    <div class="container">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <!--<h1 class="logo-name text-center">LOG</h1>-->
                    <h1>
                        <span>
                            <img  alt="image" class="img-circle" width="80" height="80" src="{{ url('/img/lock_color.png') }}">
                        </span>
                    </h1>
                </div>

                <h3>Bienvenido a TOMA PEDIDO</h3>
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }} ">
                    @csrf
               <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" placeholder="Código de usuario" type="text"  name="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required autofocus>


                    @if ($errors->has('email'))
                        <span class="help-block" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" placeholder="Contraseña" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                  <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>
                    <!--
                    <a href="#">
                        <small>Olvidaste la contraseña?</small>
                    </a>
                    <a class="btn btn-sm btn-white btn-block" href="register.html">Crear una cuenta</a>
                </form>-->
                <p class="m-t">
                    <small>Developer &copy; 2018</small>
                </p>
            </div>
        </div>
    </div>
@endsection