@extends('layouts.app') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Usuarios</h2>
		<ol class="breadcrumb">
			<li>
				<a href="">Inicio</a>
			</li>
			<li class="active">
				<strong>Usuarios</strong>
			</li>

		</ol>
	</div>
</div>

<div id="usuarios">
<div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Listado de Usuarios</h5>
                        <div class="ibox-tools">
                            <a href="#" class="btn btn-primary btn-xs" v-on:click="nuevaUsuario();">Crear un nuevo usuario</a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                          {{--   <div class="col-md-1">
                                <button type="button" id="loading-example-btn" class="btn btn-white btn-sm">
                                    <i class="fa fa-refresh"></i> Refrescar</button>
                            </div> --}}
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" placeholder="Nombre" v-model="name" class="input-sm form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="input-group">
                                    <input type="text" placeholder="Codigo" v-model="codigo" class="input-sm form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" placeholder="Direccion" v-model="direccion"class="input-sm form-control">
                                </div>
                            </div>
                        </div>

                        <div class="project-list">

                            <table class="table table-hover">
                                <thead>
                                	<td class="project-title">
                                        <strong>Codigo</strong>
                                    </td>
                                    <td class="project-title">
                                        <strong>Usuario</strong>
                                    </td>
                                    <td class="project-title">
                                        <strong>Email</strong>
                                    </td>

                                    <td class="project-actions">
                                    </td>
                                </thead>
                                <tbody>
                                    <tr v-for="u in searchUserByName">

   										<td>
                                            @{{u.id}}
                                        </td>
                                        <td >
                                        	@{{u.name}}

                                        </td>
                                        <td>
                                            @{{u.email}}
                                        </td>

                                        <td class="project-actions">

                                            <span class="btn btn-warning btn-sm" title="Editar" @click.prevent="obtenerUsuario(u);">
                                                <i class="fa fa-pencil"></i>
                                            </span>
                                            <span class="btn btn-danger btn-sm" title="Eliminar" @click.prevent="eliminarUsuario(u.id);">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                            <nav>
                            	<ul class="pagination">
                            		<li v-if="pagination.current_page > 1">
                            			<a href="#" @click.prevent="changePage(pagination.current_page - 1);">
                            				<span>Atras</span>
                            			</a>
                            		</li>
                            		<li v-for="page in pagesNumber" v-bind:class="[page == isActive ? 'active' : '']">
                            			<a href="#" @click.prevent="changePage(page);">
                            				@{{ page }}
                            			</a>
                            		</li>
                            		<li v-if="pagination.current_page < pagination.last_page">
                            			<a href="#" @click.prevent="changePage(pagination.current_page + 1);">
                            				<span>Siguiente</span>
                            			</a>
                            		</li>

                            	</ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row" v-show="nuevo">
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-md-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab-principal">Principal</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="tab-content">
                                <div id="tab-principal" class="tab-pane active">
                                    <div class="panel-body">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-1 control-label">Usuario</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="Usuario" v-model="usuario.name"/>
                                                </div>
                                                <label class="col-md-1 control-label">Contraseña</label>
                                                <div class="col-md-3">
                                                    <input  type="password" class="form-control" placeholder="Contraseña"  v-model="usuario.password"/>
                                                </div>
                                                <label class="col-md-1 control-label">Email</label>
                                                <div class="col-md-3">
                                                    <input  type="text" class="form-control" placeholder="Email"  v-model="usuario.email"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-offset-9 col-md-3">
                                                    <span class="btn btn-md btn-primary" v-on:click="guardarUsuario()">Guardar</span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
<script src="{{ asset('js/app/sistema_usuario.js') }}"></script>
