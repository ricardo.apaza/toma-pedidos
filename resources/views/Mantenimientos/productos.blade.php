@extends('layouts.app') @section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>PRODUCTOS</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>MANTENIMIENTO</a>
            </li>
            <li class="active">
                <strong>PRODUCTOS</strong>
            </li>
        </ol>
    </div>
</div>
<div id="productos">
   
    <div class="row">
        <div  class=""> {{-- col-lg-12 --}} 
               <div class="ibox-title">
                        <h5>PRODUCTOS</h5>
                    </div>

            <div class="ibox-content">
                <div class="row m-b-sm m-t-sm">

                    <div class="col-md-4">
                        <div class="input">
                            <input type="text" placeholder="Nombre" class="input-sm form-control">

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="input">
                            <input type="text" placeholder="Codigo" class="input-sm form-control">
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="input">
                            <input type="text" placeholder="Precio" class="input-sm form-control">
                        </div>
                    </div>

                </div>

                <div class="">
                    <div class="row m-b-sm m-t-sm col-md-offset-1">
                        <div class="col-md-3">
                            <a  class="btn btn-primary" v-on:click="">BUSCAR</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="btn btn-primary" v-on:click="">ELIMINAR</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('editar-crear-producto')  }}" class="btn btn-primary" >EDITAR</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('editar-crear-producto')  }}" class="btn btn-primary" v-on:click="editar();">NUEVO</a>
                        </div>
                    </div>
                </div>  
                <div class="">
                    <div class="row m-b-sm m-t-sm col-md-offset-1">
                        <div class="col-md-3">
                                    <button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="modal" data-target="#importarFromCSV" >IMPORTAR DE CSV
                                    </button>
                        </div>
                                                           
                        <div class="col-md-3">
                                    <button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="modal" data-target="#importarFromTXT" >IMPORTAR DE TXT
                                    </button>
                        </div>

                        <div class="col-md-2">
                                    {{-- <span class="btn btn-md btn-primary" @click.prevent="descargarExcel();">EXPORTAR A EXCEL</span> --}}
                                <a href="{{ route('downloadProductosCSV')  }}" class="btn btn-info" >Exportar a CSV</a>
                        </div>
                        <div class="col-md-2">
                                    {{-- <span class="btn btn-md btn-primary" @click.prevent="descargarExcel();">EXPORTAR A EXCEL</span> --}}
                                <a href="{{ route('downloadProductosTXT')  }}" class="btn btn-info" >Exportar a TXT</a>
                        </div>
                                                           
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="importarFromCSV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button @click.prevent="defaultData();" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    {{-- <span aria-hidden="true">&times;</span> --}}
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title" style="font-size:20pt"  id="myModalLabel">IMPORTAR DATOS</h4>
            </div>
            <div class="modal-body">
         
       
                <form class="form-horizontal" method="post" id="formularioCSV" enctype="multipart/form-data" >
                        <div class="form-group">
                            <label class="col-md-1 control-label">ARCHIVO CSV </label>
                            <div class="fileinput fileinput-new input-group col-md-3" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <input type="file" name="fileCSV" accept=".csv">
                                </span>
                            </div>
                        </div>
                </form>
            </div>
            {{-- </div> --}}
            <div class="modal-footer">
                <div class="outer_div">                                          
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="">    
                                <tr  class="warning">
                                    <th>Producto</th>
                                    <th>Precio</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dp in dataProductsFromFile">
                                    <td class="project-title">
                                        @{{dp.nombre}}
                                    </td>
                                    <td class="project-title">
                                        @{{dp.precio}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-9 col-md-3">
                        <span class="btn btn-md btn-primary" @click.prevent="saveLoadDataFromFile();">GUARDAR EN DB</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="importarFromTXT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button @click.prevent="defaultData();" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    {{-- <span aria-hidden="true">&times;</span> --}}
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title" style="font-size:20pt"  id="myModalLabel">IMPORTAR DATOS</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="formularioTXT" enctype="multipart/form-data" >
                        <div class="form-group">
                            <label class="col-md-1 control-label">ARCHIVO TXT </label>
                            <div class="fileinput fileinput-new input-group col-md-3" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <input type="file" name="fileTXT" accept=".txt">
                                </span>
                            </div>
                        </div>
                </form>
            </div>
            {{-- </div> --}}
            <div class="modal-footer">
                <div class="outer_div">                                          
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="">    
                                <tr  class="warning">
                                    <th>Producto</th>
                                    <th>Precio</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dp in dataProductsFromFile">
                                    <td class="project-title">
                                        @{{dp.nombre}}
                                    </td>
                                    <td class="project-title">
                                        @{{dp.precio}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-9 col-md-3">
                        <span class="btn btn-md btn-primary" @click.prevent="saveLoadDataFromFile();">GUARDAR EN DB</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>


    <div class="row">   

        {{-- Detalles --}}
        <div class="row">
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">

                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                            <th class="project-title">
                                                <strong>CODIGO</strong>
                                            </th>
                                            <th class="project-title">
                                                <strong>NOMBRE</strong>
                                            </th>
                                            <th class="project-title">
                                                <strong>PRECIO</strong>
                                            </th>
                                            <th class="project-title">
                                                <strong>SELECCIONAR</strong>
                                            </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="p in productos">
                                                <td class="project-title">
                                                    @{{p.id}} {{-- c.codigo --}}
                                                </td>
                                                <td class="project-title">
                                                    @{{p.descripcion}}
                                                    {{-- @{{p.codigo}} --}}
                                                </td>
                                                <td class="project-title">
                                                    @{{p.precio_base}}
                                                </td>
                                                <td class="project-title">
                                                    <span class="btn btn-danger btn-sm" title="Editar" @click.prevent="eliminarProducto(p.id)">
                                                        <i class="fa fa-trash"></i>
                                                    </span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                         <nav>
                                            <ul class="pagination">
                                                <li v-if="pagination.current_page > 1">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page - 1);">
                                                        <span>Atras</span>
                                                    </a>
                                                </li>
                                                <li v-for="page in pagesNumber" v-bind:class="[page == isActive ? 'active' : '']">
                                                    <a href="#" @click.prevent="changePage(page);">
                                                        @{{ page }}
                                                    </a>
                                                </li>
                                                <li v-if="pagination.current_page < pagination.last_page">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page + 1);">
                                                        <span>Siguiente</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

{{-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"> </script> --}}
<script src="{{ asset('js/app/sistema_producto.js') }}"></script>
