<!-- <!DOCTYPE HTML>
<html>
 <head> -->
@extends('layouts.app') @section('content')

 <form method="post" id="formulario" enctype="multipart/form-data">
    Subir imagen: <input type="file" name="fileCSV">
    <!-- <input type="submit" name=""> -->
 </form>
  <div id="respuesta"></div>

@endsection
{{-- <script src="{{ asset('js/app/sistema_usuario.js') }}"></script> --}}
  <script src="http://code.jquery.com/jquery-1.11.1.min.js"> </script>
  <script>
     $(function(){
        $("input[name='fileCSV']").on("change", function(){
            var formData = new FormData($("#formulario")[0]);
            var ruta = "/importar-productos-csv";
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    $("#respuesta").html(datos);
                }
            });
        });
     });
    </script>