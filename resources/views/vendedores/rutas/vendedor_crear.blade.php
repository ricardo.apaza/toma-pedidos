@extends('layouts.app') 
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>VENDEDORES</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>VENDEDORES</a>
            </li>
            <li class="active">
                <strong>CREAR VENDEDOR</strong>
            </li>
        </ol>
    </div>
</div>
<div id="NuevoVendedor">
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
            </div>
        </div>
    </div>

    <div>
        <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">
                                        <div class="row m-b-sm m-t-sm">
                                            <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-1 control-label">Codigo</label>
                                                    <div class="col-md-3">
                                                        <input readonly type="text" class="form-control" placeholder="VEND001" v-model="" />
                                                    </div>

                                                    <label class="col-md-1 control-label">Apellido</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" placeholder="Mendoza" v-model="vendedor.apellido"/>
                                                    </div>

                                                    <label class="col-md-1 control-label">Nombre</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" placeholder="Juan" v-model="vendedor.nombre"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                  
                                                    <label class="col-md-1 control-label">Zona</label>
                                                    <div class="col-md-3">
                                                        <select class="form-control" v-model="vendedor.id_zona">
                                                            <option v-for="z in zonas" value="@{{ z.id }}">@{{ z.codigo }}</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <label class="col-md-1 control-label">Usuario</label>
                                                    <div class="col-md-3">
                                                        <select class="form-control" v-model="vendedor.id_usuario">
                                                            <option v-for="u in usuarios" value="@{{ u.id }}">@{{ u.name }} @{{ u.apellido}}</option>
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                              
                                                <div class="form-group">
                                                    <div class="col-md-offset-9 col-md-3">
                                                        <span class="btn btn-md btn-primary" v-on:click="guardarVendedor()">Guardar</span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
<script src="{{ asset('js/app/sistema_vendedor_crear.js') }}"></script>

