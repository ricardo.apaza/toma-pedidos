@extends('layouts.app') 
@section('content')
    {{-- <input type="submit" name="asd"> --}}
    <div id="rutas">
         <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="col-lg-10">
                            <div class="col-lg-1">
                                <a href="/rutas" class="btn btn-info"><i class="fa fa-arrow-left"></i></a>
                            </div>
                            <div class="col-lg-11">
                                <bold class="lead">
                                    <label class=""> RUTA:
                                    </label>
                                </bold> 
                                <spam class="lead"> @{{ruta[0].descripcion}}0001</spam>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <span  @click.prevent="editarDatos();" class="btn btn-default"><i class="fa fa-pencil"></i></span>
                        </div>
                        <div class="col-lg-1">
                            <span @click.prevent="eliminarruta(ruta[0].id);" class="btn btn-danger"> <i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
        <div class="">
            @foreach ($ruta as $r)
                <select hidden v-model="id_ruta">
                  <option selected>{{$r->id}}</option>
                </select>

            @endforeach
        </div>

          <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">

                                 <div class="ibox-content">
                                    <div class="project-list">
                                        <div class="row m-b-sm m-t-sm">
                                            <form class="form-horizontal">
                                                <div class=""  v-for="r in ruta">
                                                    <div class="form-group">
                                                        <label class="col-md-1 control-label">Codigo</label>
                                                        <div class="col-md-3">
                                                            <input readonly type="text" class="form-control" v-model="vendedor_editar.codigo" placeholder="RUTA001" value="@{{r.codigo}}" />
                                                        </div>

                                                        <label class="col-md-1 control-label">Cliente</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" v-if="editar" placeholder="Mendoza" v-model="ruta_editar.id_cliente" value="@{{r.id_cliente}}">
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.id_cliente}}</h1>
                                                        </div>

                                                        <label class="col-md-1 control-label">Vendedor</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" v-if="editar" placeholder="Juan" v-model="ruta_editar.id_vendedor" value="@{{r.id_vendedor}}"/>
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.id_vendedor}}</h1>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                      
                                                        <label class="col-md-1 control-label">Fecha Inicio</label>
                                                        <div class="col-md-3">
                                                          {{--   <select v-if="editar" class="form-control" v-model="ruta_editar.id_zona">
                                                                <option v-for="z in zonas" value="@{{ z.id }}">@{{ z.codigo }}</option>
                                                            </select> --}}
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.fecha_inicio}}</h1>
                                                        </div>
                                                        
                                                        <label class="col-md-1 control-label">Fecha fin</label>
                                                            <div class="col-md-3">

                                                             {{--    <select v-if="editar" class="form-control" v-model="ruta_editar.id_usuario">
                                                                    <option v-for="u in usuarios" value="@{{ u.id }}">@{{ u.name }}</option>
                                                                </select> --}}
                                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.fecha_fin}}</h1>
                                                            </div>

                                                        <label class="col-md-1 control-label">Dia semana<label>
                                                        <div class="col-md-3">

                                                           <!-- Default checked -->
                                                          {{--   <label class="bs-switch">
                                                              <input type="checkbox" checked>
                                                              <span class="slider round"></span>
                                                            </label> --}}
                                                            <input type="text"  v-if="editar"  type="text" name="" value="@{{r.dia_semana}}">
                                                            <h1 class="text-info" v-else type="text" name="">@{{r.dia_semana}}</h1>
                                                        </div>
                                                    
                                                    </div>
                                                  
                                                    <div class="form-group">
                                                        <div v-if="editar" class="col-md-offset-9">
                                                            <span class="btn btn-md btn-primary" v-on:click="guardar_ruta()">Guardar</span>
                                                            <span class="btn btn-md btn-danger" v-on:click="noEditarDatos()">Cancelar</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                              {{--   <div class="ibox-content">
                                    <div class="project-list">
                                        <div class=""  v-for="r in ruta">
                                            <div>
                                                <h1 class="text-primary" >Codigo</h1>
                                                <h1 class="text-info" readonly type="text">@{{r.id}}<h1>
                                             </div>
                                            <div>
                                                <h1 class="text-dark">Cliente</h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{r.id_cliente}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.id_cliente}}</h1>

                                             </div>
                                             <div>
                                                <h1 class="text-dark">Vendedor</h1>
                                                 <input class="text-info" v-if="editar"  type="text" name="" value="@{{r.id_vendedor}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.id_vendedor}}</h1>
                                             </div>
                                             <div>
                                                <h1 class="text-dark">Fecha Inicio</h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{r.fecha_inicio}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.fecha_inicio}}</h1>
                                             </div>
                                             <div>
                                                <h1 class="text-dark">Fecha Fin</h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{r.fecha_fin}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.fecha_fin}}</h1>
                                             </div>
                                             <div>
                                                <h1 class="text-dark">Dia Semana</h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{r.dia_semana}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{r.dia_semana}}</h1>
                                             </div>
                                        </div>

                                        <br>

                                        <div>
                                            <input class="btn btn-primary" type="button" name="" value="GUARDAR">
                                            <input class="btn btn-default" type="button" name="" value="CANCELAR">
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


       
    </div>

@endsection
<script src="{{ asset('js/app/sistema_ruta_datos.js') }}"></script>

