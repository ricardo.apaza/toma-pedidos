@extends('layouts.app') 
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>VENDEDORES</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>VENDEDORES</a>
            </li>
            <li class="active">
                <strong>CREAR GRUPO VENDEDOR</strong>
            </li>
        </ol>
    </div>
</div>
<div id="nuevo_grupo_vendedor">
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
            </div>
        </div>
    </div>

    <div>
        <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">
                                        <div class="row m-b-sm m-t-sm">
                                            <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-1 control-label">Codigo</label>
                                                    <div class="col-md-3">
                                                        <input readonly type="text" class="form-control" placeholder="GRUPOVENDEDOR001" v-model="" />
                                                    </div>

                                                    <label class="col-md-1 control-label">Nombre</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" placeholder="" v-model="grupo_vendedor.descripcion"/>
                                                    </div>

                                                    <label class="col-md-1 control-label">Supervisor</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" placeholder="" v-model="grupo_vendedor.id_supervisor"/>
                                                    </div>
                                                </div>
                                              
                                                <div class="form-group">
                                                    <div class="col-md-offset-9 col-md-3">
                                                        <span class="btn btn-md btn-primary" v-on:click="guardar_grupo_vendedor()">Guardar</span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
<script src="{{ asset('js/app/sistema_grupo_vendedor_crear.js') }}"></script>

