@extends('layouts.app') @section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>VENDEDORES</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">INICIO</a>
            </li>
            <li>
                <a>VENDEDORES</a>
            </li>
            <li class="active">
                <strong>GRUPOS VENDEDORES</strong>
            </li>
        </ol>
    </div>
</div>
<div id="grupo_vendedor">
    {{-- Listado --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="col-lg-12">
                    <div class="row m-b-sm m-t-sm">
                        <a href="{{ url('grupo-vendedor/create')}}" class="btn btn-primary">
                            NUEVO GRUPO VENDEDOR
                        </a>
                    </div>
                </div>
               {{--  <div class="ibox">
                    <div class="ibox-title">
                                    <span>Zonas que cumplan 
                                        <input type="button" name="btnAlgunas" value="Algunas">
                                        <input type="button" name="btlTodas" value="todas">
                                        de las siguientes:
                                    </span>
                                    <button class="btn btn-danger">
                                        Nueva Zona
                                    </button>
                    </div>  

                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <select>
                                          <option value="1">Nombre</option>
                                          <option value="2">...</option>
                                          <option value="3">...</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group">
                                    <select>
                                          <option value="1">Contiene</option>
                                          <option value="2">Direferente a </option>
                                          <option value="3">Igual a </option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="input-group">
                                    <span class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="btn btn-default">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                    <span class="btn btn-default">
                                        <i class="fa fa-retweet"></i>
                                    </span>
                                    <span class="btn btn-default">
                                        <i class="fa fa-filter"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

        {{-- Detalles --}}
<!-- Modal Busca Producto-->
    <div>
        <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">
                                       {{--  <div class="row m-b-sm m-t-sm">
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="btn btn-default">
                                                      <input type="checkbox" name="" value="">Seleccionar todo
                                                    </div>                                                        
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                 <div class="btn btn-default">
                                                    <i class="fa fa-file"> Importar</i>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                
                                                 <div class="btn btn-default">
                                                    <i class="fa fa-file"> Exportar</i>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                    Ordenado por:
                                                    <select>
                                                          <option value="1">Nombre</option>
                                                          <option value="2">ID</option>
                                                          <option value="3">...</option>
                                                    </select>
                                            </div>
                                        </div> --}}

                                        <table class="table table-hover">
                                            <thead>
                                               {{--  <td class="project-title">
                                                    <strong></strong>
                                                </td> --}}
                                                <td class="project-title">
                                                    <strong>CODIGO</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>NOMBRE</strong>
                                                </td>
                                                <td class="project-title">
                                                    <strong>SUPERVISOR</strong>
                                                </td>
                                            </thead>
                                            <tbody>
                                                <tr v-for="gv in grupos_vendedores">
                                                    {{-- <td class="project-title" >
                                                        <input type="checkbox" name="">
                                                    </td> --}}
                                                    <td class="project-title" >
                                                        <a href="{{ url('grupo-vendedor/grupo-vendedor-datos/')}}/@{{gv.id}}">
                                                            @{{gv.codigo_grupo_vendedor}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                        <a href="{{ url('grupo-vendedor/grupo-vendedor-datos/')}}/@{{gv.id}}">
                                                            @{{gv.nombre_grupo_vendedor}}
                                                        </a>
                                                    </td>
                                                    <td class="project-title">
                                                        <a href="{{ url('grupo-vendedor/grupo-vendedor-datos/')}}/@{{gv.id}}">
                                                            @{{gv.nombre_supervisor}}
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                          <nav>
                                            <ul class="pagination">
                                                <li v-if="pagination.current_page > 1">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page - 1);">
                                                        <span>Atras</span>
                                                    </a>
                                                </li>
                                                <li v-for="page in pagesNumber" v-bind:class="[page == isActive ? 'active' : '']">
                                                    <a href="#" @click.prevent="changePage(page);">
                                                        @{{ page }}
                                                    </a>
                                                </li>
                                                <li v-if="pagination.current_page < pagination.last_page">
                                                    <a href="#" @click.prevent="changePage(pagination.current_page + 1);">
                                                        <span>Siguiente</span>
                                                    </a>
                                                </li>
                                                <li class="list-group">
                                                <div class="col-md-6">

                                                        <select>
                                                          <option value="5">5</option>
                                                          <option value="10">10</option>
                                                          <option value="25">25</option>
                                                          <option value="50">50</option>
                                                        </select>
                                                </div>
                                                </li>             


                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
<script src="{{ asset('js/app/sistema_grupo_vendedor.js') }}"></script>

{{-- <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script> --}}



{{-- <script >
    $(document).ready(function(){
        //alert("dd");
        $("#btnAbrirModal").hide();
    });

</script> --}}