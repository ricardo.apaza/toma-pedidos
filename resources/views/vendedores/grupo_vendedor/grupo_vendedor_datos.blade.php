@extends('layouts.app') 
@section('content')
    {{-- <input type="submit" name="asd"> --}}
    <div id="grupo_vendedor">
         <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="col-lg-10">
                            <div class="col-lg-1">
                                <a href="/grupos-vendedores" class="btn btn-info"><i class="fa fa-arrow-left"></i></a>
                            </div>
                            <div class="col-lg-11">
                                <bold class="lead">
                                    <label class=""> GRUPO:
                                    </label>
                                </bold> 
                                <spam class="lead"> @{{grupo_vendedor[0].descripcion}}</spam>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <span  @click.prevent="editarDatos();" class="btn btn-default"><i class="fa fa-pencil"></i></span>
                        </div>
                        <div class="col-lg-1">
                            <span @click.prevent="eliminar_grupo_vendedor(grupo_vendedor[0].id);" class="btn btn-danger"> <i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
        <div class="">
            @foreach ($grupo_vendedor as $gv)
                <select hidden v-model="id_grupo_vendedor">
                  <option selected>{{$gv->id}}</option>
                </select>

            @endforeach
        </div>
                              {{-- nueva direccion --}}
                                      <div class="modal fade" id="modal_supervisor_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">CREAR GRUPO</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive">
                                                                    <div>
                                                                       <h1>CODIGO</h1> 
                                                                        <input  readonly placeholder="VENDEDOR001" type="text" class="form-control"  v-model="vendedor_nuevo.codigo">
                                                                    </div>
                                                                
                                                                    {{--  <div>
                                                                        <h1>TIPO:</h1> 
                                                                        <select class="form-control" v-model="cliente_email_nuevo.tipo_email">
                                                                          <option value="Negocio">Negocio</option>
                                                                          <option value="Entrega">Entrega</option>
                                                                          <option value="Pago">Pago</option>
                                                                          <option value="Domicilio" selected>Domicilio</option>
                                                                        </select>
                                                                    </div> --}}
                                                                    <div>
                                                                        <h1>NOMBRE</h1> 
                                                                        <input  type="text" class="form-control"  v-model="vendedor_nuevo.nombre">
                                                                    </div>
                                                                      <div>
                                                                        <h1>APELLIDO</h1> 
                                                                        <input  type="text" class="form-control"  v-model="vendedor_nuevo.apellido">
                                                                    </div>
                                                                        <div>
                                                                       <h1>PEFIL</h1> 
                                                                        <input  readonly placeholder="SUPERVISOR" type="text" class="form-control"  v-model="vendedor_nuevo.codigo">
                                                                    </div>
                                                                    
                                                            </div>                                          
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                         <button type="button" data-dismiss="modal" class="btn btn-primary" v-on:click="guardarGrupoVendedor();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


          <div class="row" > {{-- Esto hace ver con un dato v-show="nuevo" --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="project-list">
                                        <div class="row m-b-sm m-t-sm">
                                            <form class="form-horizontal">
                                                <div class=""  v-for="gv in grupo_vendedor">
                                                    <div class="form-group">
                                                        <label class="col-md-1 control-label">Codigo</label>
                                                        <div class="col-md-3">
                                                            <input readonly type="text" class="form-control" v-model="grupo_vendedor_editar.codigo" placeholder="GRUPOVENDEDOR001" value="@{{gv.codigo}}" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                           <label class="col-md-1 control-label">Nombre</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" v-if="editar" placeholder="Mendoza" v-model="grupo_vendedor_editar.descripcion" value="@{{gv.descripcion}}">
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{gv.descripcion}}</h1>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1 control-label">Supervisor</label>
                                                        <span v-show="editar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modal_supervisor_crear">
                                                                <i class="text-success"  aria-="true"> + NUEVO</i>
                                                            </span>
                                                        {{-- <div class="col-md-3"> --}}
                                                             <div class="col-md-3">
                                                            <select v-if="editar" class="form-control" v-model="vendedor_editar.id_supervisor">
                                                                <option v-for="s in supervisores" value="@{{ s.id }}">@{{ s.nombre }}, @{{ s.apellido }}</option>
                                                            </select>
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{gv.id_supervisor}}</h1>
                                                        </div>

                                                            {{-- <input type="text" class="form-control" v-if="editar" placeholder="" v-model="grupo_vendedor_editar.id_supervisor" value="@{{gv.id_supervisor}}"/> --}}
                                                        {{-- </div> --}}
                                                    </div>
                                                    <div class="form-group">
                                                        <div v-if="editar" class="col-md-offset-9">
                                                            <span class="btn btn-md btn-primary" v-on:click="guardar_grupo_vendedor()">Guardar</span>
                                                            <span class="btn btn-md btn-danger" v-on:click="noEditarDatos()">Cancelar</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<script src="{{ asset('js/app/sistema_grupo_vendedor_datos.js') }}"></script>

