@extends('layouts.app') 
@section('content')
    <div id="vendedores">
         <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="col-lg-12">
                    <div class="col-lg-10">
                        <a id="btnIrViewVendedores" href="/vendedores" class="btn btn-info"><i class="fa fa-arrow-left"></i></a>
                    </div>
                    <div class="col-lg-1">
                        <span  @click.prevent="editarDatos();" class="btn btn-default"><i class="fa fa-pencil"></i></span>
                    </div>
                    <div class="col-lg-1">
                        <span @click.prevent="eliminarvendedor(vendedor[0].id);" class="btn btn-danger"> <i class="fa fa-trash"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
        <div class="">
            @foreach ($vendedor as $v)
                <select hidden v-model="id_vend">
                  <option selected>{{$v->id}}</option>
                </select>

            @endforeach
        </div>

                                    {{-- nueva direccion --}}
                                      <div class="modal fade" id="modal_grupo_vendedor_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        {{-- <span aria-hidden="true">&times;</span> --}}
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </button>
                                                    <h4 class="modal-title" style="font-size:20pt" id="myModalLabel">CREAR GRUPO</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <div class="form-group">   
                                                            <div class="outer_div">                                          
                                                                <div class="table-responsive">
                                                                    <div>
                                                                       <h1>CODIGO</h1> 
                                                                        <input  readonly placeholder="GRUPOVENDEDOR001" type="text" class="form-control"  v-model="grupo_vendedor_nuevo.codigo">
                                                                    </div>
                                                                    {{--  <div>
                                                                        <h1>TIPO:</h1> 
                                                                        <select class="form-control" v-model="cliente_email_nuevo.tipo_email">
                                                                          <option value="Negocio">Negocio</option>
                                                                          <option value="Entrega">Entrega</option>
                                                                          <option value="Pago">Pago</option>
                                                                          <option value="Domicilio" selected>Domicilio</option>
                                                                        </select>
                                                                    </div> --}}
                                                                    <div>
                                                                        <h1>NOMBRE</h1> 
                                                                        <input  type="text" class="form-control"  v-model="grupo_vendedor_nuevo.descripcion">
                                                                    </div>
                                                                    
                                                            </div>                                          
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                         <button type="button" data-dismiss="modal" class="btn btn-primary" v-on:click="guardarGrupoVendedor();" >GUARDAR</button>
                                                         <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


          <div class="row" > 
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                 <div class="ibox-content">
                                    <div class="project-list">
                                        <div class="row m-b-sm m-t-sm">
                                            <form class="form-horizontal">
                                                <div class=""  v-for="v in vendedor">
                                                    <div class="form-group">
                                                        <label class="col-md-1 control-label">Codigo</label>
                                                        <div class="col-md-3">
                                                            <input readonly type="text" class="form-control" v-model="vendedor_editar.codigo" placeholder="VEND001" value="@{{v.codigo}}" />
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                         
                                                        <label class="col-md-1 control-label">Apellido</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" v-if="editar" placeholder="Mendoza" v-model="vendedor_editar.apellido" value="@{{v.apellido}}">
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.apellido}}</h1>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        
                                                        <label class="col-md-1 control-label">Nombre</label>
                                                        <div class="col-md-3">
                                                             {{-- <input class="text-info" v-if="editar"  type="text" name="" value="@{{z.codigo}}"> --}}
                                                            {{-- <h1 class="text-info" v-else="editar"  type="text" name="">@{{z.codigo}}</h1> --}}
                                                            <input type="text" class="form-control" v-if="editar" placeholder="Juan" v-model="vendedor_editar.nombre" value="@{{v.nombre}}"/>
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.nombre}}</h1>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1 control-label">PEFIL</label>
                                                            <div class="col-md-3">

                                                                <select v-if="editar" class="form-control" v-model="vendedor_editar.id_usuario">
                                                                    <option v-for="u in usuarios" value="@{{ u.id }}">@{{ u.name }}</option>
                                                                </select>
                                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.id_usuario}}</h1>
                                                            </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <label class="col-md-1 control-label">Grupo</label>
                                                        <span v-show="editar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modal_grupo_vendedor_crear">
                                                            <i class="text-success"  aria-="true"> + NUEVO</i>
                                                       </span>
                                                        <div class="col-md-3">
                                                            <select v-if="editar" class="form-control" v-model="vendedor_editar.id_zona">
                                                                <option v-for="gv in grupos_vendedores" value="@{{ gv.id }}">@{{ gv.codigo }}</option>
                                                            </select>
                                                            <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.id_grupo_vendedor}}</h1>
                                                        </div>
                                                    </div>
                                                  
                                                    <div class="form-group">
                                                        <div v-if="editar" class="col-md-offset-9">
                                                            <span class="btn btn-md btn-primary" v-on:click="guardarVendedor()">Guardar</span>
                                                            <span class="btn btn-md btn-danger" v-on:click="noEditarDatos()">Cancelar</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                          {{--       <div class="ibox-content">
                                    <div class="project-list">
                                        <div class=""  v-for="v in vendedor">
                                            <div>
                                                <h1 class="text-primary" >Codigo</h1>
                                               <h1 class="text-info" v-else readonly type="text">@{{v.id}}<h1>
                                             </div>
                                            <div>
                                                <h1 class="text-dark">Nombre</h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{v.nombre}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.nombre}}</h1>

                                             </div>
                                             <div>
                                                <h1 class="text-dark">Apellido</h1>
                                                 <input class="text-info" v-if="editar"  type="text" name="" value="@{{v.apellido}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.apellido}}</h1>
                                             </div>
                                             <div>
                                                <h1 class="text-dark">Perfil</h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{v.id_usuario}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.id_usuario}}</h1>
                                             </div>
                                             <div>
                                                <h1 class="text-dark">Zona  <button class="btn btn-info text-dark">+Nueva</button></h1>
                                                <input class="text-info" v-if="editar"  type="text" name="" value="@{{v.id_zona}}">
                                                <h1 class="text-info" v-else="editar"  type="text" name="">@{{v.id_zona}}</h1>
                                             </div>
                                        </div>

                                        <br>

                                        <div>
                                            <input class="btn btn-primary" type="button" name="" value="GUARDAR">
                                            <input class="btn btn-default" type="button" name="" value="CANCELAR">
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


       
    </div>

@endsection
<script src="{{ asset('js/app/sistema_vendedor_2.js') }}"></script>

