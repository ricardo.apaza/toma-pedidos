let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

// mix.scripts([
// 	'public/js/app/sistema_cliente.js',
// 	'public/js/app/sistema_components.js',
// 	'public/js/app/sistema_no_pedido.js',
// 	'public/js/app/sistema_pedido.js',
// 	'public/js/app/sistema_producto.js',
// 	'public/js/app/sistema_ruta.js',
// 	'public/js/app/sistema_ruta_datos.js',
// 	'public/js/app/sistema_usuario.js',
// ],'public/js/app.js');

// mix.combine([
//     'public/css/bootstrap.min.css', 
//     'public/css/style.css',
//     'public/css/animate.css'
// ], 'public/css/app.css');
// mix.combine([
//     'public/js/jquery-2.1.1.js',
//     'public/js/bootstrap.min.js',
//     'public/js/plugins/metisMenu/jquery.metisMenu.js',
//     'public/js/plugins/slimscroll/jquery.slimscroll.min.js',    
//     'public/js/plugins/pace/pace.min.js'
// ], 'public/js/app.js');