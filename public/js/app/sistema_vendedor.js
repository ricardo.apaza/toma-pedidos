/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#vendedores",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        vendedores:{},
        vendedor:{},
        users:[],
        nuevo:0,
        pagination:{  
            'total' :0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0,
          },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
      };
    },
    computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
    },
    methods: {
      load: function() {
      
        this.obtenervendedores();
      },
      nuevavendedor:function(){
        this.vendedor={};
        this.nuevo=1;
      },
    
      obtenervendedores: function(page) {
        params = { page: page};
        this.$http
            .post(root + "/obtener-vendedores", params)
            .then(function(_response) {
              this.vendedores = _response.data.data.vendedor.data;
              this.pagination = _response.data.data.pagination;
              // this.users = _response.data.data.users;
              // this.vendedores = _re
            });
      },

      // test: function(){
      //   alert("test");
      // },

      // detallesVendedor: function(vendedor){
      //   alert("GG");
      //   this.$http.get(root + '/vendedor/vendedor-datos/'+ vendedor.id);
      //   this.$route.router.go('/vendedor/vendedor-datos/'+ vendedor.id);
      // },

      obtenervendedor: function(_perfil, _evt) {
        this.vendedor = _perfil;
        this.nuevo=1;
        console.log(this.perfil);
        _evt.preventDefault();
      },
      guardarvendedor: function(_evt) {
        this.$http
            .post(root + "/guardar-vendedor", this.vendedor)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){

                this.nuevo=0;
                this.vendedor={};
                this.obtenervendedores();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
        _evt.preventDefault();
      },
        guardarVendedor: function(_evt) {
        this.$http
            .post(root + "/vendedor/guardar-vendedor", this.vendedor)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                this.vendedor={};
                this.selectData();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
        _evt.preventDefault();
      },
      
      eliminarvendedor: function(_id,_evt) {
        params = { id_vendedor: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/eliminar-vendedor", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.obtenervendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },
      changePage:function(page){
        this.pagination.current_page = page;
        this.obtenervendedores(page);
      },
      



    }

  });

});

