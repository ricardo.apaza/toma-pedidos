/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#grupo_vendedor",
    ready: function() {
      this.load();

    },
    data: function() {
      return {
        grupos_vendedores:{},
        grupo_vendedor:{},
        users:[],
        nuevo:0,
        pagination:{  
            'total' :0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0,
          },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
      };
    },
    computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
    },
    methods: {
      load: function() {
        this.obtener_grupos_vendedores();
      },
      nuevo_grupo_vendedor:function(){
        this.grupo_vendedor={};
        this.nuevo=1;
      },
    
      obtener_grupos_vendedores: function(page) {
        params = { page: page};
        this.$http
            .post(root + "/grupo-vendedor/obtener-grupos-vendedores", params)
            .then(function(_response) {
              this.grupos_vendedores = _response.data.data.grupos_vendedores.data;
              this.pagination = _response.data.data.pagination;
            });
      },
      obtener_grupo_vendedor: function(_perfil, _evt) {
        this.grupo_vendedor = _perfil;
        this.nuevo = 1;
      },
      guardar_grupo_vendedor: function(_evt) {
        this.$http
            .post(root + "/grupo-vendedor/guardar-grupo-vendedor", this.grupo_vendedor)
            .then(function(_response) {              
            if(_response.data.status.code=='PROCESS_COMPLETE'){

                this.nuevo=0;
                this.grupo_vendedor={};
                this.obtener_grupos_vendedores();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
      },
      eliminar_grupo_vendedor: function(_id,_evt) {
        params = { id_grupo_vendedor: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/grupo-vendedor/eliminar-grupo-vendedor", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.obtener_grupos_vendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },

       agregar_grupo_vendedor: function(_id) {
        params ={};
        swal({
          title: "Agregar Grupo Vendedor",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si",
          closeOnConfirm: false
        }, function () {
                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }
          swal.close();
        });

      },
      changePage:function(page){
        this.pagination.current_page = page;
        this.obtener_grupos_vendedores(page);
      },
    }

  });

});

