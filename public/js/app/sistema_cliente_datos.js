/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#vendedores",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        id_cliente:"",
        cliente_editar:{id_cliente:0},
        cliente:{},
        editar:0,
        cliente_direcciones:{},
        cliente_telefonos:{},
        cliente_emails:{},


        cliente_direccion_nuevo:{},
        cliente_direccion:{},
        cliente_direccion_editar:{},


        cliente_email_nuevo:{},
        cliente_email:{},
        cliente_email_editar:{},

        cliente_telefono_nuevo:{},
        cliente_telefono:{},
        cliente_telefono_editar:{},


        
        // zonas:{},
        // usuarios:{},
      };
    },
  
    methods: {
      load: function() {
        this.obtenerCliente(this.id_cliente);
        this.obtenerDatosCliente(this.id_cliente);
        // this.selectData();   
      },
      editarDatos: function(){
        // alert("asds");
        this.editar = 1;
        // alert(this.editar);
      },
      noEditarDatos: function(){
        // alert("asds");
        this.editar = 0;
        // alert(this.editar);
      },
      // selectData: function(){
      // this.$http.get(root + "/cliente/select-data").then(
      //       function(_response) {
      //         this.cliente_direcciones = _response.data.data.cliente_direcciones;
      //         this.cliente_telefonos = _response.data.data.cliente_telefonos;
      //         this.cliente_emails = _response.data.data.cliente_emails;
      //       },
      //       function(response) {
      //         console.log(response);
      //       }
      //   );
      // },
      obtenerCliente: function(id){
        params = { id_cliente: id};
        this.$http
            .post(root + "/cliente/obtener-cliente", params)
            .then(function(_response) {
              this.cliente = _response.data.data.cliente;
              // this.pagination = _response.data.data.pagination;
              // this.users = _response.data.data.users;
              // this.vendedores = _re
            });
      },
      obtenerDatosCliente: function(id){
        params = { id_cliente: id};
        this.$http
            .post(root + "/cliente/obtener-datos-adicionales-cliente", params)
            .then(function(_response) {
              console.log(_response);
              this.cliente_direcciones = _response.data.data.cliente_direcciones;
              this.cliente_telefonos = _response.data.data.cliente_telefonos;
              this.cliente_emails = _response.data.data.cliente_emails;
            });
      },
      guardarCliente: function(_evt) {
        var formData = new FormData();
        this.vendedor_editar.id_vendedor = this.vendedor[0].id;
        for ( var key in this.vendedor_editar ) {
          formData.append(key, this.vendedor_editar[key]);
        }
        this.$http
            .post(root + "/cliente/guardar-cliente", formData)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                this.selectData();
                this.obtenervendedor(this.id_vend);
                this.noEditarDatos();
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
        _evt.preventDefault();
      },

      eliminarCliente: function(_id) {
        params = { id_cliente: _id};
        swal({
          title: "Confirma eliminación",
          text: "¿Esta seguro que desea eliminar el clientes cliente " + this.cliente[0].codigo + "?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/cliente/eliminar-cliente", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.retrocederViewVendedores();
                  app.obtenervendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });
      },

      eliminarClienteDireccion: function(_id) {
        params = { id_direccion: _id};
        swal({
          title: "Confirma eliminación",
          text: "¿Esta seguro que desea eliminar la direccion " + _id + " ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/cliente-direccion/eliminar-cliente-direccion", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  this.obtenerDatosCliente(this.id_cliente);
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });
      },

       obtenerClienteDireccion: function(_id){
        params = { id_direccion: _id};
        this.$http
            .post(root + "/cliente-direccion/obtener-direccion", params)
            .then(function(_response) {
              this.cliente_direccion = _response.data.data.cliente_direccion;
            });
      },

      guardarClienteDireccion: function(_evt) {
        var formData = new FormData();
        this.cliente_direccion_nuevo.id_cliente = this.id_cliente;
        for ( var key in this.cliente_direccion_nuevo ) {
          formData.append(key, this.cliente_direccion_nuevo[key]);
        }
        this.$http
            .post(root + "/cliente-direccion/guardar-cliente-direccion", formData)
            .then(function(_response) {              
              // console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                  this.obtenerDatosCliente(this.id_cliente);
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
      },

      editarClienteDireccion: function(_evt) {
        var formData = new FormData();
        this.cliente_direccion_editar.id_cliente = this.id_cliente;
        this.cliente_direccion_editar.id_direccion = this.cliente_direccion[0].id;

        for ( var key in this.cliente_direccion_editar ) {
          formData.append(key, this.cliente_direccion_editar[key]);
        }
        this.$http
            .post(root + "/cliente-direccion/guardar-cliente-direccion", formData)
            .then(function(_response) {              
              // console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                  this.obtenerDatosCliente(this.id_cliente);
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
      },


      //telefono
      eliminarClienteTelefono: function(_id) {
        params = { id_telefono: _id};
        swal({
          title: "Confirma eliminación",
          text: "¿Esta seguro que desea eliminar el telefono " + _id + " ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/cliente-telefono/eliminar-cliente-telefono", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  this.obtenerDatosCliente(this.id_cliente);
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });
      },

       obtenerClienteTelefono: function(_id){
        params = { id_telefono: _id};
        this.$http
            .post(root + "/cliente-telefono/obtener-telefono", params)
            .then(function(_response) {
              this.cliente_telefono = _response.data.data.cliente_telefono;
            });
      },

      guardarClienteTelefono: function(_evt) {
        var formData = new FormData();
        this.cliente_telefono_nuevo.id_cliente = this.id_cliente;
        for ( var key in this.cliente_telefono_nuevo ) {
          formData.append(key, this.cliente_telefono_nuevo[key]);
        }
        this.$http
            .post(root + "/cliente-telefono/guardar-cliente-telefono", formData)
            .then(function(_response) {              
              // console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                  this.obtenerDatosCliente(this.id_cliente);
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
      },

      editarClienteTelefono: function(_evt) {
        var formData = new FormData();
        this.cliente_telefono_editar.id_cliente = this.id_cliente;
        this.cliente_telefono_editar.id_telefono = this.cliente_telefono[0].id;

        for ( var key in this.cliente_telefono_editar ) {
          formData.append(key, this.cliente_telefono_editar[key]);
        }
        this.$http
            .post(root + "/cliente-telefono/guardar-cliente-telefono", formData)
            .then(function(_response) {              
              // console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                  this.obtenerDatosCliente(this.id_cliente);
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
      },



      //email
      eliminarClienteEmail: function(_id) {
        params = { id_email: _id};
        swal({
          title: "Confirma eliminación",
          text: "¿Esta seguro que desea eliminar el email " + _id + " ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/cliente-email/eliminar-cliente-email", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  this.obtenerDatosCliente(this.id_cliente);
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });
      },

       obtenerClienteEmail: function(_id){
        params = { id_email: _id};
        this.$http
            .post(root + "/cliente-email/obtener-email", params)
            .then(function(_response) {
              this.cliente_email = _response.data.data.cliente_email;
            });
      },

      guardarClienteEmail: function(_evt) {
        var formData = new FormData();
        this.cliente_email_nuevo.id_cliente = this.id_cliente;
        for ( var key in this.cliente_email_nuevo ) {
          formData.append(key, this.cliente_email_nuevo[key]);
        }
        this.$http
            .post(root + "/cliente-email/guardar-cliente-email", formData)
            .then(function(_response) {              
              // console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                  this.obtenerDatosCliente(this.id_cliente);
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
      },

      editarClienteEmail: function(_evt) {
        var formData = new FormData();
        this.cliente_email_editar.id_cliente = this.id_cliente;
        this.cliente_email_editar.id_email = this.cliente_email[0].id;

        for ( var key in this.cliente_email_editar ) {
          formData.append(key, this.cliente_email_editar[key]);
        }
        this.$http
            .post(root + "/cliente-email/guardar-cliente-email", formData)
            .then(function(_response) {              
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                this.obtenerDatosCliente(this.id_cliente);
                toastr.success("Registro Correcto", "ACTUALIZAR");
          }else{
            toastr.error("Registro Incorrecto", "ACTUALIZAR");
          }
        });     
      },











     }

  });

});

