/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#vendedores",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        id_pedido:"",
        id_cliente: 1,
        pedido_editar:{id_cliente:0},
        pedido:{},
        cliente_direccion_principal:{},
        detalles:{},
        cliente:{},
        editar:0,
      };
    },
  
    methods: {
      load: function() {
        this.obtenerPedidoDetalles(this.id_pedido);
        this.obtenerDatosAdicionales(this.id_cliente);
      },
      editarDatos: function(){
        this.editar = 1;
      },
      noEditarDatos: function(){
        this.editar = 0;
      },

      obtenerPedidoDetalles: function(_id,_evt){
        params = {id_pedido:_id};
        // console.log("asdasda");

        this.$http
            .post(root + "/pedidos/obtener-pedido-detalles", params)
            .then(function(_response) {
                this.pedido = _response.data.data.pedido;
                this.detalles = _response.data.data.detalles;

            });
        // console.log(this.pedido[0].id_cliente);
        // console.log("asdasd");

      },
      obtenerDatosAdicionales: function(_id,_evt){


        console.log(_id);
        console.log("asdasdsa");

        params = {id_cliente: _id};
        this.$http
            .post(root + "/pedidos/obtener-datos-adicionales", params)
            .then(function(_response) {
                this.cliente_direccion_principal = _response.data.data.direccion_principal;
            });
      },

     }
  });
});

