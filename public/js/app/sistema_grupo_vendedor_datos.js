/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#grupo_vendedor",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        id_grupo_vendedor:"",
        grupo_vendedor:{},
        grupo_vendedor_editar:{},
        editar:0,
      };
    },
  
    methods: {
      load: function() {
        this.obtener_grupo_vendedor(this.id_grupo_vendedor);
      },
      editarDatos: function(){
        this.editar = 1;
      },
       noEditarDatos: function(){
        this.editar = 0;
      },
      obtener_grupo_vendedor: function(id){
        params = { id_grupo_vendedor: id};
        this.$http
            .post(root + "/grupo-vendedor/obtener-grupo-vendedor", params)
            .then(function(_response) {
              this.grupo_vendedor = _response.data.data.grupo_vendedor;
            });
      },
      guardar_grupo_vendedor: function(_evt) {
        // alert("ss");
            // alert(this.zona_editar);
        var formData = new FormData();
        this.grupo_vendedor_editar.id_grupo_vendedor = this.grupo_vendedor[0].id;

        for ( var key in this.grupo_vendedor_editar ) {
          formData.append(key, this.grupo_vendedor_editar[key]);
        }

        this.$http
            .post(root + "/grupo-vendedor/guardar-grupo-vendedor", formData)
            .then(function(_response) {              
              // console.log(_response);
            if(_response.data.status.code=='PROCESS_COMPLETE'){
                    // this.vendedor={};
                    // this.selectData();
                    this.obtener_grupo_vendedor(this.id_grupo_vendedor);
                    this.noEditarDatos();
                    toastr.success("Registro Correcto", "ACTUALIZAR");
                  }else{
                    toastr.error("Registro Incorrecto", "ACTUALIZAR");
                  }
                });
            // _evt.preventDefault();
          },

      eliminar_grupo_vendedor_2: function(_id) {
        params = { id_grupo_vendedor: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/grupo-vendedor/eliminar-grupo-vendedor", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  // 
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },
      eliminar_grupo_vendedor: function(_id) {
        params = { id_grupo_vendedor: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        },
         function (){
          // alert("registro eliminado");
          app.$http
              .post(root + "/grupo-vendedor/eliminar-grupo-vendedor", params)
              .then(function(_response) {


                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  window.location.replace('/grupos-vendedores');
                  // 
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });

          swal.close();
        });

      },

      agregar_grupo_vendedor: function(_id) {
        // params = { id_ruta: _id};
        params ={};
        swal({
          title: "Agregar Grupo Vendedor",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si",
          closeOnConfirm: false
        }, function () {
                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }
          swal.close();
        });

      },

      // retrocederViewVendedores: function(){
      //   // alert("aa");
      //   // $('#btnIrViewVendedores').trigger('click');
      //   $("#btnIrViewVendedores").click();
      // },


     }

  });

});

