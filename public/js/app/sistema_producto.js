/* Configuración */

window.addEventListener('load', function () {
// import VueResource from "../Vue/vue-resource.min.js";
// Vue.use(VueResource);
 

  // Vue.http.options.root = "/public";

  var app = new Vue({
    el: "#productos",
    ready: function() {
      this.load();
    },
    data: function() {
      return {

        src:{},
        productos: {},//checked:false
        producto:{},
        nuevo:0,
           pagination:{  
            'total' :0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0,
          },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
        dataProductsFromFile:{},
        // files:{},
      };
    },
    methods: {
 
      load: function() {
        this.obtenerTodosProductos();
    
      },

     editar: function(){
        this.nuevo=1;
     },
     defaultData:function(){
        this.dataProductsFromFile = {};
     },
     guardarProducto: function(_evt) {

        var formData = new FormData();
        for ( var key in this.producto ) {
          formData.append(key, this.producto[key]);
        }
        this.$http
            .post(root + "/guardar-producto", formData)
            .then(function(_response) {
              // this.obtenerProductos();
              this.producto={};
              this.nuevo=0;
              console.log(_response);
              if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro guardado", "Guardar");
                  // app.obtenerProductos();
                }else{
                  toastr.error("Registro no guardado", "Guardar");
                }
            });
        _evt.preventDefault();
      },
      // default
      saveLoadDataFromFile: function(_evt){
        params = { data: this.dataProductsFromFile};
        this.$http
            .post(root + "/guardar-productos-cargados-csv", params)
            .then(function(_response) {
              console.log(_response);
              this.defaultData();
              if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registros guardados", "GUARDAR");
                    app.obtenerTodosProductos();
                }else{
                  toastr.error("Registro no guardado", "GUARDAR");
                }
            });
        _evt.preventDefault();
      },

      loadDataFromFile: function(){
        var formData = new FormData();
        formData.append('foto', this.imagen);
        // var config = { headers: { 'Content-Type': 'multipart/form-data' ,''} };

        this.$http
            .post(root + "/importar-productos-csv", FormData)
            .then(function(_response) {
              console.log(_response);
              if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro guardado", "Guardar");
                }else{
                  toastr.error("Registro no guardado", "Guardar");
                }
            });
      },
        eliminarProducto: function(_id) {
          params = { id_producto: _id};
          swal({
            title: "¿Estas seguro?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          }, function () {
            app.$http
                .post(root + "/eliminar-producto", params)
                .then(function(_response) {
                  if(_response.data.status.code=='PROCESS_COMPLETE'){
                      toastr.success("Registro Eliminado", "Eliminar");
                      app.obtenerTodosProductos();
                  }else{
                    toastr.error("Registro no se pudo eliminar", "Eliminar");
                  }
                });
            swal.close();
          });
        },
         changePage:function(page){
          this.pagination.current_page = page;
          this.obtenerTodosProductos(page);
        },

        obtenerTodosProductos: function(page) {
          params = { page: page};
          this.$http
              .post(root + "/obtener-productos", params)
              .then(function(_response) {
                this.productos = _response.data.data.productos.data;
                this.pagination = _response.data.data.pagination;
                // this.users = _response.data.data.users;
                // this.usuarios = _re
              });
        },
      },
      computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
     searchUserByName: function () {
        // obtenerUsuarios();
        // return this.users.filter((u)=>u.name.includes(this.name));
        if(this.codigo != 0){
          // return this.usuarios.filter((u) => u.name.includes(this.name) u.id_usr.includes(this.codigo)  );
          return this.usuarios.filter((u) => u.name.includes(this.name));
          }else{
          return this.usuarios.filter((u) => u.name.includes(this.name));
        }
      },

      
    }

  });


   $(function(){
      $("input[name='fileCSV']").on("change", function(){
          var formData = new FormData($("#formularioCSV")[0]);
          var ruta = "/importar-productos-csv";
          // alert("add");
          $.ajax({
              url: ruta,
              type: "POST",
              data: formData,
              contentType: false,
              processData: false,
              success: function(_response)
              {
                  // $("#respuesta").html(_response);
                  app.dataProductsFromFile = _response.data;

                  if(_response.status.code=='PROCESS_COMPLETE'){
                    toastr.success("Registro cargado", "CARGAR");
                  }else{
                    toastr.error(_response.status.code, "CARGAR");
                  } 
                  // $("#respuesta").html(app.dataProductsFromFile);

              }
          });
      });
  });

  $(function(){
      $("input[name='fileTXT']").on("change", function(){
          var formData = new FormData($("#formularioTXT")[0]);
          var ruta = "/importar-productos-txt";
          // alert("ad");
          $.ajax({
              url: ruta,
              type: "POST",
              data: formData,
              contentType: false,
              processData: false,
              success: function(_response)
              {
                  // $("#respuesta").html(_response);
                  app.dataProductsFromFile = _response.data;
                  // $("#respuesta").html(app.dataProductsFromFile);
                  if(_response.status.code=='PROCESS_COMPLETE'){
                    toastr.success("Registro cargado", "CARGAR");
                  }else{
                    toastr.error(_response.status.code, "CARGAR");
                  } 
              }
          });
      });
   });




 function onFileChange (input) {
    // if (input.files && input.files[0])
    // {
      var reader = new FileReader();
      reader.onload = function (e)
      {
        app.image=e.target.result;
        app.src = input.files[0];

      };
      reader.readAsDataURL(input.files[0]);
    // }
  }
  $("#fotoRead").change(function ()
  {
    onFileChange(this);

  });

});

