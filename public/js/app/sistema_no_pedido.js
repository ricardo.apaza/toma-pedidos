/* Configuración */
window.addEventListener('load', function () {

  Vue.http.options.root = "/public";

  var app = new Vue({
    el: "#pedidos",
    ready: function() {
      this.load();


    },
    data: function() {
      return {

        nopedidos: {},//checked:false
        nopedido:{codigoCliente:0,nombreCliente:'',FechaHora:'',codigoVendedor:0,nombreVendedor:'',motivo:''},
        nuevo:0,
        con: 0,
        pagination:{  
          'total' :0,
          'current_page': 0,
          'per_page': 0,
          'last_page': 0,
          'from': 0,
          'to': 0,
        },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
      };
    },
      computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
    },
   
    methods: {
      load: function() {
        this.obtenerTodosNoPedidos();
      },

      obtenerTodosNoPedidos: function(page) {
        params = { page: page};
        this.$http.get(root + "/obtener-no-pedidos").then(function(_response) {
          this.nopedidos = _response.data.data.nopedidos.data;
          this.pagination = _response.data.data.pagination;
        });
      },
       changePage:function(page){
        this.pagination.current_page = page;
        this.obtenerTodosNoPedidos(page);
      },
    },
     filters: {
      formatDate: function (value) {
        if (value) {
          return moment(String(value)).format('MM/DD/YYYY')
        }
      },
      formatHour: function (value) {
        if (value) {

            return moment(String(value)).format('hh:mm A ')
        }
      }
    }
  });
});

