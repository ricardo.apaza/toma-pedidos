/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#nuevo_grupo_vendedor",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        grupo_vendedor:{},
        vendedor:{},
        supervisores:{},//Deben tener el perfil de supervisor
        nuevo:0,
      };
    },
    methods: {
      load: function() {
          this.selectData();      
      },
      selectData: function(){
         this.$http.get(root + "/grupo-vendedor/select-data").then(
            function(_response) {
              this.supervisores = _response.data.data.supervisores;
            },
        );
      },
      guardar_grupo_vendedor: function(_evt) {
        this.$http
            .post(root + "/grupo-vendedor/guardar-grupo-vendedor", this.grupo_vendedor)
            .then(function(_response) {              
            if(_response.data.status.code=='PROCESS_COMPLETE'){
                this.grupo_vendedor={};
                this.selectData();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
        // _evt.preventDefault();
      },
    }
  });
});

