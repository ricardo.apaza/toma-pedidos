/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#usuarios",
    ready: function() {
      this.load();

    },
    data: function() {
      return {
        usuarios:{password:''},
        usuario:{password:''},
        users:[],
        nuevo:0,
        pagination:{  
            'total' :0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0,
          },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
      };
    },
    computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
     searchUserByName: function () {
        // obtenerUsuarios();
        // return this.users.filter((u)=>u.name.includes(this.name));
        if(this.codigo != 0){
          // return this.usuarios.filter((u) => u.name.includes(this.name) u.id_usr.includes(this.codigo)  );
          return this.usuarios.filter((u) => u.name.includes(this.name));
          }else{
          return this.usuarios.filter((u) => u.name.includes(this.name));
        }
      },
    },
    methods: {
      load: function() {
        this.obtenerUsuarios();
      },
      nuevaUsuario:function(){
        this.usuario={};
        this.nuevo=1;
      },
    
      obtenerUsuarios: function(page) {
        params = { page: page};
        this.$http
            .post(root + "/obtener-usuarios", params)
            .then(function(_response) {
              this.usuarios = _response.data.data.usuario.data;
              this.pagination = _response.data.data.pagination;
              // this.users = _response.data.data.users;
              // this.usuarios = _re
            });
      },
      obtenerUsuario: function(_perfil, _evt) {
        this.usuario = _perfil;
        this.nuevo=1;
        console.log(this.perfil);
        _evt.preventDefault();
      },
      guardarUsuario: function(_evt) {
        this.$http
            .post(root + "/guardar-usuario", this.usuario)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){

                this.nuevo=0;
                this.usuario={};
                this.obtenerUsuarios();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
        _evt.preventDefault();
      },
      eliminarUsuario: function(_id,_evt) {
        params = { id_usuario: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/eliminar-usuario", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.obtenerUsuarios();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },
      changePage:function(page){
        this.pagination.current_page = page;
        this.obtenerUsuarios(page);
      },


    }

  });

});

