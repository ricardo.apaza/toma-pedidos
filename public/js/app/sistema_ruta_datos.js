/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#rutas",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        ruta:{},
        ruta_editar:{},
        editar:0,
      };
    },
  
    methods: {
      load: function() {
        this.obtenerruta(this.id_ruta);
        // this.obtenervendedorervendedores();
        // this.eliminarElemento('wrapper');
        // alert("aasdsa");
      },
      editarDatos: function(){
        // alert("asds");
        this.editar = 1;
        // alert(this.editar);
      },  
      noEditarDatos: function(){
        // alert("asds");
        this.editar = 0;
        // alert(this.editar);
      },

      


      obtenerruta: function(id){
        params = { id_ruta: id};
        this.$http
            .post(root + "/ruta/obtener-ruta/", params)
            .then(function(_response) {
              this.ruta = _response.data.data.ruta;
              // this.pagination = _response.data.data.pagination;
              // this.users = _response.data.data.users;
              // this.vendedores = _re
            });
      },

      eliminarruta: function(_id) {
        params = { id_ruta: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/ruta/eliminar-ruta", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.retrocederViewVendedores();
                  // app.obtenervendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },

      AgregarRuta: function(_id) {
        // params = { id_ruta: _id};
        params ={};
        swal({
          title: "Agregar ruta",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si",
          closeOnConfirm: false
        }, function () {
                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.retrocederViewVendedores();
                  // app.obtenervendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }
          swal.close();
        });

      },
    guardar_ruta: function(_evt) {
        // alert("ss");
            // alert(this.zona_editar);
        var formData = new FormData();
        this.ruta_editar.id_ruta = this.ruta[0].id;

        for ( var key in this.ruta_editar ) {
          formData.append(key, this.ruta_editar[key]);
        }

        this.$http
            .post(root + "/ruta/guardar-ruta", formData)
            .then(function(_response) {              
              // console.log(_response);
            if(_response.data.status.code=='PROCESS_COMPLETE'){
                    // this.vendedor={};
                    // this.selectData();
                    this.obtenerruta(this.id_ruta);
                    this.noEditarDatos();
                    toastr.success("Registro Correcto", "ACTUALIZAR");
                  }else{
                    toastr.error("Registro Incorrecto", "ACTUALIZAR");
                  }
                });
            // _evt.preventDefault();
          },

      retrocederViewVendedores: function(){
        // alert("aa");
        // $('#btnIrViewVendedores').trigger('click');
        $("#btnIrViewVendedores").click();
      },


     }

  });

});

