/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#rutas",
    ready: function() {
      this.load();

    },
    data: function() {
      return {
        rutas:{},
        ruta:{},
        users:[],
        nuevo:0,
        pagination:{  
            'total' :0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0,
          },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
      };
    },
    computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
  
    },
    methods: {
      load: function() {
        this.obtenerrutas();
      },
      nuevaruta:function(){
        this.ruta={};
        this.nuevo=1;
      },
    
      obtenerrutas: function(page) {
        params = { page: page};
        this.$http
            .post(root + "/ruta/obtener-rutas", params)
            .then(function(_response) {
              this.rutas = _response.data.data.ruta.data;
              this.pagination = _response.data.data.pagination;
              // this.users = _response.data.data.users;
              // this.rutas = _re
            });
      },

       AgregarRuta: function(_id) {
        // params = { id_ruta: _id};
        params ={};
        swal({
          title: "Agregar ruta",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si",
          closeOnConfirm: false
        }, function () {
                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.retrocederViewVendedores();
                  // app.obtenervendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }
          swal.close();
        });

      },



      obtenerruta: function(_perfil, _evt) {
        this.ruta = _perfil;
        this.nuevo=1;
        console.log(this.perfil);
        _evt.preventDefault();
      },
      guardarruta: function(_evt) {
        this.$http
            .post(root + "/ruta/guardar-ruta", this.ruta)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){

                this.nuevo=0;
                this.ruta={};
                this.obtenerrutas();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
        _evt.preventDefault();
      },
      eliminarruta: function(_id,_evt) {
        params = { id_ruta: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Quiero eliminarlo",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/eliminar-ruta", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.obtenerrutas();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },
      changePage:function(page){
        this.pagination.current_page = page;
        this.obtenerrutas(page);
      },


    }

  });

});

