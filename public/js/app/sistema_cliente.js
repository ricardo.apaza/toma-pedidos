/* Configuración */
window.addEventListener('load', function () {

  Vue.http.options.root = "/public";

  var app = new Vue({
    el: "#clientes",
    ready: function() {
      this.load();


    },
    data: function() {
      return {
        clientes: {checked:false},
        cliente: {},
        con: 0,
        ubi: 1,
        nuevo: 0,
        pagination:{  
            'total' :0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0,
        },
        offset:3,
        name:'',
        codigo:'',
        direccion:'',
        dataProductsFromFile:{},
      };
    },
      computed:{
      isActived: function(){
        return this.pagination.current_page;
      },
      pagesNumber: function(){
        if(!this.pagination.to){
          return {};
        }
        var from =  this.pagination.current_page - this.offset;
        if (from < 1) {
          from  = 1;
        }

        var to = from + (this.offset*2);
        if (to >= this.pagination.last_page) {
          to  = this.pagination.last_page;
        }


        var pagesArray = [];

        while(from <= to){
          pagesArray.push(from);
          from++;
        }

        return pagesArray;

      },
     searchUserByName: function () {
        // obtenerUsuarios();
        // return this.users.filter((u)=>u.name.includes(this.name));
        if(this.codigo != 0){
          // return this.usuarios.filter((u) => u.name.includes(this.name) u.id_usr.includes(this.codigo)  );
          return this.usuarios.filter((u) => u.name.includes(this.name));
          }else{
          return this.usuarios.filter((u) => u.name.includes(this.name));
        }
      },
    },
    methods: {
      load: function() {
        this.nuevo=1;
        this.obtenerTodosClientes();
 
      },

      defaultData:function(){
        this.dataProductsFromFile = {};
      },
      saveLoadDataFromFile: function(_evt){
        params = { data: this.dataProductsFromFile};
        this.$http
            .post(root + "/guardar-clientes-cargados-csv", params)
            .then(function(_response) {
              console.log(_response);
              this.defaultData();
              if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registros guardados", "GUARDAR");
                    app.obtenerTodosClientes();
                }else{
                  toastr.error("Registro no guardado", "GUARDAR");
                }
            });
        _evt.preventDefault();
      },
      nuevoCliente:function(){
        this.correntista= {};
        this.con= 0;
        this.nuevo=1;
      },
       changePage:function(page){
        this.pagination.current_page = page;
        this.obtenerTodosClientes(page);
      },
 
      obtenerTodosClientes: function(page) {
        params = { page: page};
        
        this.$http.post(root + "/obtener-clientes", params).then(function(_response) {
          this.clientes = _response.data.data.clientes.data;
          this.pagination = _response.data.data.pagination;
        });
      },
     guardarCliente: function(_evt) {

        var formData = new FormData();
        for ( var key in this.cliente ) {
          formData.append(key, this.cliente[key]);
        }
        this.$http
            .post(root + "/guardar-cliente", formData)
            .then(function(_response) {
              this.cliente={};
              this.nuevo=0;
              console.log(_response);
              if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro guardado", "Guardar");
                }else{
                  toastr.error("Registro no guardado", "Guardar");
                }
            });
        _evt.preventDefault();
      },
        eliminarCliente: function(_id,_evt) {
        params = { id_cliente: _id};
        swal({
          title: "¿Estas seguro?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        }, function () {
          app.$http
              .post(root + "/eliminar-cliente", params)
              .then(function(_response) {
                if(_response.data.status.code=='PROCESS_COMPLETE'){
                    toastr.success("Registro Eliminado", "Eliminar");
                    app.obtenerTodosClientes();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }
              });
          swal.close();
        });
        _evt.preventDefault();
      },
    

    }

  });


   $(function(){
      $("input[name='fileCSV']").on("change", function(){
          var formData = new FormData($("#formularioCSV")[0]);
          var ruta = "/importar-clientes-csv";
          // alert("add");
          $.ajax({
              url: ruta,
              type: "POST",
              data: formData,
              contentType: false,
              processData: false,
              success: function(_response)
              {
                  // $("#respuesta").html(_response);
                  app.dataProductsFromFile = _response.data;

                  if(_response.status.code=='PROCESS_COMPLETE'){
                    toastr.success("Registro cargado", "CARGAR");
                  }else{
                    toastr.error(_response.status.code, "CARGAR");
                  } 
                  // $("#respuesta").html(app.dataProductsFromFile);

              }
          });
      });
  });

  $(function(){
      $("input[name='fileTXT']").on("change", function(){
          var formData = new FormData($("#formularioTXT")[0]);
          var ruta = "/importar-clientes-txt";
          // alert("ad");
          $.ajax({
              url: ruta,
              type: "POST",
              data: formData,
              contentType: false,
              processData: false,
              success: function(_response)
              {
                  // $("#respuesta").html(_response);
                  app.dataProductsFromFile = _response.data;
                  // $("#respuesta").html(app.dataProductsFromFile);
                  if(_response.status.code=='PROCESS_COMPLETE'){
                    toastr.success("Registro cargado", "CARGAR");
                  }else{
                    toastr.error(_response.status.code, "CARGAR");
                  } 
              }
          });
      });
   });
});

