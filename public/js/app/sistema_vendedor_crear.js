/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#NuevoVendedor",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        grupos_vendedores:{},
        vendedor:{},
        usuarios:{},
        nuevo:0,
      };
    },
    methods: {
      load: function() {
          this.selectData();      
      },

      selectData: function(){
         this.$http.get(root + "/vendedor/select-data").then(
            function(_response) {
              this.grupos_vendedores = _response.data.data.grupos_vendedores;
              this.usuarios = _response.data.data.usuarios;
            },
            function(response) {
              console.log(response);
            }
        );
      },
        guardarVendedor: function(_evt) {
        this.$http
            .post(root + "/vendedor/guardar-vendedor", this.vendedor)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                this.vendedor={};
                this.selectData();
                toastr.success("Registro Correcto", "Guardar");
              }else{
                toastr.error("Registro Incorrecto", "Guardar");
              }
            });
        _evt.preventDefault();
      },
      

    }

  });

});

