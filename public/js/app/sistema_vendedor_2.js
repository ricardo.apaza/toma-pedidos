/* Configuración */
window.addEventListener('load', function () {

  var app = new Vue({
    el: "#vendedores",
    ready: function() {
      this.load();
    },
    data: function() {
      return {
        id_vend:"",
        vendedor_editar:{id_vendedor:0},
        vendedor:{},
        editar:0,
        grupos_vendedores:{},
        usuarios:{},
      };
    },
  
    methods: {
      load: function() {
        this.obtenervendedor(this.id_vend);
        this.selectData();   
      },
      editarDatos: function(){
        // alert("asds");
        this.editar = 1;
        // alert(this.editar);
      },
      noEditarDatos: function(){
        // alert("asds");
        this.editar = 0;
        // alert(this.editar);
      },
        selectData: function(){
         this.$http.get(root + "/vendedor/select-data").then(
            function(_response) {
              this.grupos_vendedores = _response.data.data.grupos_vendedores;
              this.usuarios = _response.data.data.usuarios;
            },
            function(response) {
              console.log(response);
            }
        );
      },
      obtenervendedor: function(id){
        params = { id_vendedor: id};
        this.$http
            .post(root + "/obtener-vendedor", params)
            .then(function(_response) {
              this.vendedor = _response.data.data.vendedor;
              // this.pagination = _response.data.data.pagination;
              // this.users = _response.data.data.users;
              // this.vendedores = _re
            });
      },
      guardarVendedor: function(_evt) {
        var formData = new FormData();
        this.vendedor_editar.id_vendedor = this.vendedor[0].id;
        for ( var key in this.vendedor_editar ) {
          formData.append(key, this.vendedor_editar[key]);
        }
        this.$http
            .post(root + "/vendedor/guardar-vendedor", formData)
            .then(function(_response) {              
              console.log(_response);
        if(_response.data.status.code=='PROCESS_COMPLETE'){
                // this.vendedor={};
                this.selectData();
                this.obtenervendedor(this.id_vend);
                this.noEditarDatos();
                toastr.success("Registro Correcto", "ACTUALIZAR");
              }else{
                toastr.error("Registro Incorrecto", "ACTUALIZAR");
              }
            });
        _evt.preventDefault();
      },

      eliminarvendedor: function(_id) {
        params = { id_vendedor: _id};
        swal({
          title: "Confirma eliminación",
          text: "¿Esta seguro que desea eliminar el vendedor VENDEDOR001?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Eliminar",
          closeOnConfirm: false
        }, function () {

          app.$http
              .post(root + "/eliminar-vendedor", params)
              .then(function(_response) {

                if(_response.data.status.code=='PROCESS_COMPLETE'){
                  toastr.success("Registro Eliminado", "Eliminar");
                  app.retrocederViewVendedores();
                  app.obtenervendedores();
                }else{
                  toastr.error("Registro no se pudo eliminar", "Eliminar");
                }

              });
          swal.close();
        });

      },

      retrocederViewVendedores: function(){
        // alert("aa");
        // $('#btnIrViewVendedores').trigger('click');
        $("#btnIrViewVendedores").click();
      },


     }

  });

});

