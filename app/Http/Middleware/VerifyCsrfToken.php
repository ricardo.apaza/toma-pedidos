<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/obtener-pedido-detalles',
        '/producto-editar',
        '/guardar-producto',
        '/eliminar-producto',
        '/cliente-editar',
        '/guardar-cliente',
        '/eliminar-cliente',
        '/obtener-usuarios',
        '/guardar-usuario',
        '/eliminar-usuario',
        '/descargar-excel',
        '/obtener-productos',
        '/obtener-usuarios',
        '/obtener-clientes',
        '/obtener-pedidos',
        'importar-productos-csv',
        'importar-productos-txt',
        'guardar-productos-cargados-csv',
        'importar-clientes-csv',
        'importar-clientes-txt',
        'guardar-clientes-cargados-csv',
        '/descargar-producto-csv',
        '/descargar-producto-txt',

        //v2
        '/obtener-vendedores',
        '/guardar-vendedor',
        '/eliminar-vendedor',

        //ruta
        '/ruta/obtener-rutas',
        '/ruta/obtener-ruta',
        '/ruta/guardar-ruta',
        '/ruta/eliminar-ruta',
        '/ruta/ruta-datos/{id}',

        //cliente
        '/cliente/obtener-clientes',
        '/cliente/obtener-cliente',
        '/cliente/guardar-cliente',
        '/cliente/eliminar-cliente',
        '/cliente/cliente-datos/{id}',
        '/cliente/obtener-datos-adicionales-cliente',




        //zona
        '/grupo-vendedor/obtener-grupos-vendedores',
        '/grupo-vendedor/guardar-grupo-vendedor',
        '/grupo-vendedor/eliminar-grupo-vendedor',
        '/grupo-vendedor/obtener-grupo-vendedor',
        
        //grupo-vendedor/obtener-grupos-vendedores


        '/grupo-vendedor/grupo-vendedor-datos/{id}',

        '/vendedor/vendedor-datos/{id}',
        '/vendedor/vendedor-datos',
        '/obtener-vendedor',
        '/vendedor/select-data',
        '/vendedor/guardar-vendedor',



        '/cliente-direccion/guardar-cliente-direccion',
        '/cliente-direccion/eliminar-cliente-direccion',
        '/cliente-direccion/obtener-direccion',


        '/cliente-email/guardar-cliente-email',
        '/cliente-email/eliminar-cliente-email',
        '/cliente-email/obtener-email',

        '/cliente-telefono/guardar-cliente-telefono',
        '/cliente-telefono/eliminar-cliente-telefono',
        '/cliente-telefono/obtener-telefono',


        '/pedidos/obtener-pedido-detalles',
        '/pedidos/obtener-datos-adicionales',
        '/pedidos/pedido-topdf'

    ];
}
