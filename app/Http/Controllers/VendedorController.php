<?php

namespace App\Http\Controllers;

use Illuminate\Database\Seeder;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
class VendedorController extends Controller
{
    // public function __construct()
    // {$this->middleware('auth');}



    // public function datosVendedor(Request $request, $id){

    //     return view('home');
    //     // dd($id,$request);
    //     // return $id;
    // }


    public function selectDataVendedor(){

        $r =new ApiResponse();
        $grupos_vendedores = DB::table('tbl_grupo_vendedor')->select('tbl_grupo_vendedor.id','tbl_grupo_vendedor.codigo')->get();
        $usuarios = DB::table('tbl_usuario')->select('tbl_usuario.id','tbl_usuario.name')->get();

        $r->data['grupos_vendedores']=$grupos_vendedores;
        $r->data['usuarios']=$usuarios;
        return response()->json($r);
    }
    public function createController()
    {   
        return view('vendedores.vendedores.vendedor_crear');
    }
    public function datosVendedor(Request $request,$id)
    {	
	  	$vendedor = Vendedor::orderBy('id','ASC')
    				->where('id','=',$id)
    				->get();	

        return view('vendedores.vendedores.vendedor_datos', compact('vendedor'));
    }

    public function obtenerVendedores(Request $request)
    {
        
        $page = $request->get('page');
        $r = new ApiResponse();
        //Los usuarios no tiene definido el id_perfil
        $vendedor = DB::table('tbl_vendedor')
            ->join('tbl_usuario', 'tbl_vendedor.id_usuario', '=', 'tbl_usuario.id')
            ->join('tbl_grupo_vendedor', 'tbl_vendedor.id_grupo_vendedor', '=', 'tbl_grupo_vendedor.id')
            ->join('tbl_perfil', 'tbl_usuario.id_perfil', '=', 'tbl_perfil.id')
            ->whereNull('tbl_vendedor.deleted_at')
            ->select(DB::raw("tbl_vendedor.id,tbl_vendedor.codigo as codigo_vendedor, concat(tbl_vendedor.nombre,', ',tbl_vendedor.apellido) as nombre_vendedor, tbl_perfil.codigo as perfil_vendedor, tbl_grupo_vendedor.descripcion as descripcion_grupo_vendedor"))
            ->orderBy('tbl_vendedor.id','ASC')
            ->paginate(5);

        // $vendedor = User::all();
        $pagination = [
            'total' => $vendedor->total(),
            'current_page' => $vendedor->currentPage(),
            'per_page' => $vendedor->perPage(),
            'last_page' => $vendedor->lastPage(),
            'from' => $vendedor->firstItem(),
            'to' => $vendedor->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['vendedor'] = $vendedor;
        // $r->data['users'] = $users;
        return response()->json($r);
    }

    public function obtenerVendedor(Request $request)
    {
        $id = $request->get('id_vendedor');
        $r = new ApiResponse();

        $vendedor = Vendedor::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    
        $r->data['vendedor'] = $vendedor;
        return response()->json($r);
    }


    

    public function vendedorInsertar(Request $request)
    {
        $r = new ApiResponse();

        $datavendedor = $request->all();
        // dd($datavendedor);

        if ($request->get('id_vendedor', 0) == 0) {
            $vendedor = new Vendedor();

            $count=Vendedor::count();
            $count++;
            $datavendedor['codigo']='VENDEDOR'.$count;
        } else {
            $vendedor = Vendedor::find($request->get('id_vendedor'));
        }
        $validate=$vendedor->isValid($datavendedor);
        if ($validate->passes()) {
            $vendedor->fill($datavendedor);
            $vendedor->save();

        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        $r->data = $vendedor;
        return response()->json($r);
    }
    public function vendedorEliminar(Request $request)
    {
        $r = new ApiResponse();
        if ($request->get('id_vendedor', 0) != 0) {
            $id=$request->get('id_vendedor');
            $vendedor = Vendedor::find($id);
            $vendedor->delete();
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
        // return view('vendedores.vendedores.vendedores');
    }
}
