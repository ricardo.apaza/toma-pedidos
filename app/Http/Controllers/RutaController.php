<?php

namespace App\Http\Controllers;

use Illuminate\Database\Seeder;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\Ruta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class RutaController extends Controller
{
    public function __construct()
    {$this->middleware('auth');}
    
    public function datosRuta(Request $request,$id)
    {   
        $ruta = Ruta::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    
        // $ruta = DB::table('tbl_ruta')
        //     ->join('tbl_vendedor', 'tbl_ruta.id_vendedor', '=', 'tbl_vendedor.id')
        //     ->join('tbl_cliente', 'tbl_ruta.id_cliente', '=', 'tbl_cliente.id')
        //     ->select(DB::raw("tbl_ruta.id,concat(tbl_cliente.nombre,', ', tbl_cliente.apellido ) as nombre_cliente, concat(tbl_vendedor.nombre,', ', tbl_vendedor.apellido ) as nombre_vendedor, tbl_ruta.fecha_inicio, tbl_ruta.fecha_fin as fecha_final ,tbl_ruta.dia_semana"))
        //     ->where('tbl_ruta.id','=',$id)
        //     ->orderBy('tbl_ruta.id','ASC');
        //     ->get();
            // ->paginate(5);
            // dd($ruta);
        return view('vendedores.rutas.ruta_datos', compact('ruta'));
    }

    public function createController()
    {   
        // return view('vendedores.rutas.ruta_datps');
    }
    public function obtenerRuta(Request $request)
    {
        $id = $request->get('id_ruta');
        $r = new ApiResponse();

        $ruta= Ruta::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    
        $r->data['ruta'] = $ruta;
        return response()->json($r);

    }

    public function obtenerRutas(Request $request)
    {
        
        $page = $request->get('page');
        $r = new ApiResponse();
        // $users = User::all();
        // $ruta = Ruta::orderBy('id','ASC')->paginate(5);

        $ruta = DB::table('tbl_ruta')
            ->join('tbl_vendedor', 'tbl_ruta.id_vendedor', '=', 'tbl_vendedor.id')
            ->join('tbl_cliente', 'tbl_ruta.id_cliente', '=', 'tbl_cliente.id')
            ->select(DB::raw("tbl_ruta.id,concat(tbl_cliente.nombre,', ', tbl_cliente.apellido ) as nombre_cliente, concat(tbl_vendedor.nombre,', ', tbl_vendedor.apellido ) as nombre_vendedor, tbl_ruta.fecha_inicio, tbl_ruta.fecha_fin as fecha_final ,tbl_ruta.dia_semana"))
            ->whereNull('tbl_ruta.deleted_at')
            ->orderBy('tbl_ruta.id','ASC')
            ->paginate(5);

        // $ruta = User::all();
        $pagination = [
            'total' => $ruta->total(),
            'current_page' => $ruta->currentPage(),
            'per_page' => $ruta->perPage(),
            'last_page' => $ruta->lastPage(),
            'from' => $ruta->firstItem(),
            'to' => $ruta->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['ruta'] = $ruta;
        // $r->data['users'] = $users;
        return response()->json($r);
    }
    

    public function rutaInsertar(Request $request)
    {
        $r = new ApiResponse();

        $dataruta = $request->all();

        if ($request->get('id_ruta', 0) == 0) {
            $ruta = new ruta();
        } else {
            $ruta = Ruta::find($request->get('id_ruta'));
        }
        $validate=$ruta->isValid($dataruta);
        if ($validate->passes()) {
            $ruta->fill($dataruta);
            $ruta->save();

        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        $r->data = $ruta;
        return response()->json($r);
    }
    public function rutaEliminar(Request $request)
    {
        $r = new ApiResponse();
        if ($request->get('id_ruta', 0) != 0) {
            $id=$request->get('id_ruta');
            $ruta = Ruta::find($id);
            $ruta->delete();
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }
}
