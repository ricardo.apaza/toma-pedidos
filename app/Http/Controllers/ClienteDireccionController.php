<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\ClienteDireccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClienteDireccionController extends Controller
{


	public function clienteDireccionInsertar(Request $request){

        $r = new ApiResponse();
        $datacliente = $request->all();
        $datacliente['ubigeo'] = '';
        $datacliente['latitud'] = '';
        $datacliente['longitud'] = '';
        if ($datacliente['principal'] =='true') {
        	$datacliente['principal'] = 1;
        }else{
        	$datacliente['principal'] = 0;
        }

        if ($request->get('id_direccion', 0) == 0) {
            $cliente_direccion = new ClienteDireccion();
        } else {
            $cliente_direccion = ClienteDireccion::find($request->get('id_direccion'));
        }
        $validate=$cliente_direccion->isValid($datacliente);

        if ($validate->passes()) {
            $cliente_direccion->fill($datacliente);
            $cliente_direccion->save();
        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        // $r->data = $cliente_direccion;
        return response()->json($r);
    }
    


	public function obtenerClienteDireccion(Request $request)
    {
  		$r = new ApiResponse();
        // $users = User::all();
        $id=$request->get('id_direccion');
        $cliente_direccion =  DB::table('tbl_cliente_direccion')
                ->select(DB::raw('tbl_cliente_direccion.id,tbl_cliente_direccion.direccion,tbl_cliente_direccion.tipo_direccion,tbl_cliente_direccion.principal'))
                ->where('tbl_cliente_direccion.id','=',$id )
                ->orderBy('tbl_cliente_direccion.id','ASC')
                ->get();
       
        $r->data['cliente_direccion'] = $cliente_direccion;
        return response()->json($r);
    }
    public function clienteDireccionEliminar(Request $request)
    {
        $r = new ApiResponse();

        if ($request->get('id_direccion', 0) != 0) {
            $id=$request->get('id_direccion');
            // dd($id);
            $direccion = ClienteDireccion::find($id);
            if (is_null($direccion)) {
    	        $r->status->setStatus(Status::ERROR_PARAMS);
	        }else{
            	$direccion->delete();
            }
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }
    
}
