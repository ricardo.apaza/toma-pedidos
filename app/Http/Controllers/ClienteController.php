<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;

use App\Models\Cliente;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function clienteEliminar(Request $request)
    {
        $r = new ApiResponse();

        if ($request->get('id_cliente', 0) != 0) {
            $id=$request->get('id_cliente');
            $cliente = Cliente::find($id);
            $cliente->delete();
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
        // return view('clientes.clientes');
    }
    


    public function obtenerDatosAdicionalesCliente(Request $request){

        $r =new ApiResponse();
        $id=$request->get('id_cliente');
        $cliente_direcciones = DB::table('tbl_cliente_direccion')
                ->join('tbl_cliente', 'tbl_cliente_direccion.id_cliente', '=', 'tbl_cliente.id')
                ->where('tbl_cliente.id','=',$id)
                ->whereNull('tbl_cliente_direccion.deleted_at')

                ->select(DB::raw("tbl_cliente_direccion.id as id_direccion, tbl_cliente_direccion.direccion as direccion, tbl_cliente_direccion.tipo_direccion as tipo_direccion, tbl_cliente_direccion.principal as principal"))
                ->orderBy('tbl_cliente_direccion.id','ASC')
                ->get();
        $cliente_telefonos = DB::table('tbl_cliente_telefono')
                ->join('tbl_cliente', 'tbl_cliente_telefono.id_cliente', '=', 'tbl_cliente.id')
                ->where('tbl_cliente.id','=',$id)
                ->select(DB::raw("tbl_cliente_telefono.id as id_telefono, tbl_cliente_telefono.telefono as telefono, tbl_cliente_telefono.tipo_telefono as tipo_telefono, tbl_cliente_telefono.principal as principal"))
                ->whereNull('tbl_cliente_telefono.deleted_at')

                ->orderBy('tbl_cliente_telefono.id','ASC')
                ->get();
        $cliente_emails = DB::table('tbl_cliente_email')
                ->join('tbl_cliente', 'tbl_cliente_email.id_cliente', '=', 'tbl_cliente.id')
                ->where('tbl_cliente.id','=',$id)
                ->select(DB::raw("tbl_cliente_email.id as id_email, tbl_cliente_email.email as email, tbl_cliente_email.tipo_email as tipo_email, tbl_cliente_email.principal as principal"))
                ->whereNull('tbl_cliente_email.deleted_at')

                ->orderBy('tbl_cliente_email.id','ASC')
                ->get();
    
        $r->data['cliente_direcciones']=$cliente_direcciones;
        $r->data['cliente_telefonos']=$cliente_telefonos;
        $r->data['cliente_emails']=$cliente_emails;
        return response()->json($r);
    }

    public function obtenerCliente(Request $request)
    {
        $id = $request->get('id_cliente');
        $r = new ApiResponse();

        $cliente = DB::table('tbl_cliente')
                ->where('tbl_cliente.id','=',$id)
                ->get();
        $r->data['cliente'] = $cliente;
        return response()->json($r);
    }



    public function selectDataCliente($request, $id){

        $r =new ApiResponse();
        $cliente_direcciones = DB::table('tbl_cliente_direccion')
                ->join('tbl_cliente', 'tbl_cliente_direccion.id_cliente', '=', 'tbl_cliente.id')
                ->where('tbl_cliente.id','=',$id)
                ->get();
        // $usuarios = DB::table('tbl_usuario')->select('tbl_usuario.id','tbl_usuario.name')->get();
        // $usuarios = DB::table('tbl_usuario')->select('tbl_usuario.id','tbl_usuario.name')->get();

        // $r->data['zonas']=$zonas;
        // $r->data['usuarios']=$usuarios;
        $r->data['cliente_direcciones']=$cliente_direcciones;
        return response()->json($r);
    }

    public function createController()
    {   
        return view('clientes.cliente_crear');
    }

    public function datosCliente(Request $request,$id)
    {   
        $cliente = Cliente::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    

        return view('clientes.cliente_datos', compact('cliente'));
    }







    public function clienteImportadosInsertar(Request $request){

        $r = new ApiResponse();
        $dataclientes = $request->all();

        foreach ($dataclientes['data'] as $NuevoCliente) {
            $cliente = new Cliente();
            // dd($dataclientes);
            $validate=$cliente->isValid($NuevoCliente);

            if ($validate->passes()) {
                $cliente->fill($NuevoCliente);
                $cliente->save();

            }else{
                $r->error=$validate->errors();
                $r->status->setStatus(Status::ERROR_PARAMS);
            }
        }
        // dd($dataclientes);
        // $r->data = $cliente;
        return response()->json($r);
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function obtenerClientes(Request $request)
    {
        $r = new ApiResponse();
        // $clientes = Cliente::orderBy('id','ASC')->paginate(5);


        $clientes = DB::table('tbl_cliente')
                // ->join('tbl_cliente_direccion', 'tbl_cliente_direccion.id_cliente', '=', 'tbl_cliente.id')
                // ->leftjoin('tbl_cliente_telefono', 'tbl_cliente_telefono.id_cliente', '=', 'tbl_cliente.id')
                // ->leftjoin('tbl_cliente_email', 'tbl_cliente_email.id_cliente', '=', 'tbl_cliente.id')
                // ->where('tbl_cliente_telefono.principal','=',1)
                // ->andWhere('tbl_cliente_direccion.principal','=',1)
                // ->andWhere('tbl_cliente_email.principal','=',1)
                ->select(DB::raw('tbl_cliente.id,tbl_cliente.codigo,tbl_cliente.nombre,tbl_cliente.giro'))
                // ->where([
                //        'tbl_cliente_telefono.principal' => 1,
                //        'tbl_cliente_direccion.principal' => 1,
                //        'tbl_cliente_email.principal' => 1
                // ])
                ->whereNull('tbl_cliente.deleted_at')

                ->orderBy('tbl_cliente.id','ASC')
                ->paginate(5);

        $page = $request->get('page');

        $pagination = [
            'total' => $clientes->total(),
            'current_page' => $clientes->currentPage(),
            'per_page' => $clientes->perPage(),
            'last_page' => $clientes->lastPage(),
            'from' => $clientes->firstItem(),
            'to' => $clientes->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['clientes'] = $clientes;
        return response()->json($r);


    }

    public function viewClienteEditar()
    {
        return view('cliente.cliente_crear_editar');
    }
    
    public function clienteInsertar(Request $request){

        $r = new ApiResponse();
        $datacliente = $request->all();

        if ($request->get('id_cliente', 0) == 0) {
            $cliente = new Cliente();
        } else {
            $cliente = Cliente::find($request->get('id_cliente'));
        }
        $validate=$cliente->isValid($datacliente);

        if ($validate->passes()) {

            $cliente->fill($datacliente);
            $cliente->save();

        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        $r->data = $cliente;
        return response()->json($r);
    }
    






    
}
