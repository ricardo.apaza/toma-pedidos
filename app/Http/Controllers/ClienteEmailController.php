<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\ClienteEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClienteEmailController extends Controller
{
    
	public function clienteEmailInsertar(Request $request){

        $r = new ApiResponse();
        $datacliente = $request->all();
        
        $datacliente['tipo_email']='';

        if ($datacliente['principal'] =='true') {
        	$datacliente['principal'] = 1;
        }else{
        	$datacliente['principal'] = 0;
        }

        if ($request->get('id_email', 0) == 0) {
            $cliente_email = new ClienteEmail();
        } else {
            $cliente_email = ClienteEmail::find($request->get('id_email'));
        }
        $validate=$cliente_email->isValid($datacliente);

        if ($validate->passes()) {
            $cliente_email->fill($datacliente);
            $cliente_email->save();
        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        return response()->json($r);
    }
    


	public function obtenerClienteEmail(Request $request)
    {
  		$r = new ApiResponse();
        // $users = User::all();
        $id=$request->get('id_email');
        $cliente_email =  DB::table('tbl_cliente_email')
                ->select(DB::raw('tbl_cliente_email.id,tbl_cliente_email.email,tbl_cliente_email.tipo_email,tbl_cliente_email.principal'))
                ->where('tbl_cliente_email.id','=',$id )
                ->orderBy('tbl_cliente_email.id','ASC')
                ->get();
       
        $r->data['cliente_email'] = $cliente_email;
        return response()->json($r);
    }
    public function clienteEmailEliminar(Request $request)
    {
        $r = new ApiResponse();

        if ($request->get('id_email', 0) != 0) {
            $id=$request->get('id_email');
            // dd($id);
            $email = ClienteEmail::find($id);
            if (is_null($email)) {
    	        $r->status->setStatus(Status::ERROR_PARAMS);
	        }else{
            	$email->delete();
            }
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }}
