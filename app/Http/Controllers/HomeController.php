<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

  
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */



    public function viewInicio()
    {
        return view('home');
    }






    public function index()
    {
        return view('index');
    }

    public function viewClientes()
    {
        return view('clientes.clientes');
    }
    public function viewProductos()
    {
        return view('mantenimientos.productos');
    }
    public function viewUsuarios()
    {
        return view('mantenimientos.usuarios');
    }

    public function viewNoPedidos()
    {
        return view('pedidos.no-pedidos');
    }
    public function viewPedidos()
    {
        return view('pedidos.pedidos.pedidos');
    }


    public function viewCotizaciones()
    {
        return view('pedidos.cotizaciones');
    }
    public function viewVisitas()
    {
        return view('pedidos.visitas');
    }


    public function viewIntegracion1()
    {
        return view('integracion.integracion1');
    }
    public function viewExportar()
    {
        return view('herramientas.exportar');
    }
    public function viewImportar()
    {
        return view('herramientas.importar');
    }
    //v2
    public function viewVendedores()
    {
        return view('vendedores.vendedores.vendedores');
    }
    public function viewGrupoVendedor()
    {
        return view('vendedores.grupo_vendedor.grupo_vendedor');
    }
    public function viewRutas()
    {
        return view('vendedores.rutas.rutas');
    }
    

    /*clientes*/
    public function viewClientesDirecciones()
    {
        return view('clientes.clientes_direcciones');
    }
    public function viewClientesTelefono()
    {
        return view('clientes.clientes_telefonos');
    }
    public function viewClientesEmails()
    {
        return view('clientes.clientes_emails');
    }


}
