<?php

namespace App\Http\Controllers;

use Illuminate\Database\Seeder;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }
    public function obtenerUsuarios(Request $request)
    {
        

        // $page = $request->get('page');

        $r = new ApiResponse();
        // $users = User::all();
        $usuario = User::orderBy('id','ASC')
                    ->whereNull('tbl_ruta.deleted_at')
                    ->paginate(5);
        // $usuario = User::all();
        $pagination = [
            'total' => $usuario->total(),
            'current_page' => $usuario->currentPage(),
            'per_page' => $usuario->perPage(),
            'last_page' => $usuario->lastPage(),
            'from' => $usuario->firstItem(),
            'to' => $usuario->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['usuario'] = $usuario;
        // $r->data['users'] = $users;
        return response()->json($r);
    }
    

       // DB::table('TBL_USR')->insert([
       //      'email' => 'wchoque',
       //      'password' => bcrypt('Abc123'),
       //      'name'=>'wchoque',
       //  ]);

    public function usuarioInsertar(Request $request)
    {
        $r = new ApiResponse();

        $datausuario = $request->all();

        if ($request->get('id_usuario', 0) == 0) {
            $usuario = new User();
        } else {
            $usuario = User::find($request->get('id_usuario'));
        }
        $validate=$usuario->isValid($datausuario);
        if ($validate->passes()) {
            $usuario['password'] = bcrypt($usuario['password']);
            $usuario->fill($datausuario);
            $usuario->save();

        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        $r->data = $usuario;
        return response()->json($r);
    }
    public function usuarioEliminar(Request $request)
    {
        $r = new ApiResponse();
        if ($request->get('id_usuario', 0) != 0) {
            $id=$request->get('id_usuario');
            $usuario = User::find($id);
            $usuario->delete();
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }
}
