<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\Pedido;
use App\Models\Detallepedido;
use App\Models\Cliente;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PDF;



class PedidoController extends Controller
{
    public function pedidoToPdf(Request $request)
    {
        $id = $request->get('id_pedido');

        $pedido= DB::table('tbl_pedido')
            ->join('tbl_vendedor', 'tbl_pedido.id_vendedor', '=', 'tbl_vendedor.id')
            ->join('tbl_cliente', 'tbl_pedido.id_cliente', '=', 'tbl_cliente.id')

            ->select('tbl_pedido.id_vendedor','tbl_vendedor.nombre as nombre_vendedor','tbl_pedido.id_cliente','tbl_cliente.nombre as nombre_cliente','tbl_cliente.apellido as apellido_cliente','tbl_pedido.fecha_inicio','tbl_pedido.hora_inicio','tbl_pedido.monto_total','tbl_pedido.id','tbl_pedido.codigo')
            ->where('tbl_pedido.id',$id)
            ->get();

        $detalles= DB::table('tbl_pedido_detalle')
            ->join('tbl_pedido', 'tbl_pedido_detalle.id_pedido', '=', 'tbl_pedido.id')
            ->join('tbl_producto', 'tbl_pedido_detalle.id_producto', '=', 'tbl_producto.id')

            ->select('tbl_pedido_detalle.id_producto','tbl_producto.descripcion  as nombre_producto','tbl_pedido_detalle.precio_unitario','tbl_pedido_detalle.cantidad','tbl_pedido_detalle.subtotal','tbl_pedido_detalle.descuento_porcentaje as descuento')
            ->where('tbl_pedido_detalle.id_pedido',$id)
            ->get();
        // dd($detalles);
        $id_cliente = DB::table('tbl_pedido')
            ->join('tbl_cliente', 'tbl_pedido.id_cliente', '=', 'tbl_cliente.id')
            ->select('tbl_pedido.id_cliente')
            ->where('tbl_pedido.id',$id)
            ->get();
            // dd($id_cliente);

        $direccion_principal = DB::table('tbl_cliente_direccion')
            ->select('tbl_cliente_direccion.direccion')
            ->where('tbl_cliente_direccion.id_cliente','=',$id_cliente[0]->id_cliente)
            ->where('tbl_cliente_direccion.principal','=',1)
            ->get();

        // if (empty($direccion_principal)) {
        //     $direccion_principal[0] = 'Sin direccion principal';
        // }
            // dd($direccion_principal);



        $pdf = PDF::loadView('pedidos.pedidos.pedidoToPdf', ['pedido'=>$pedido,'detalles'=>$detalles,'direccion_principal'=>$direccion_principal]);
        return $pdf->download('pedidos.pdf');
    }
    public function pedidoDatos(Request $request,$id)
    {   
        $pedido = Pedido::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    

        return view('pedidos.pedidos.pedido_datos', compact('pedido'));
    }






    public function obtenerPedidos(Request $request)
    {
        $r = new ApiResponse();

        $page = $request->get('page');
        
        $pedidos = DB::table('tbl_pedido')
            // ->join('tbl_usuario', 'tbl_pedido.id_usuario', '=', 'tbl_usuario.id')
            ->join('tbl_vendedor', 'tbl_pedido.id_vendedor', '=', 'tbl_vendedor.id')
            ->join('tbl_cliente', 'tbl_pedido.id_cliente', '=', 'tbl_cliente.id')

            ->select('tbl_pedido.id_vendedor as id_vendedor','tbl_vendedor.nombre as nombre_vendedor','tbl_pedido.id_cliente','tbl_cliente.nombre as nombre_cliente' ,'tbl_pedido.fecha_inicio','tbl_pedido.hora_inicio','tbl_pedido.monto_total','tbl_pedido.id')
            //->where('correntista_datos.principal',1)
            ->orderBy('tbl_pedido.id','ASC')->paginate(5);

        $pagination = [
            'total' => $pedidos->total(),
            'current_page' => $pedidos->currentPage(),
            'per_page' => $pedidos->perPage(),
            'last_page' => $pedidos->lastPage(),
            'from' => $pedidos->firstItem(),
            'to' => $pedidos->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['pedidos'] = $pedidos;
        return response()->json($r);

    }





    
    public function obtenerPedidoDetalles(Request $request){
        $r =new ApiResponse();

        $id = $request->get('id_pedido');

        $pedido= DB::table('tbl_pedido')
            ->join('tbl_vendedor', 'tbl_pedido.id_vendedor', '=', 'tbl_vendedor.id')
            ->join('tbl_cliente', 'tbl_pedido.id_cliente', '=', 'tbl_cliente.id')

            ->select('tbl_pedido.id_vendedor','tbl_vendedor.nombre as nombre_vendedor','tbl_pedido.id_cliente','tbl_cliente.nombre as nombre_cliente','tbl_cliente.apellido as apellido_cliente','tbl_pedido.fecha_inicio','tbl_pedido.hora_inicio','tbl_pedido.monto_total','tbl_pedido.id')
            ->where('tbl_pedido.id',$id)
            ->get();

        $detalles= DB::table('tbl_pedido_detalle')
            ->join('tbl_pedido', 'tbl_pedido_detalle.id_pedido', '=', 'tbl_pedido.id')
            ->join('tbl_producto', 'tbl_pedido_detalle.id_producto', '=', 'tbl_producto.id')

            ->select('tbl_pedido_detalle.id_producto','tbl_producto.descripcion  as nombre_producto','tbl_pedido_detalle.precio_unitario','tbl_pedido_detalle.cantidad','tbl_pedido_detalle.subtotal','tbl_pedido_detalle.descuento_porcentaje as descuento')
            ->where('tbl_pedido_detalle.id_pedido',$id)
            ->get();


        $r->data['pedido']=$pedido;
        $r->data['detalles']=$detalles;
        //dd($r);

        return response()->json($r);
    }

    public function obtenerPedidoDatosAdicionales(Request $request){
        $r =new ApiResponse();

        $id = $request->get('id_cliente');
        // dd($id);
        $direccion_principal= DB::table('tbl_cliente_direccion')
            ->select('tbl_cliente_direccion.direccion')
            // ->whereColumn([
            //         ['tbl_cliente_direccion.id_cliente','=', $id],
            //         ['tbl_cliente_direccion.principal','=', '1']
            //     ])
            ->where('tbl_cliente_direccion.id_cliente','=',$id)
            ->where('tbl_cliente_direccion.principal','=',1)
            ->get();
        $r->data['direccion_principal']=$direccion_principal;
        return response()->json($r);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
