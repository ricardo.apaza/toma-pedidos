<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*  EXPORTAR MODELOS*/
    public function exportarTabla(Request $request){
        return response()->json($request);
    }








    public function exportPedidosToTXT(Request $request){
        $data = DB::table('tbl_pedido')->get();

        $hoy = date("Y-m-d-H,i,s");
        $archivo = "pedidos".$hoy.".txt";
        $contenido = "";

        $f=fopen($archivo,"w");

        foreach ($data as $pedido) {
            foreach ($pedido as $key => $value) {
                // $contenido.= $key .'=>'.$value;
                // dd(count($producto));
                // if($producto.count() )
                if($key == 'id' || $key == 'id_cliente' ||$key == 'id_vendedor' || $key == 'fecha_inicio'||$key == 'hora_inicio' || $key == 'monto_total'||$key == 'nota'||$key == 'latitud' ){
                    fputs($f,$value.'|'); 
                }elseif ( $key == 'longitud') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));

        }

        fwrite($f,$contenido);
        // dd($archivo);
        fclose($f);
        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
    }

    public function exportDetallesPedidosToTXT(Request $request){
        $data = DB::table('tbl_pedido_detalle')->get();
        $hoy = date("Y-m-d-H,i,s");
        $archivo = "detalle_pedidos".$hoy.".txt";
        $contenido = "";
        $f=fopen($archivo,"w");
        foreach ($data as $detalle) {
            foreach ($detalle as $key => $value) {
                if($key == 'id' || $key == 'id_pedido' ||$key == 'id_producto' || $key == 'cantidad'|| $key == 'precio_unitario'|| $key == 'id_precio_producto'|| $key == 'descuento_porcentaje'){
                    fputs($f,$value.'|'); 
                }elseif ( $key == 'subtotal') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));

        }
        fwrite($f,$contenido);
        fclose($f);
        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
    }

    public function exportNoPedidosToTXT(Request $request){
        $data = DB::table('tbl_no_pedido')->get();
        $hoy = date("Y-m-d-H,i,s");
        $archivo = "no_pedidos".$hoy.".txt";
        $contenido = "";
        $f=fopen($archivo,"w");
        foreach ($data as $nopedido) {
            foreach ($nopedido as $key => $value) {
               if($key == 'id' || $key == 'id_cliente' ||$key == 'id_vendedor' || $key == 'fecha_inicio'|| $key == 'hora_inicio'|| $key == 'motivo_no_pedido'){
                    fputs($f,$value.'|'); 
                }elseif ( $key == 'nota') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));
        }
        fwrite($f,$contenido);
        fclose($f);


        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
    }






    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
