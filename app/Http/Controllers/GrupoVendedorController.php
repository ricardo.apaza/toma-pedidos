<?php

namespace App\Http\Controllers;

use Illuminate\Database\Seeder;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\GrupoVendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GrupoVendedorController extends Controller
{
    public function __construct()
    {$this->middleware('auth');}

    public function createController()
    {   
        return view('vendedores.grupo_vendedor.grupo_vendedor_crear');
    }

    public function obtenerGrupoVendedor(Request $request)
    {
        $id = $request->get('id_grupo_vendedor');
        $r = new ApiResponse();

        $grupo_vendedor= GrupoVendedor::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    
        $r->data['grupo_vendedor'] = $grupo_vendedor;
        return response()->json($r);

    }
    public function datosGrupoVendedor(Request $request,$id)
    {   
        $grupo_vendedor = GrupoVendedor::orderBy('id','ASC')
                    ->where('id','=',$id)
                    ->get();    
        return view('vendedores.grupo_vendedor.grupo_vendedor_datos', compact('grupo_vendedor'));
    }



    public function obtenerGruposVendedores(Request $request)
    {
        
        $page = $request->get('page');
        $r = new ApiResponse();

        $grupos_vendedores = DB::table('tbl_grupo_vendedor')
            ->join('tbl_vendedor', 'tbl_grupo_vendedor.id_supervisor', '=', 'tbl_vendedor.id')
            ->select(DB::raw("tbl_grupo_vendedor.id as id, tbl_grupo_vendedor.codigo as codigo_grupo_vendedor , tbl_grupo_vendedor.descripcion as nombre_grupo_vendedor, tbl_vendedor.nombre as nombre_supervisor"))
            ->whereNull('tbl_grupo_vendedor.deleted_at')
            ->orderBy('tbl_grupo_vendedor.id','ASC')
            ->paginate(5);

        $pagination = [
            'total' => $grupos_vendedores->total(),
            'current_page' => $grupos_vendedores->currentPage(),
            'per_page' => $grupos_vendedores->perPage(),
            'last_page' => $grupos_vendedores->lastPage(),
            'from' => $grupos_vendedores->firstItem(),
            'to' => $grupos_vendedores->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['grupos_vendedores'] = $grupos_vendedores;
        // dd($r);
        return response()->json($r);
    }
    

    public function grupoVendedorInsertar(Request $request)
    {
        $r = new ApiResponse();

        $datagrupovendedor = $request->all();
        if ($request->get('id_grupo_vendedor', 0) == 0) {
            $grupo_vendedor = new GrupoVendedor();
            $count = GrupoVendedor::count();
            $count++;
            $datagrupovendedor['codigo']='GRUPOVENDEDOR'.$count;
        } else {
            $grupo_vendedor = GrupoVendedor::find($request->get('id_grupo_vendedor'));
        }
        $validate=$grupo_vendedor->isValid($datagrupovendedor);
        if ($validate->passes()) {
            $grupo_vendedor->fill($datagrupovendedor);
            $grupo_vendedor->save();

        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        $r->data = $grupo_vendedor;
        return response()->json($r);
    }
    public function grupoVendedorEliminar(Request $request)
    {
        $r = new ApiResponse();
        if ($request->get('id_grupo_vendedor', 0) != 0) {
            $id=$request->get('id_grupo_vendedor');
            $grupo_vendedor = GrupoVendedor::find($id);
            $grupo_vendedor->delete();
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
        // return view('vendedores.grupo_vendedor.grupo_vendedor');

    }
}
