<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;

use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;



class ProductoController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function productoImportadosInsertar(Request $request){

        $r = new ApiResponse();
        $dataproductos = $request->all();

        foreach ($dataproductos['data'] as $NuevoProducto) {
            $producto = new Producto();
            // dd($dataproductos);
            $validate=$producto->isValid($NuevoProducto);

            if ($validate->passes()) {
                $producto->fill($NuevoProducto);
                $producto->save();

            }else{
                $r->error=$validate->errors();
                $r->status->setStatus(Status::ERROR_PARAMS);
            }
        }
        // dd($dataproducto);
        // $r->data = $producto;
        return response()->json($r);
    }
    public function productoInsertar(Request $request){

        $r = new ApiResponse();
        $dataproducto = $request->all();
        // dd($dataproducto);
        if ($request->get('id_producto', 0) == 0) {
            $producto = new Producto();
            $count=Producto::count();
            $count++;
        } else {
            $producto = Producto::find($request->get('id_producto'));
        }
        $validate=$producto->isValid($dataproducto);

        if ($validate->passes()) {

            $producto->fill($dataproducto);
            $producto->save();

        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }

        $r->data = $producto;
        return response()->json($r);
    }
    
    public function obtenerProductos(Request $request)
    {
        $r = new ApiResponse();
        // $page = $request->get('page');

        $Producto = Producto::orderBy('id','ASC')->paginate(5);
        // $page = $request->get('page');

        $pagination = [
            'total' => $Producto->total(),
            'current_page' => $Producto->currentPage(),
            'per_page' => $Producto->perPage(),
            'last_page' => $Producto->lastPage(),
            'from' => $Producto->firstItem(),
            'to' => $Producto->lastItem(),
        ];
        $r->data['pagination'] = $pagination;
        $r->data['productos'] = $Producto;
        return response()->json($r);


    }


    public function productoEliminar(Request $request)
    {
        $r = new ApiResponse();

        if ($request->get('id_producto', 0) != 0) {
            $id=$request->get('id_producto');
            $producto = Producto::find($id);
            $producto->delete();
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }
    


    //viewProductoEditar
    public function viewProductoEditar()
    {
        return view('mantenimientos.producto_crear_editar');
    }
}
