<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\ClienteTelefono;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClienteTelefonoController extends Controller
{
    
	public function clienteTelefonoInsertar(Request $request){

        $r = new ApiResponse();
        $datacliente = $request->all();
        $datacliente['operadora'] = '';
        $datacliente['id_cliente_direccion'] = '';
        if ($datacliente['principal'] =='true') {
        	$datacliente['principal'] = 1;
        }else{
        	$datacliente['principal'] = 0;
        }

        if ($request->get('id_telefono', 0) == 0) {
            $cliente_telefono = new ClienteTelefono();
        } else {
            $cliente_telefono = ClienteTelefono::find($request->get('id_telefono'));
        }
        $validate=$cliente_telefono->isValid($datacliente);

        if ($validate->passes()) {
            $cliente_telefono->fill($datacliente);
            $cliente_telefono->save();
        }else{
            $r->error=$validate->errors();
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }
    


	public function obtenerClienteTelefono(Request $request)
    {
  		$r = new ApiResponse();
        // $users = User::all();
        $id=$request->get('id_telefono');
        $cliente_telefono =  DB::table('tbl_cliente_telefono')
                ->select(DB::raw('tbl_cliente_telefono.id,tbl_cliente_telefono.telefono,tbl_cliente_telefono.tipo_telefono,tbl_cliente_telefono.principal'))
                ->where('tbl_cliente_telefono.id','=',$id )
                ->orderBy('tbl_cliente_telefono.id','ASC')
                ->get();
       
        $r->data['cliente_telefono'] = $cliente_telefono;
        return response()->json($r);
    }
    public function clienteTelefonoEliminar(Request $request)
    {
        $r = new ApiResponse();

        if ($request->get('id_telefono', 0) != 0) {
            $id=$request->get('id_telefono');
            // dd($id);
            $telefono = ClienteTelefono::find($id);
            if (is_null($telefono)) {
    	        $r->status->setStatus(Status::ERROR_PARAMS);
	        }else{
            	$telefono->delete();
            }
        } else {
            $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }}
