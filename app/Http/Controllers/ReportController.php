<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use App\Models\Pedido;
use App\Models\Detalle_pedido;
use App\Models\Cliente;
use App\Models\Producto;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class ReportController extends Controller
{

     public function downloadReportToExcel(Request $request){

        $info = [];
        $data = [];
        $info['pedidos'] = DB::table('tbl_pedido')
            ->join('tbl_vendedor', 'tbl_pedido.id_vendedor', '=', 'tbl_vendedor.id')
            ->join('tbl_cliente', 'tbl_pedido.id_cliente', '=', 'tbl_cliente.id')
            ->select(DB::raw("tbl_pedido.id as ID ,tbl_pedido.id_vendedor as 'CODIGO VENDEDOR',tbl_vendedor.nombre as VENDEDOR,tbl_pedido.id_cliente as 'CODIGO CLIENTE',tbl_cliente.nombre as CLIENTE,tbl_pedido.fecha_inicio as FECHA,tbl_pedido.hora_inicio as HORA,ROUND(tbl_pedido.monto_total,2) as TOTAL"))
            ->orderBy('tbl_pedido.id','ASC')
            ->get()->toArray();

        $info['detalles'] = DB::table('tbl_pedido_detalle')
            ->join('tbl_pedido', 'tbl_pedido_detalle.id_pedido', '=', 'tbl_pedido.id')
            ->join('tbl_producto', 'tbl_pedido_detalle.id_producto', '=', 'tbl_producto.id')

            ->select(DB::raw("tbl_pedido_detalle.id_pedido as 'CODIGO PEDIDO',tbl_pedido_detalle.id_producto as 'CODIGO PRODUCTO',tbl_producto.codigo as 'PRODUCTO',tbl_pedido_detalle.precio_unitario as 'PRECIO UNITARIO',tbl_pedido_detalle.cantidad as 'CANTIDAD',tbl_pedido_detalle.descuento_porcentaje as 'DESCUENTO',tbl_pedido_detalle.subtotal as 'SUBTOTAL'"))
            ->orderBy('tbl_pedido_detalle.id','ASC')
            // ->orderByRaw("tbl_pedido_detalle.id_pedido ASC, tbl_pedido_detalle.id_producto ASC")
            ->get()->toArray();

        $info['nopedidos'] = DB::table('tbl_no_pedido')
            ->join('tbl_vendedor', 'tbl_no_pedido.id_vendedor', '=', 'tbl_vendedor.id')
            ->join('tbl_cliente', 'tbl_no_pedido.id_cliente', '=', 'tbl_cliente.id')
            ->select(DB::raw("tbl_no_pedido.id as ID,tbl_no_pedido.id_vendedor as 'CODIGO VENDEDOR',tbl_vendedor.nombre as VENDEDOR,tbl_no_pedido.id_cliente as 'CODIGO CLIENTE',tbl_cliente.nombre as CLIENTE,tbl_no_pedido.fecha_inicio as FECHA,tbl_no_pedido.hora_inicio as HORA,tbl_no_pedido.motivo_no_pedido as MOTIVO, tbl_no_pedido.nota as NOTA"))
            ->orderBy('tbl_no_pedido.id','ASC')
            ->get()->toArray();

        $data = json_decode(json_encode($info), true);
        
        $hoy = date("Y-m-d-H,i,s");
        $hoy ='PEDIDOS-'.$hoy;

        Excel::create($hoy, function($excel) use($data) {
         
            $excel->sheet('PEDIDOS', function($sheet) use($data) {
                $sheet->fromArray($data['pedidos'], null, 'A1', true);
                $sheet->cells('A1:H1', function($row) { 
                    $row->setFontWeight('bold'); 
                    $row->setAlignment('center');
                    $row->setValignment('center');
                    $row->setFontSize(12); 
                });

            });
            $excel->sheet('DETALLE PEDIDOS', function($sheet) use($data) {
                $sheet->fromArray($data['detalles'], null, 'A1', true);
                    $sheet->cells('A1:F1', function($row) { 
                    $row->setFontWeight('bold'); 
                    $row->setAlignment('center');
                    $row->setValignment('center');
                    $row->setFontSize(12); 
                });
            });
            $excel->sheet('NO PEDIDOS', function($sheet) use($data) {
                $sheet->fromArray($data['nopedidos'], null, 'A1', true);
                $sheet->cells('A1:H1', function($row) { 
                    $row->setFontWeight('bold'); 
                    $row->setAlignment('center');
                    $row->setValignment('center');
                    $row->setFontSize(12); 
                });
            });
         })->download('XLSX');
        // return view('reportes.pedidos');
    }


    public function downloadProductosToCSV(Request $request){
        $dataproducto = DB::table('tbl_producto')->get();
        $hoy = date("Y-m-d-H,i,s");
        $archivo = 'productos'.$hoy.".csv";
        $contenido = "";

        $f=fopen($archivo,"w");

        foreach ($dataproducto as $producto) {
            foreach ($producto as $key => $value) {
                // $contenido.= $key .'=>'.$value;
                // dd(count($producto));
                // if($producto.count() )
                if($key == 'id' || $key == 'codigo'|| $key == 'descripcion'){
                    fputs($f,$value.';'); 
                }elseif ( $key == 'precio_base') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));

        }

        fwrite($f,$contenido);
        // dd($archivo);
        fclose($f);
        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
        return view('mantenimientos.productos');

    }
    
    public function downloadProductosToTXT(Request $request){
       $dataproducto = DB::table('tbl_producto')->get();

        $hoy = date("Y-m-d-H,i,s");
        $archivo = "productos".$hoy.".txt";
        $contenido = "";

        $f=fopen($archivo,"w");

        foreach ($dataproducto as $producto) {
            foreach ($producto as $key => $value) {
                // $contenido.= $key .'=>'.$value;
                // dd(count($producto));
                // if($producto.count() )
               if($key == 'id' || $key == 'codigo'|| $key == 'descripcion'){
                    fputs($f,$value.';'); 
                }elseif ( $key == 'precio_base') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));

        }

        fwrite($f,$contenido);
        // dd($archivo);
        fclose($f);
        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
        return view('mantenimientos.productos');

    }



    //clientes
    public function downloadClientesToCSV(Request $request){
        $datacliente = DB::table('tbl_cliente')->get();
        $hoy = date("Y-m-d-H,i,s");
        $archivo = 'clientes'.$hoy.".csv";
        $contenido = "";

        $f=fopen($archivo,"w");

         foreach ($datacliente as $cliente) {
            foreach ($cliente as $key => $value) {
                // $contenido.= $key .'=>'.$value;
                // dd(count($producto));
                // if($producto.count() )
                if($key == 'id' || $key == 'codigo'  || $key == 'nombre'|| $key == 'direccion'|| $key == 'telefono'){
                    fputs($f,$value.';'); 
                }elseif ( $key == 'tipo_cliente') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));

        }

        fwrite($f,$contenido);
        // dd($archivo);
        fclose($f);
        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
        return view('mantenimientos.clientes');


    }
    public function downloadClientesToTXT(Request $request){
       $datacliente = DB::table('tbl_cliente')->get();
       
        $hoy = date("Y-m-d-H,i,s");
        $archivo = "clientes".$hoy.".txt";
        $contenido = "";

        $f=fopen($archivo,"w");

        foreach ($datacliente as $cliente) {
            foreach ($cliente as $key => $value) {
                // $contenido.= $key .'=>'.$value;
                // dd(count($producto));
                // if($producto.count() )
                if($key == 'id' || $key == 'codigo'  || $key == 'nombre'|| $key == 'direccion'|| $key == 'telefono'){
                    fputs($f,$value.'|'); 
                }elseif ( $key == 'tipo_cliente') {
                    fputs($f,$value); 
                }
            }
            fputs($f,chr(13).chr(10));

        }

        fwrite($f,$contenido);
        // dd($archivo);
        fclose($f);
        $enlace = $archivo; 
        header ("Content-Disposition: attachment; filename=".$enlace); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($enlace)); 
        readfile($enlace); 
        return view('mantenimientos.clientes');
        
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
