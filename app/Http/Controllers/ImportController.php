<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Api\Status;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use Excel;




class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //  public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function importDataFromFileCSV(Request $request){

        $r = new ApiResponse();
        $tipo = $request->file('fileCSV')->extension();
        // dd($tipo);
        if ($request->hasFile('fileCSV')){
            if ($tipo == 'txt' || $tipo == 'csv') {
                $file = $request->file('fileCSV')->storeAs('public/productos', uniqid() . '.csv' );//$request->file('foto')->extension()
                    // $file->storeAs( 'products', 'products_' . date( 'd_m_y' ) . '.csv' );
                $ruta = storage_path('app/'.$file);
                    // $ruta = "C:\Users\User\Downloads\productos.csv";
                $dataProductsFromFile = array();
                $archivo = fopen($ruta, "r");
                    // dd($archivo);
                while ($data = fgetcsv ($archivo, 1000, ";")) {
                    $json = new \stdClass();
                        // $json->id_pro = $data[0];

                    // $json->nombre = $data[1];
                    $json->codigo = $data[1];
                    $json->descripcion = $data[2];
                    $json->precio_base = $data[3];
                    array_push( $dataProductsFromFile, $json );
                }

                fclose($archivo);

                $r->data = $dataProductsFromFile;
                unlink($ruta);//Elimina el archivo subido
            }else{
                    $r->status->setStatus(Status::ERROR_PARAMS);
                }
        } else {
                $r->status->setStatus(Status::ERROR_PARAMS);
        }

        return response()->json($r);
    }

     public function importDataFromFileCSVclientes(Request $request){

        $r = new ApiResponse();
        $tipo = $request->file('fileCSV')->extension();
        // dd($tipo);
        if ($request->hasFile('fileCSV')){
            if ($tipo == 'txt' || $tipo == 'csv') {
                $file = $request->file('fileCSV')->storeAs('public/clientes', uniqid() . '.csv' );//$request->file('foto')->extension()
                    // $file->storeAs( 'products', 'products_' . date( 'd_m_y' ) . '.csv' );
                $ruta = storage_path('app/'.$file);
                    // $ruta = "C:\Users\User\Downloads\productos.csv";
                $dataCustomersFromFile = array();
                $archivo = fopen($ruta, "r");
                    // dd($archivo);
                while ($data = fgetcsv ($archivo, 1000, ";")) {
                    $json = new \stdClass();
                        // $json->id_pro = $data[0];
                    $json->codigo = $data[1];
                    $json->apellido = $data[2];
                    $json->nombre = $data[3];
                    $json->razon_social = $data[4];
                    $json->tipo_cliente = $data[5];
                    $json->giro = $data[6];
                    $json->deuda = $data[7];
                    $json->calificacion = $data[8];
                    array_push( $dataCustomersFromFile, $json );
                }

                fclose($archivo);

                $r->data = $dataCustomersFromFile;
                unlink($ruta);//Elimina el archivo subido
            }else{
                    $r->status->setStatus(Status::ERROR_PARAMS);
                }
        } else {
                $r->status->setStatus(Status::ERROR_PARAMS);
        }

        return response()->json($r);
    }





public function importDataFromFileTXTclientes(Request $request){

        $r = new ApiResponse();
        $tipo = $request->file('fileTXT')->extension();
        if ($request->hasFile('fileTXT')){
               if ($tipo == 'txt') {

                $file = $request->file('fileTXT')->storeAs('public/clientes', uniqid() . '.txt' );
                $ruta = storage_path('app/'.$file);
                $dataCustomersFromFile = array();

                $lineas = file($ruta);
                foreach ($lineas as $linea_num => $linea)
                {
                    $json = new \stdClass();
                    $datos = explode("|",$linea);
                        // $json->id_pro = $datos[0];
                    $json->codigo = $datos[1];
                    $json->apellido = $datos[2];
                    $json->nombre = $datos[3];
                    $json->razon_social = $datos[4];
                    $json->tipo_cliente = $datos[5];
                    $json->giro = $datos[6];
                    $json->deuda = $datos[7];
                    $json->calificacion = $datos[8];
                    array_push( $dataCustomersFromFile, $json );
                }
                $r->data = $dataCustomersFromFile;
                unlink($ruta);//Elimina el archivo subido
           }else{
                    $r->status->setStatus(Status::ERROR_PARAMS);
                }
        } else {
                $r->status->setStatus(Status::ERROR_PARAMS);
        }
        return response()->json($r);
    }

    public function importDataFromFileTXT(Request $request){

        $r = new ApiResponse();
        $tipo = $request->file('fileTXT')->extension();

        if ($request->hasFile('fileTXT')){
               if ($tipo == 'txt') {

                $file = $request->file('fileTXT')->storeAs('public/productos', uniqid() . '.txt' );//$request->file('foto')->extension()
                $ruta = storage_path('app/'.$file);
                // $ruta = "C:\Users\User\Downloads\productos.csv";
                // dd($ruta);
                $dataProductsFromFile = array();
                $lineas = file($ruta);
                // $dd($lineas);
                foreach ($lineas as $linea_num => $linea)
                {
                    $json = new \stdClass();
                    $datos = explode("|",$linea);
                 
                    // $json->id_pro = trim($datos[0]);
                    $json->codigo = $datos[1];
                    $json->descripcion = $datos[2];
                    $json->precio_base = $datos[3];
                    array_push( $dataProductsFromFile, $json );
                }
                $r->data = $dataProductsFromFile;
                unlink($ruta);//Elimina el archivo subido
           }else{
                    $r->status->setStatus(Status::ERROR_PARAMS);
                }
        } else {
                $r->status->setStatus(Status::ERROR_PARAMS);
        }

        return response()->json($r);
    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
