<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductoCategoria extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_producto_categoria';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	'nombre',
        'descripcion',
        'id_padre'
    ];

    public function isValid($input){
        $rules = array(

            'nombre' => 'required',
            'descripcion' => 'required'

        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
