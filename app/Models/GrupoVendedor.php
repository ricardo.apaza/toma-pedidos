<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrupoVendedor extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_grupo_vendedor';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	'codigo',
        'descripcion',
        'id_supervisor',
        'ubigeo',
    ];
    public function isValid($input){
        $rules = array(

            'codigo' => 'required',
            // 'descripcion' => 'required',
            // 'id_supervisor' => 'required',
        );
        // make a new validator object
        $v = Validator::make($input, $rules);
        return  $v;
    }
}
