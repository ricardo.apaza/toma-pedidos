<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClienteTelefono extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_cliente_telefono';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'id_cliente',
        'telefono',
        'tipo_telefono',
        'operadora',
        'id_cliente_direccion',
        'principal',
    ];


    public function isValid($input){
        $rules = array(
            'id_cliente' => 'required',
            'telefono' => 'required',
            'tipo_telefono' => 'required',
            'principal'=>'required'
        );
        $v = Validator::make($input, $rules);
        return  $v;
    }
}
