<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePedido extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_pedido_detalle';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	'id_pedido',
        'id_producto',
        'cantidad',
        'precio_unitario',
        'id_producto_precio',
        'descuento_porcentaje',
        'subtotal',
    ];
    public function isValid($input){
        $rules = array(

            'id_pedido' => 'required',
            'id_producto' => 'required'
            'cantidad' => 'required'
            'precio_unitario' => 'required'
            'id_producto_precio' => 'required'
            'descuento_porcentaje' => 'required'
            'subtotal' => 'required'
        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
