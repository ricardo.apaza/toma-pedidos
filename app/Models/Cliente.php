<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model

{
    use SoftDeletes;

    protected $table = 'tbl_cliente';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'codigo',
        'apellido',
        'nombre',
        'razon_social',
        'tipo_cliente',
        'giro',
        'deuda',
        'calificacion',
    ];


    public function isValid($input){
        $rules = array(
            'nombre' => 'required',
            'direccion' => 'required',
            'tipo_cliente' => 'required'
        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
