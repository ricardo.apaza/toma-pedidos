<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_pedido';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	'id_cliente',
        'id_vendedor',
        'fecha_inicio',
        'hora_inicio',
        'monto_total',
        'nota',
        'codigo',
        'latitud',
        'longitud',
    ];
    public function isValid($input){
        $rules = array(

            'id_cliente' => 'required',
            'id_vendedor' => 'required',
            'fecha_inicio' => 'required',
            'hora_inicio' => 'required',
            'monto_total' => 'required',
            'nota' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',

        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
