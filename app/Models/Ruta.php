<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ruta extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_ruta';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	'id_cliente',
        'id_vendedor',
        'fecha_inicio',
        'fecha_fin',
        'dia_semana',
    ];
    public function isValid($input){
        $rules = array(

            'id_cliente' => 'required',
            'id_vendedor' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'dia_semana' => 'required',
        );
        // make a new validator object
        $v = Validator::make($input, $rules);
        return  $v;
    }
}
