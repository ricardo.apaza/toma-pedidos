<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClienteEmail extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_cliente_email';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'id_cliente',
        'email',
        'tipo_email',
        'principal',
    ];


    public function isValid($input){
        $rules = array(
            'id_cliente' => 'required',
            'email' => 'required',
            // 'tipo_email' => 'required',
            'principal'=>'required'
        );
        $v = Validator::make($input, $rules);
        return  $v;
    }
}
