<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoPedido extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_no_pedido';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	'id_cliente',
        'id_vendedor',
        'codigo',
        'fecha_inicio',
        'hora_inicio',
        'motivo_no_pedido',
        'nota',
    ];
    public function isValid($input){
        $rules = array(

            'id_cliente' => 'required',
            'id_vendedor' => 'required',
            'fecha_inicio' => 'required',
            'hora_inicio' => 'required',
            'motivo_no_pedido' => 'required',
            'nota' => 'required',
        );
        // make a new validator object
        $v = Validator::make($input, $rules);
        return  $v;
    }
}
