<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_producto';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	// 'nombre',
        'codigo',
        'descripcion',
        'precio_base',
    ];

    public function isValid($input){
        $rules = array(

            'codigo' => 'required',
            'descripcion' => 'required',
            'precio_base' => 'required'

        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
