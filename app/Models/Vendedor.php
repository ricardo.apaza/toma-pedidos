<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendedor extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_vendedor';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
    	// 'nombre',
        'codigo',
        'apellido',
        'nombre',
        'id_grupo_vendedor',
        'id_usuario',
    ];

    public function isValid($input){
        $rules = array(

            'codigo' => 'required',
            'apellido' => 'required',
            'nombre' => 'required',
            'id_grupo_vendedor' => 'required',
            'id_usuario' => 'required'

        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
