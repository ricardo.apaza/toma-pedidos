<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
class ClienteDireccion extends Model
{
     use SoftDeletes;

    protected $table = 'tbl_cliente_direccion';
    //protected  $hidden = ['updated_at'];
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'id_cliente',
        'direccion',
        'ubigeo',
        'tipo_direccion',
        'principal',
        'latitud',
        'longitud',
    ];


    public function isValid($input){
        $rules = array(
            'id_cliente' => 'required',
            'direccion' => 'required',
            'tipo_direccion' => 'required',
            'principal'=>'required'
        );
        // make a new validator object
        $v = Validator::make($input, $rules);

        return  $v;
    }
}
