<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use \Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'tbl_usuario';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'email', 'password','id_perfil'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isValid($input){
        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        );
        // make a new validator object
        $v = Validator::make($input, $rules);
        return  $v;
    }
}
