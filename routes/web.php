<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('/home', 'HomeController@index')->name('home');


/**/
// Route::get('/{any}', 'SpaController@index')->where('any', '.*');
/**/



// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/api/customers', 'CustomerController@search');
// Route::get('/api/products', 'ProductController@search');

// Route::resource('/api/invoices', 'InvoiceController');



Route::get('/', 'HomeController@index');

Route::get('logout',[
    'uses' => 'Auth\LoginController@logout',
    'as'   => 'logout'
]);

Auth::routes();

Route::get('/usuarios', 'HomeController@viewUsuarios');
Route::get('/clientes', 'HomeController@viewClientes');
Route::get('/productos', 'HomeController@viewProductos');
Route::get('/pedidos', 'HomeController@viewPedidos');
Route::get('/no-pedidos', 'HomeController@viewNoPedidos');
Route::get('/cotizaciones', 'HomeController@viewCotizaciones');
Route::get('/visitas', 'HomeController@viewVisitas');
Route::get('/integracion1', 'HomeController@viewIntegracion1');

//v3

/**/

Route::get('/clientes-direcciones', 'HomeController@viewClientesDirecciones');
Route::get('/clientes-telefonos', 'HomeController@viewClientesTelefono');
Route::get('/clientes-emails', 'HomeController@viewClientesEmails');

/*
*/

Route::get('/exportar', 'HomeController@viewExportar');
Route::get('/importar', 'HomeController@viewImportar');

Route::get('/home', 'HomeController@viewInicio');



//vendedores
Route::get('/vendedores', 'HomeController@viewVendedores');



    

Route::get('/grupos-vendedores', 'HomeController@viewGrupoVendedor');
Route::get('/rutas', 'HomeController@viewRutas');






//

// Route::group( [ 'middleware' => 'auth' ], function (){

// }

//clientes
Route::get('/cliente-editar', 'ClienteController@viewClienteEditar')->name('editar-crear-cliente');
Route::any('/obtener-clientes', 'ClienteController@obtenerClientes');
Route::post('/guardar-cliente', 'ClienteController@clienteInsertar');
Route::post('/eliminar-cliente', 'ClienteController@clienteEliminar');
//Route::any('/select-data-clientes', 'ClienteController@selectDataClientes');


//Producto
Route::any('/obtener-productos', 'ProductoController@obtenerProductos');
Route::post('/guardar-producto', 'ProductoController@productoInsertar');

//Route::get('/producto-editar', 'ProductoController@viewProductoEditar');

Route::get('/producto-editar', 'ProductoController@viewProductoEditar')->name('editar-crear-producto');


Route::post('/eliminar-producto', 'ProductoController@productoEliminar');
//Route::any('/select-data-productos', 'ProductoController@selectDataProductos');

//Pedidos
Route::any('/obtener-pedidos', 'PedidoController@obtenerPedidos');
Route::post('/guardar-pedido', 'PedidoController@pedidoInsertar');
Route::post('/eliminar-pedido', 'PedidoController@pedidoEliminar');


//Import
Route::post('/importar-productos-csv', 'ImportController@importDataFromFileCSV');
Route::post('/importar-productos-txt', 'ImportController@importDataFromFileTXT');
Route::post('/importar-clientes-csv', 'ImportController@importDataFromFileCSVclientes');
Route::post('/importar-clientes-txt', 'ImportController@importDataFromFileTXTclientes');

Route::post('/guardar-productos-cargados-csv', 'ProductoController@productoImportadosInsertar');
Route::post('/guardar-clientes-cargados-csv', 'ClienteController@clienteImportadosInsertar');


//Route::any('/select-data-pedido', 'PedidoController@selectDataPedidos');

Route::any('/obtener-pedido-detalles', 'PedidoController@obtenerPedidoDetalles');
//REPORTES

Route::any('/descargar-excel', 'ReportController@downloadReportToExcel')->name('downloadExcel');


//Export to csv / txt productos
Route::any('/descargar-producto-csv', 'ReportController@downloadProductosToCSV')->name('downloadProductosCSV');
Route::any('/descargar-producto-txt', 'ReportController@downloadProductosToTXT')->name('downloadProductosTXT');


//Export to csv / txt clientes
Route::any('/descargar-cliente-csv', 'ReportController@downloadClientesToCSV')->name('downloadClientesCSV');
Route::any('/descargar-cliente-txt', 'ReportController@downloadClientesToTXT')->name('downloadClientesTXT');




//pedidos to txt
Route::any('/descargar-pedidod-txt', 'ExportController@exportPedidosToTXT')->name('exportPedidosToTXT');
//no pedidos to txt
Route::any('/descargar-no-pedidos-txt', 'ExportController@exportNoPedidosToTXT')->name('exportNoPedidosToTXT');
//detalles to txt
Route::any('/descargar-detalles-pedidos-txt', 'ExportController@exportDetallesPedidosToTXT')->name('exportDetallesToTXT');




//built
Route::any('/exportar-tabla', 'ExportController@exportarTabla')->name('exportarTabla');


	

//NO PEDIDOS
Route::any('/obtener-no-pedidos', 'NoPedidoController@obtenerNoPedidos');



//usuarios
Route::any('/obtener-usuarios', 'UsuarioController@obtenerUsuarios');
Route::post('/guardar-usuario', 'UsuarioController@usuarioInsertar');
Route::post('/eliminar-usuario', 'UsuarioController@usuarioEliminar');

//v2

//Vendedores
Route::any('/obtener-vendedores', 'VendedorController@obtenerVendedores');
Route::any('/obtener-vendedor', 'VendedorController@obtenerVendedor');

//vendedores editar
// Route::any('/vendedor/vendedor-datos/{id}', 'HomeController@datosVendedor');

// Route::get('/vendedor/vendedor-datos/{id}','VendedorController@datosVendedor');




/*1ra*/
// Route::get('/vendedor/vendedor-datos/{id}', 'VendedorController@datosVendedor');



// Route::any('/vendedor/vendedor-datos', 'VendedorController@datosVendedor');



Route::prefix('vendedor')->group(function () {
    Route::get('vendedor-datos/{id}', 'VendedorController@datosVendedor')->name('vendedor-datos');
    Route::get('create', 'VendedorController@createController');
    Route::any('select-data', 'VendedorController@selectDataVendedor');
    Route::post('guardar-vendedor', 'VendedorController@vendedorInsertar');



});
Route::prefix('ruta')->group(function () {
    Route::get('ruta-datos/{id}', 'RutaController@datosRuta')->name('ruta-datos');
    Route::get('create', 'RutaController@createController');
    Route::any('select-data', 'RutaController@selectDataZona');
    Route::any('/obtener-rutas', 'RutaController@obtenerRutas');
    Route::any('/obtener-ruta', 'RutaController@obtenerRuta');
    Route::post('/guardar-ruta', 'RutaController@rutaInsertar');
    Route::post('/eliminar-ruta', 'RutaController@rutaEliminar');

});


Route::prefix('grupo-vendedor')->group(function () {
    Route::get('grupo-vendedor-datos/{id}', 'GrupoVendedorController@datosGrupoVendedor')->name('grupo-vendedor-datos');
    Route::get('create', 'GrupoVendedorController@createController');
    Route::any('select-data', 'GrupoVendedorController@selectDataGrupoVendedor');
    Route::post('guardar-grupo-vendedor', 'GrupoVendedorController@grupoVendedorInsertar');
    Route::any('obtener-grupos-vendedores', 'GrupoVendedorController@obtenerGruposVendedores');
    Route::any('obtener-grupo-vendedor', 'GrupoVendedorController@obtenerGrupoVendedor');
    Route::any('eliminar-grupo-vendedor', 'GrupoVendedorController@grupoVendedorEliminar');
});

Route::prefix('pedidos')->group(function () {
    Route::get('pedido-datos/{id}', 'PedidoController@pedidoDatos');
    Route::post('pedido-topdf','PedidoController@pedidoToPdf');
    Route::post('obtener-pedido-detalles', 'PedidoController@obtenerPedidoDetalles');
    Route::post('obtener-datos-adicionales', 'PedidoController@obtenerPedidoDatosAdicionales');

});




Route::prefix('cliente')->group(function () {
    Route::get('cliente-datos/{id}', 'ClienteController@datosCliente')->name('cliente-datos');
    Route::get('create', 'ClienteController@createController');
    Route::any('select-data', 'ClienteController@selectDataCliente');
    Route::post('guardar-cliente', 'ClienteController@clienteInsertar');
    Route::post('obtener-cliente', 'ClienteController@obtenerCliente');
    Route::post('eliminar-cliente', 'ClienteController@clienteEliminar');
    Route::post('obtener-datos-adicionales-cliente', 'ClienteController@obtenerDatosAdicionalesCliente');

    
    // cliente/eliminar-cliente
});

Route::prefix('cliente-direccion')->group(function () {
    Route::post('/guardar-cliente-direccion', 'ClienteDireccionController@clienteDireccionInsertar');
    Route::post('/eliminar-cliente-direccion', 'ClienteDireccionController@clienteDireccionEliminar');
    Route::post('/obtener-direccion', 'ClienteDireccionController@obtenerClienteDireccion');
});
Route::prefix('cliente-email')->group(function () {
    Route::post('/guardar-cliente-email', 'ClienteEmailController@clienteEmailInsertar');
    Route::post('/eliminar-cliente-email', 'ClienteEmailController@clienteEmailEliminar');
    Route::post('/obtener-email', 'ClienteEmailController@obtenerClienteEmail');
});
Route::prefix('cliente-telefono')->group(function () {
    Route::post('/guardar-cliente-telefono', 'ClienteTelefonoController@clienteTelefonoInsertar');
    Route::post('/eliminar-cliente-telefono', 'ClienteTelefonoController@clienteTelefonoEliminar');
    Route::post('/obtener-telefono', 'ClienteTelefonoController@obtenerClienteTelefono');
});





Route::prefix('test')->group(function () {
	Route::get('datos/{id}', 'HomeController@test');
});

// Route::post('/guardar-vendedor', 'VendedorController@vendedorInsertar');
Route::post('/eliminar-vendedor', 'VendedorController@vendedorEliminar');

// Route::any('/obtener-rutas', 'RutaController@obtenerRutas');
// Route::any('/obtener-ruta', 'RutaController@obtenerRuta');
// Route::post('/guardar-ruta', 'RutaController@rutaInsertar');
// Route::post('/eliminar-ruta', 'RutaController@rutaEliminar');






